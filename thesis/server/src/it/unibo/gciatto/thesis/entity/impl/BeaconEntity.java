package it.unibo.gciatto.thesis.entity.impl;

import it.unibo.gciatto.thesis.device.IBeaconDevice;
import it.unibo.gciatto.thesis.device.impl.BeaconDevice;
import it.unibo.gciatto.thesis.entity.id.impl.EntityId;
import it.unibo.gciatto.thesis.entity.landmark.impl.Landmark;

public class BeaconEntity extends Landmark {

	/**
	 *
	 */
	private static final long serialVersionUID = -75693565736450601L;

	public BeaconEntity(final IBeaconDevice dev) {
		super(new EntityId(dev.getMac()));
		associateDevice(dev);
	}
	
	public BeaconEntity(final String name, final IBeaconDevice dev) {
		super(new EntityId(name));
		associateDevice(dev);
	}
}
