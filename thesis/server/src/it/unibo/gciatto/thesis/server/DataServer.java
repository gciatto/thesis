package it.unibo.gciatto.thesis.server;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.device.impl.BeaconDevice;
import it.unibo.gciatto.thesis.device.impl.Device;
import it.unibo.gciatto.thesis.device.impl.JsonDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.EntityId;
import it.unibo.gciatto.thesis.entity.id.impl.JsonEntityId;
import it.unibo.gciatto.thesis.entity.impl.BeaconEntity;
import it.unibo.gciatto.thesis.entity.impl.Entity;
import it.unibo.gciatto.thesis.entity.impl.JsonEntity;
import it.unibo.gciatto.thesis.entity.landmark.impl.LandmarkDecorator;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.entity.position.impl.EntityRelativeDistance;
import it.unibo.gciatto.thesis.http.impl.Protocol;
import it.unibo.gciatto.thesis.lod.impl.LOD;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.AbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.JsonAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.JsonEntityRelativeDistance;
import it.unibo.gciatto.util.IJsonArrayConvertible;
import it.unibo.gciatto.util.IJsonObjectConvertible;
import it.unibo.gciatto.util.impl.JsonAttributeSet;
import it.unibo.gciatto.util.impl.MathUtils.rand;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json2.JSONArray;
import org.json2.JSONObject;
import org.json2.JSONTokener;

/**
 * Servlet implementation class DataServer
 */
@WebServlet({ "/DataServer", "/Data", "/data" })
public class DataServer extends HttpServlet {
	protected static void marshall(final Collection<? extends IJsonObjectConvertible> convertibles, final HttpServletResponse response) {
		final JSONArray array = new JSONArray();
		for (final IJsonObjectConvertible conv : convertibles) {
			array.put(conv.toJsonObject());
		}
		marshall(array, response);
	}

	protected static void marshall(final IJsonArrayConvertible convertible, final HttpServletResponse response) {
		marshall(convertible.toJsonArray(), response);
	}

	protected static void marshall(final IJsonObjectConvertible convertible, final HttpServletResponse response) {
		marshall(convertible.toJsonObject(), response);
	}

	protected static void marshall(final JSONArray array, final HttpServletResponse response) {
		try {
			array.write(response.getWriter());
		} catch (final Exception e) {
			L.e(e);
		}
	}

	protected static void marshall(final JSONObject obj, final HttpServletResponse response) {
		try {
			obj.write(response.getWriter());
		} catch (final Exception e) {
			L.e(e);
		}
	}

	protected static void marshall(final Map<JsonEntityId, ? extends IJsonObjectConvertible> convertibles, final HttpServletResponse response) {
		final JSONArray array = new JSONArray();
		for (final JsonEntityId id : convertibles.keySet()) {
			final JSONArray element = new JSONArray();
			element.put(id.toJsonObject());
			element.put(convertibles.get(id).toJsonObject());
			array.put(element);
		}
		marshall(array, response);
	}

	protected static JSONArray unmarshallJsonArray(final HttpServletRequest request) {
		JSONTokener tokener;
		try {
			tokener = new JSONTokener(request.getReader());
		} catch (final IOException e) {
			L.e(e);
			return null;
		}
		final JSONArray array = new JSONArray(tokener);
		return array;
	}

	protected static JSONObject unmarshallJsonObject(final HttpServletRequest request) {
		JSONTokener tokener;
		try {
			tokener = new JSONTokener(request.getReader());
		} catch (final IOException e) {
			L.e(e);
			return null;
		}
		final JSONObject obj = new JSONObject(tokener);
		return obj;
	}

	private static final long serialVersionUID = 1L;

	private final Map<IEntityId, IEntity> mData = new HashMap<IEntityId, IEntity>();
	private final IEntity mTestEntity;
	private final IEntity mBeacon1, mBeacon2, mBeacon3; //, mBeacon4, mBeacon5;

	private final ScheduledExecutorService mWorker = Executors.newSingleThreadScheduledExecutor();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DataServer() {
		super();
		mTestEntity = new Entity(new EntityId("TestEntity"));
		mTestEntity.putAttribute("message", "This is a test entity, please ignore it");
		mTestEntity.setLOD(new LOD(3));
		mTestEntity.move(AbsolutePosition.getRandomAbsolutePosition());
		mTestEntity.associateDevice(new Device(IDevice.TYPE_PC, "server"));

		// L.i("e1.id.hashcode = %s", mTestEntity.getId().hashCode());

		mData.put(mTestEntity.getId(), mTestEntity);

		mBeacon1 = new LandmarkDecorator(new BeaconEntity("landmark_A", BeaconDevice.getBeacon1()));
		mData.put(mBeacon1.getId(), mBeacon1);
		mBeacon1.move(new AbsolutePosition(44.151578, 12.243431, 0));

		mBeacon2 = new LandmarkDecorator(new BeaconEntity("landmark_B", BeaconDevice.getBeacon2()));
		mData.put(mBeacon2.getId(), mBeacon2);
		mBeacon2.move(new AbsolutePosition(44.151636, 12.243522, 0));
		
		mBeacon3 = new LandmarkDecorator(new BeaconEntity("landmark_C", BeaconDevice.getBeacon3()));
		mData.put(mBeacon3.getId(), mBeacon3);
		mBeacon3.move(new AbsolutePosition(44.151581, 12.243636, 0));
//
//		mBeacon4 = new LandmarkDecorator(new BeaconEntity("landmark_D", BeaconDevice.getBeacon4()));
//		mData.put(mBeacon4.getId(), mBeacon4);
//		mBeacon4.move(new AbsolutePosition(44.139939, 12.242986, 0));
//		
//		mBeacon5 = new LandmarkDecorator(new BeaconEntity("landmark_E", BeaconDevice.getBeacon5()));
//		mData.put(mBeacon5.getId(), mBeacon5);
//		mBeacon5.move(new AbsolutePosition(44.139811, 12.243047, 0));
		
		
		mWorker.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				mTestEntity.move(AbsolutePosition.getRandomAbsolutePosition());
//				mTestEntity.removeAllRelativeDistances();
//				synchronized (mData) {
//					for (final IEntityId id : mData.keySet()) {
//						if (!id.isSameEntity(mTestEntity.getId())) {
//							mTestEntity.addRelativeDistance(new EntityRelativeDistance(mData.get(id), rand.randomPositiveDouble(30d)));
//						}
//					}
//				}
			}
		}, 1, 1, TimeUnit.SECONDS);
	}

	protected int countEntities() {
		synchronized (mData) {
			return mData.size();
		}
	}

	protected void doAddData(final IEntityId id, final HttpServletRequest request, final HttpServletResponse response) {
		final IEntity entity = eventuallyRegisterEntity(id);
		final JSONObject obj = unmarshallJsonObject(request);
		entity.putAllAttributes(new JsonAttributeSet(obj));
	}

	protected void doAddRelativeDistance(final IEntityId id, final HttpServletRequest request, final HttpServletResponse response) {
		final IEntity entity = eventuallyRegisterEntity(id);
		final JSONObject obj = unmarshallJsonObject(request);
		synchronized (mData) {
			entity.addRelativeDistance(new JsonEntityRelativeDistance(mData, obj));
		}
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		final String what = request.getParameter(Protocol.PARAM_WHAT);
		final String idString = request.getParameter(Protocol.PARAM_ENTITY_ID);
		final IEntityId id = EntityId.fromURICharSeq(idString);
		L.i("DELETE What=%s Entity=%s", what, idString);
		if (what == null) {
			response.sendError(HttpServletResponse.SC_EXPECTATION_FAILED);
		} else if (Protocol.what.VALUE_EVERYTHING.equalsIgnoreCase(what)) {
			doReset(request, response);
		} else {
			response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
		}
	}

	protected void doFixAbsolutePosition(final IEntityId id, final HttpServletRequest request, final HttpServletResponse response) {
		final IEntity entity = eventuallyRegisterEntity(id);
		final JSONObject obj = unmarshallJsonObject(request);
		entity.move(new JsonAbsolutePosition(obj));
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		final String what = request.getParameter(Protocol.PARAM_WHAT);
		final String idString = request.getParameter(Protocol.PARAM_ENTITY_ID);
		final IEntityId id = EntityId.fromURICharSeq(idString);
		L.i("GET What=%s Entity=%s", what, idString);
		if (what == null) {
			doGetWebPage(response);
		} else if (Protocol.what.VALUE_DATA.equalsIgnoreCase(what)) {
			doGetAttributes(id, response);
		} else if (Protocol.what.VALUE_EVERYTHING.equalsIgnoreCase(what)) {
			doGetEverything(response);
		} else if (Protocol.what.VALUE_IDS.equalsIgnoreCase(what)) {
			doGetIds(response);
		} else if (Protocol.what.VALUE_POSITION.equalsIgnoreCase(what)) {
			doGetAbsolutePosition(id, response);
		} else if (Protocol.what.VALUE_POSITIONS.equalsIgnoreCase(what)) {
			doGetPositions(response);
		} else if (Protocol.what.VALUE_DISTANCES.equalsIgnoreCase(what)) {
			doGetRelativeDistances(id, response);
		} else if (Protocol.what.VALUE_DEVICES.equalsIgnoreCase(what)) {
			doGetDevices(id, response);
		} else {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	protected void doGetAbsolutePosition(final IEntityId id, final HttpServletResponse response) {
		if (isIdValid(id)) {
			final IEntity query = getEntity(id);
			final JsonAbsolutePosition attributes = new JsonAbsolutePosition(query.getAbsolutePosition());
			marshall(attributes, response);
		} else {
			try {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			} catch (final IOException e) {
				L.e(e);
			}
		}
	}

	protected void doGetAttributes(final IEntityId id, final HttpServletResponse response) {
		if (isIdValid(id)) {
			final JsonAttributeSet attributes = new JsonAttributeSet(getEntity(id));
			marshall(attributes, response);
		} else {
			try {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			} catch (final IOException e) {
				L.e(e);
			}
		}
	}

	protected void doGetDevices(final IEntityId id, final HttpServletResponse response) {
		if (isIdValid(id)) {
			final IEntity query = getEntity(id);
			final Collection<JsonDevice> devices = new ArrayList<JsonDevice>(query.getAssociatedDevices().size());
			for (final IDevice d : query.getAssociatedDevices()) {
				devices.add(new JsonDevice(d));
			}
			marshall(devices, response);
		} else {
			try {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			} catch (final IOException e) {
				L.e(e);
			}
		}
	}

	protected void doGetEverything(final HttpServletResponse response) {
		final Set<IEntityId> source = getIds();
		final List<JsonEntity> entities = new ArrayList<JsonEntity>(countEntities());
		for (final IEntityId id : source) {
			entities.add(new JsonEntity(getEntity(id)));
		}
		marshall(entities, response);
	}

	protected void doGetIds(final HttpServletResponse response) {
		final List<JsonEntityId> ids = new ArrayList<JsonEntityId>(countEntities());
		final Set<IEntityId> source = getIds();
		for (final IEntityId id : source) {
			ids.add(new JsonEntityId(id));
		}
		marshall(ids, response);
	}

	protected void doGetPositions(final HttpServletResponse response) {
		final Set<IEntityId> source = getIds();
		final Map<JsonEntityId, JsonAbsolutePosition> positions = new HashMap<JsonEntityId, JsonAbsolutePosition>();
		for (final IEntityId id : source) {
			positions.put(new JsonEntityId(id), new JsonAbsolutePosition(getAbsolutePosition(id)));
		}
		marshall(positions, response);
	}

	protected void doGetRelativeDistances(final IEntityId id, final HttpServletResponse response) {
		if (isIdValid(id)) {
			final IEntity entity = getEntity(id);
			final Collection<IEntityRelativeDistance> distances = entity.getRelativeDistances();
			final List<JsonEntityRelativeDistance> output;
			synchronized (distances) {
				output = new ArrayList<JsonEntityRelativeDistance>(distances.size());
				for (final IEntityRelativeDistance dist : distances) {
					output.add(new JsonEntityRelativeDistance(dist));
				}
			}
			marshall(output, response);
		} else {
			try {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			} catch (final IOException e) {
				L.e(e);
			}
		}
	}

	protected void doGetWebPage(final HttpServletResponse response) throws IOException {
		final Writer writer = response.getWriter();
		writer.write("<!DOCTYPE html><html><head><title>GCiatto's thesis</title></head><body><h1>Registered entities:</h1>");
		final Set<IEntityId> ids = getIds();
		if (ids.size() > 0) {
			for (final IEntityId id : ids) {
				writer.write(String.format("<p>%s</p>", getEntity(id)).replaceAll("\n", "<br/>"));
			}
		} else {
			writer.write("<p>EMPTY</p>");
		}
		writer.write("</body><html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		final String what = request.getParameter(Protocol.PARAM_WHAT);
		final String idString = request.getParameter(Protocol.PARAM_ENTITY_ID);
		final IEntityId id = EntityId.fromURICharSeq(idString);
		L.i("POST What=%s Entity=%s", what, idString);
		if (what == null) {
			response.sendError(HttpServletResponse.SC_EXPECTATION_FAILED);
		} else if (Protocol.what.VALUE_DATA.equalsIgnoreCase(what)) {
			doAddData(id, request, response);
		} else if (Protocol.what.VALUE_DISTANCE.equalsIgnoreCase(what)) {
			doAddRelativeDistance(id, request, response);
		} else if (Protocol.what.VALUE_DEVICE.equalsIgnoreCase(what)) {
			doRegisterDevice(id, request, response);
		} else if (Protocol.what.VALUE_ID.equalsIgnoreCase(what)) {
			doRegisterEntity(id, response);
		} else if (Protocol.what.VALUE_POSITION.equalsIgnoreCase(what)) {
			doFixAbsolutePosition(id, request, response);
		} else {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}

	}

	protected void doRegisterDevice(final IEntityId id, final HttpServletRequest request, final HttpServletResponse response) {
		final IEntity entity = eventuallyRegisterEntity(id);
		final JSONObject obj = unmarshallJsonObject(request);
		entity.associateDevice(new JsonDevice(obj));
	}

	protected void doRegisterEntity(final IEntityId id, final HttpServletResponse response) {
		eventuallyRegisterEntity(id);
	}

	protected void doReset(final HttpServletRequest request, final HttpServletResponse response) {
		synchronized (mData) {
			mData.clear();
			mData.put(mTestEntity.getId(), mTestEntity);
			mData.put(mBeacon1.getId(), mBeacon1);
			mData.put(mBeacon2.getId(), mBeacon2);
			mData.put(mBeacon3.getId(), mBeacon3);
//			mData.put(mBeacon4.getId(), mBeacon4);
//			mData.put(mBeacon5.getId(), mBeacon5);
		}
	}

	protected IEntity eventuallyRegisterEntity(final IEntityId id) {
		final IEntity output;
		synchronized (mData) {
			if (mData.containsKey(id)) {
				output = mData.get(id);
			} else {
				output = new Entity(id);
				mData.put(id, output);
			}
		}
		return output;
	}

	protected IAbsolutePosition getAbsolutePosition(final IEntityId id) {
		return getEntity(id).getAbsolutePosition();
	}

	protected IEntity getEntity(final IEntityId id) {
		synchronized (mData) {
			return mData.get(id);
		}
	}

	protected Set<IEntityId> getIds() {
		synchronized (mData) {
			return new HashSet<IEntityId>(mData.keySet());
		}
	}

	protected boolean isIdValid(final IEntityId id) {
		L.i("received_entity.id.hashcode = %s", id.hashCode());
		synchronized (mData) {
			final boolean result = mData.containsKey(id);
			return result;
		}
	}

}
