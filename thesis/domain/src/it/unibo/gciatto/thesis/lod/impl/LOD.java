package it.unibo.gciatto.thesis.lod.impl;

import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.lod.IMutableLevelOfDetail;

public class LOD implements IMutableLevelOfDetail {

	/**
	 *
	 */
	private static final long serialVersionUID = -6255672519043666049L;
	private int mValue = LOD_POINT_OF_INTEREST;

	public LOD() {

	}

	public LOD(final ILevelOfDetail toClone) {
		setValue(toClone.getValue());
	}

	public LOD(final int value) {
		setValue(value);
	}

	@Override
	public int compareTo(final ILevelOfDetail o) {
		return Integer.compare(mValue, o.getValue());
	}

	@Override
	public boolean equals(final int val) {
		return mValue == val;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ILevelOfDetail)) {
			return false;
		}
		final ILevelOfDetail other = (ILevelOfDetail) obj;
		if (mValue != other.getValue()) {
			return false;
		}
		return true;
	}

	@Override
	public int getValue() {
		return mValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mValue;
		return result;
	}

	@Override
	public boolean isBTLocalizationConvenient() {
		final boolean cond = getValue() <= LOD_BT_LIMIT;
		return cond;
	}

	@Override
	public boolean isGPSLocalizationConvenient() {
		final boolean cond = getValue() <= LOD_GPS_LIMIT;
		return cond;
	}

	@Override
	public boolean isInsideView() {
		return mValue == LOD_INSIDE_VIEW;
	}

	@Override
	public boolean isInvisible() {
		return getValue() == LOD_INVISIBLE;
	}

	@Override
	public boolean isVisible() {
		return getValue() != LOD_INVISIBLE;
	}

	@Override
	public IMutableLevelOfDetail setValue(final int value) {
		if (value <= 0) {
			mValue = 0;
		} else if (value >= 6) {
			mValue = 6;
		} else {
			mValue = value;
		}
		return this;
	}

	@Override
	public IMutableLevelOfDetail setValueFromDistance(final double value) {
		// TODO implement
		throw new IllegalStateException("not implemented");
	}

	@Override
	public String toString() {
		return getValueName() + " [" + mValue + "]";
	}

	@Override
	public String getValueName() {
		switch (mValue) {
			case LOD_BT_LIMIT:
				return "LOD_BT_LIMIT";
			case LOD_GPS_LIMIT:
				return "LOD_GPS_LIMIT";
			case LOD_INSIDE_INTERESTING_AREA:
				return "LOD_INSIDE_INTERESTING_AREA";
			case LOD_INSIDE_VIEW:
				return "LOD_INSIDE_VIEW";
			case LOD_INVISIBLE:
				return "LOD_INVISIBLE";
			case LOD_NEAR:
				return "LOD_NEAR";
			case LOD_POINT_OF_INTEREST:
				return "LOD_POINT_OF_INTEREST";
			default:
				return null;
		}
	}
	
	

}
