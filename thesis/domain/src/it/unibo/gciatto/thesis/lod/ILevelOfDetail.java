package it.unibo.gciatto.thesis.lod;

import java.io.Serializable;

public interface ILevelOfDetail extends Comparable<ILevelOfDetail>, Serializable, Cloneable {
	static final int LOD_INVISIBLE = 0;
	static final int LOD_POINT_OF_INTEREST = 1;
	static final int LOD_GPS_LIMIT = 2;
	static final int LOD_NEAR = 3;
	static final int LOD_INSIDE_INTERESTING_AREA = 4;
	static final int LOD_BT_LIMIT = 5;
	static final int LOD_INSIDE_VIEW = 6;

	boolean equals(int val);

	@Override
	boolean equals(Object obj);

	int getValue();
	
	String getValueName();

	boolean isBTLocalizationConvenient();

	boolean isGPSLocalizationConvenient();

	boolean isInsideView();

	boolean isInvisible();

	boolean isVisible();
}
