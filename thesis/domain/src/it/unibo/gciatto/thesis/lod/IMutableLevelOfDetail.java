package it.unibo.gciatto.thesis.lod;

public interface IMutableLevelOfDetail extends ILevelOfDetail {
	IMutableLevelOfDetail setValue(int value);

	IMutableLevelOfDetail setValueFromDistance(double value);
}
