package it.unibo.gciatto.thesis.position;

public interface IMutableRelativeDistance extends IRelativeDistance {
	IMutableRelativeDistance setDistance(double value);

	IMutableRelativeDistance setRelativeTo(IAbsolutePosition value);
}
