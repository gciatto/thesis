package it.unibo.gciatto.thesis.position;

public interface IRelative {
	IAbsolutePosition getRelativeTo();
}
