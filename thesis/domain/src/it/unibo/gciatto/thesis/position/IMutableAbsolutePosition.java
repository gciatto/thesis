package it.unibo.gciatto.thesis.position;

import java.util.Date;

public interface IMutableAbsolutePosition extends IAbsolutePosition {
	IMutableAbsolutePosition removeAccurancy();

	IMutableAbsolutePosition removeAltitude();

	IMutableAbsolutePosition removeBearing();

	IMutableAbsolutePosition removeSpeed();

	IMutableAbsolutePosition setAccurancy(double value);

	IMutableAbsolutePosition setAltitude(double value);

	IMutableAbsolutePosition setBearing(double value);

	IMutableAbsolutePosition setLatitude(double value);

	IMutableAbsolutePosition setLongitude(double value);

	IMutableAbsolutePosition setSpeed(double value);

	// IMutableAbsolutePosition removeLatitude(double value);

	// IMutableAbsolutePosition removeLongitude(double value);

	IMutableAbsolutePosition setTimestamp(Date value);

	// IMutableAbsolutePosition removeTimestamp(Date value);
}
