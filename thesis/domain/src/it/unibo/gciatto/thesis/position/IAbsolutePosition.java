package it.unibo.gciatto.thesis.position;

import java.io.Serializable;
import java.util.Date;

public interface IAbsolutePosition extends Serializable, Cloneable {
	double distanceTo(IAbsolutePosition other);

	@Override
	boolean equals(Object other);

	double getAccurancy();

	double getAltitude();

	double getBearing();

	double getLatitude();

	double getLongitude();

	double getSpeed();

	Date getTimestamp();

	boolean isAccurancySet();

	boolean isAltitudeSet();

	boolean isBearingSet();

	boolean isSpeedSet();

	// boolean isLatitudeSet();

	// boolean isLongitudeSet();

	IRelativeDistance relativeDistanceTo(IAbsolutePosition other);
}
