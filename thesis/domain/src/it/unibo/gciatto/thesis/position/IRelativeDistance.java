package it.unibo.gciatto.thesis.position;

import java.io.Serializable;

public interface IRelativeDistance extends IRelative, Serializable, Cloneable {
	double getDistance();
}
