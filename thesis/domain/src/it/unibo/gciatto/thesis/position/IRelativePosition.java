package it.unibo.gciatto.thesis.position;

import java.io.Serializable;

public interface IRelativePosition extends IRelative, Serializable, Cloneable {
	double getLatitudeDelta();

	double getLongitudeDelta();

	IRelativeDistance getRelativeDistance();

	IAbsolutePosition toAbsolute();
};