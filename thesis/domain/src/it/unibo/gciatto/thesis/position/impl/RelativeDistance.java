package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IMutableRelativeDistance;
import it.unibo.gciatto.thesis.position.IRelativeDistance;
import it.unibo.gciatto.thesis.position.IRelativePosition;

public class RelativeDistance implements IMutableRelativeDistance {

	/**
	 *
	 */
	private static final long serialVersionUID = 1092128499436244986L;
	private IAbsolutePosition mRelativeTo;
	private double mDistance = 0d;

	public RelativeDistance(final IAbsolutePosition relativeTo) {
		mRelativeTo = relativeTo;
	}

	public RelativeDistance(final IAbsolutePosition relativeTo, final double distance) {
		mRelativeTo = relativeTo;
		mDistance = distance;
	}

	public RelativeDistance(final IAbsolutePosition source, final IAbsolutePosition destination) {
		this(source, source.distanceTo(destination));
	}

	public RelativeDistance(final IRelativeDistance toClone) {
		mRelativeTo = toClone.getRelativeTo();
		mDistance = toClone.getDistance();
	}

	public RelativeDistance(final IRelativePosition relPos) {
		this(relPos.getRelativeTo(), relPos.toAbsolute());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IRelativeDistance)) {
			return false;
		}
		final IRelativeDistance other = (IRelativeDistance) obj;
		if (Double.doubleToLongBits(getDistance()) != Double.doubleToLongBits(other.getDistance())) {
			return false;
		}
		if (getRelativeTo() == null) {
			if (other.getRelativeTo() != null) {
				return false;
			}
		} else if (!getRelativeTo().equals(other.getRelativeTo())) {
			return false;
		}
		return true;
	}

	@Override
	public double getDistance() {
		return mDistance;
	}

	@Override
	public IAbsolutePosition getRelativeTo() {
		return mRelativeTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(mDistance);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + (mRelativeTo == null ? 0 : mRelativeTo.hashCode());
		return result;
	}

	@Override
	public IMutableRelativeDistance setDistance(final double value) {
		mDistance = value;
		return this;
	}

	@Override
	public IMutableRelativeDistance setRelativeTo(final IAbsolutePosition value) {
		mRelativeTo = value;
		return this;
	}

	@Override
	public String toString() {
		return String.format("%s [RelativeTo=%s, Distance=%s]", getClass().getSimpleName(), mRelativeTo, mDistance);
	}

}
