package it.unibo.gciatto.thesis.position;

public interface IMutableRelativePosition extends IRelativePosition {
	IMutableRelativePosition add(double deltaLat, double deltaLong);

	IMutableRelativePosition add(IRelativePosition value);

	IMutableRelativePosition setLatitudeDelta(double value);

	IMutableRelativePosition setLongitudeDelta(double value);

	IMutableRelativePosition setRelativeTo(IAbsolutePosition value);

	IMutableRelativePosition sub(double deltaLat, double deltaLong);

	IMutableRelativePosition sub(IRelativePosition value);
}
