package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IMutableRelativePosition;
import it.unibo.gciatto.thesis.position.IRelativeDistance;
import it.unibo.gciatto.thesis.position.IRelativePosition;

public class RelativePosition implements IMutableRelativePosition {

	/**
	 *
	 */
	private static final long serialVersionUID = -2248205188466676151L;
	private IAbsolutePosition mRelativeTo;
	private double mLatDelta = 0d;
	private double mLongDelta = 0d;

	public RelativePosition(final IAbsolutePosition relativeTo) {
		mRelativeTo = relativeTo;
	}

	public RelativePosition(final IAbsolutePosition relativeTo, final double deltaLatitude, final double deltaLongitude) {
		mRelativeTo = relativeTo;
		setLatitudeDelta(deltaLatitude);
		setLongitudeDelta(deltaLongitude);
	}

	public RelativePosition(final IAbsolutePosition source, final IAbsolutePosition destination) {
		this(source, destination.getLatitude() - source.getLatitude(), destination.getLongitude() - source.getLongitude());
	}

	@Override
	public IMutableRelativePosition add(final double deltaLat, final double deltaLong) {
		mLatDelta += deltaLat;
		mLongDelta += deltaLong;
		return this;
	}

	@Override
	public IMutableRelativePosition add(final IRelativePosition value) {
		mLatDelta += value.getLatitudeDelta();
		mLongDelta += value.getLongitudeDelta();
		return this;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IRelativePosition)) {
			return false;
		}
		final IRelativePosition other = (IRelativePosition) obj;
		if (Double.doubleToLongBits(getLatitudeDelta()) != Double.doubleToLongBits(other.getLatitudeDelta())) {
			return false;
		}
		if (Double.doubleToLongBits(getLongitudeDelta()) != Double.doubleToLongBits(other.getLongitudeDelta())) {
			return false;
		}
		if (getRelativeTo() == null) {
			if (other.getRelativeTo() != null) {
				return false;
			}
		} else if (!getRelativeTo().equals(other.getRelativeTo())) {
			return false;
		}
		return true;
	}

	@Override
	public double getLatitudeDelta() {
		return mLatDelta;
	}

	@Override
	public double getLongitudeDelta() {
		return mLongDelta;
	}

	@Override
	public IRelativeDistance getRelativeDistance() {
		return mRelativeTo.relativeDistanceTo(toAbsolute());
	}

	@Override
	public IAbsolutePosition getRelativeTo() {
		return mRelativeTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(mLatDelta);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(mLongDelta);
		result = prime * result + (int) (temp ^ temp >>> 32);
		result = prime * result + (mRelativeTo == null ? 0 : mRelativeTo.hashCode());
		return result;
	}

	@Override
	public IMutableRelativePosition setLatitudeDelta(final double value) {
		mLatDelta = value;
		return this;
	}

	@Override
	public IMutableRelativePosition setLongitudeDelta(final double value) {
		mLongDelta = value;
		return this;
	}

	@Override
	public IMutableRelativePosition setRelativeTo(final IAbsolutePosition value) {
		mRelativeTo = value;
		return this;
	}

	@Override
	public IMutableRelativePosition sub(final double deltaLat, final double deltaLong) {
		mLatDelta -= deltaLat;
		mLongDelta -= deltaLong;
		return this;
	}

	@Override
	public IMutableRelativePosition sub(final IRelativePosition value) {
		mLatDelta -= value.getLatitudeDelta();
		mLongDelta -= value.getLongitudeDelta();
		return this;
	}

	@Override
	public IAbsolutePosition toAbsolute() {
		return new AbsolutePosition(mRelativeTo.getLatitude() + mLatDelta, mRelativeTo.getLongitude() + mLongDelta);
	}

	@Override
	public String toString() {
		return String.format("%s [RelativeTo=%s, LatitudeDelta=%s, LongitudeDelta=%s]", getClass().getSimpleName(), mRelativeTo, mLatDelta, mLongDelta);
	}

}
