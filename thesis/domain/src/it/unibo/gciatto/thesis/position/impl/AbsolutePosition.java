package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IMutableAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativeDistance;
import it.unibo.gciatto.util.impl.MathUtils;
import it.unibo.gciatto.util.impl.MathUtils.rand;

import java.util.Date;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

public class AbsolutePosition implements IMutableAbsolutePosition {
	public static AbsolutePosition getRandomAbsolutePosition() {
		return (AbsolutePosition) new AbsolutePosition(rand.randomPositiveDouble(90d), rand.randomPositiveDouble(180d), rand.randomPositiveDouble(100d)).setAccurancy(rand.randomPositiveDouble(30d)).setBearing(rand.randomPositiveDouble(360d)).setSpeed(rand.randomPositiveDouble(10d));
	}

	public static void main(final String[] args) {
		final double[] b1 = new double[] { 37.99613116918291, 15.4123366815009 };
		final double[] b2 = new double[] { 37.996028, 15.412364 };

		System.out.println(String.format("%s meteres from %s to %s", MathUtils.geo.distance(b1[0], b1[1], b2[0], b2[1], 70.0), b1, b2));
	}

	protected static LatLng toLatLng(final IAbsolutePosition pos) {
		return new LatLng(pos.getLatitude(), pos.getLongitude());
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 6843396619734378309L;
	private double mAccurancy = Double.NaN;
	private double mAltitude = Double.NaN;
	private double mBearing = Double.NaN;
	private double mLongitude = 0d;
	private double mLatitude = 0d;

	private double mSpeed = Double.NaN;

	private Date mTimestamp = new Date();

	public AbsolutePosition() {

	}

	public AbsolutePosition(final double latitude, final double longitude) {
		mLatitude = latitude;
		mLongitude = longitude;
	}

	public AbsolutePosition(final double latitude, final double longitude, final double altitude) {
		this(latitude, longitude);
		mAltitude = altitude;
	}

	public AbsolutePosition(final IAbsolutePosition toClone) {
		mAccurancy = toClone.getAccurancy();
		mAltitude = toClone.getAltitude();
		mLongitude = toClone.getLongitude();
		mLatitude = toClone.getLatitude();
		mTimestamp = toClone.getTimestamp();
	}

	@Override
	public double distanceTo(final IAbsolutePosition other) {
		return LatLngTool.distance(toLatLng(this), toLatLng(other), LengthUnit.METER);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IAbsolutePosition)) {
			return false;
		}
		final IAbsolutePosition other = (IAbsolutePosition) obj;
		if (Double.doubleToLongBits(getLatitude()) != Double.doubleToLongBits(other.getLatitude())) {
			return false;
		}
		if (Double.doubleToLongBits(getLongitude()) != Double.doubleToLongBits(other.getLongitude())) {
			return false;
		}
		return true;
	}

	@Override
	public double getAccurancy() {
		return mAccurancy;
	}

	@Override
	public double getAltitude() {
		return mAltitude;
	}

	@Override
	public double getBearing() {
		return mBearing;
	}

	@Override
	public double getLatitude() {
		return mLatitude;
	}

	@Override
	public double getLongitude() {
		return mLongitude;
	}

	@Override
	public double getSpeed() {
		return mSpeed;
	}

	@Override
	public Date getTimestamp() {
		return mTimestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(mLatitude);
		result = prime * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(mLongitude);
		result = prime * result + (int) (temp ^ temp >>> 32);
		return result;
	}

	@Override
	public boolean isAccurancySet() {
		return !Double.isNaN(mAccurancy);
	}

	@Override
	public boolean isAltitudeSet() {
		return !Double.isNaN(mAltitude);
	}

	@Override
	public boolean isBearingSet() {
		return !Double.isNaN(mBearing);
	}

	@Override
	public boolean isSpeedSet() {
		return !Double.isNaN(mSpeed);
	}

	@Override
	public IRelativeDistance relativeDistanceTo(final IAbsolutePosition other) {
		return new RelativeDistance(other, distanceTo(other));
	}

	@Override
	public IMutableAbsolutePosition removeAccurancy() {
		mAccurancy = Double.NaN;
		return this;
	}

	@Override
	public IMutableAbsolutePosition removeAltitude() {
		mAltitude = Double.NaN;
		return this;
	}

	@Override
	public IMutableAbsolutePosition removeBearing() {
		mBearing = Double.NaN;
		return this;
	}

	@Override
	public IMutableAbsolutePosition removeSpeed() {
		mSpeed = Double.NaN;
		return this;
	}

	@Override
	public AbsolutePosition setAccurancy(final double accurancy) {
		mAccurancy = accurancy;
		return this;
	}

	@Override
	public AbsolutePosition setAltitude(final double altitude) {
		mAltitude = altitude;
		return this;
	}

	@Override
	public IMutableAbsolutePosition setBearing(final double value) {
		mBearing = value;
		return this;
	}

	@Override
	public AbsolutePosition setLatitude(final double latitude) {
		mLatitude = latitude;
		return this;
	}

	@Override
	public AbsolutePosition setLongitude(final double longitude) {
		mLongitude = longitude;
		return this;
	}

	@Override
	public IMutableAbsolutePosition setSpeed(final double value) {
		mSpeed = value;
		return this;
	}

	@Override
	public IMutableAbsolutePosition setTimestamp(final Date value) {
		mTimestamp = value;
		return this;
	}

	@Override
	public String toString() {
		return String.format("%s [Accurancy=%s, Altitude=%s, Longitude=%s, Latitude=%s, Bearing=%s, Speed=%s, Timestamp=%s]", getClass().getSimpleName(), getAccurancy(), getAltitude(), getLongitude(), getLatitude(), getBearing(), getSpeed(), getTimestamp());
	}

}
