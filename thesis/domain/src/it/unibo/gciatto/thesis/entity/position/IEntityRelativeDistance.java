package it.unibo.gciatto.thesis.entity.position;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.position.IRelativeDistance;

public interface IEntityRelativeDistance extends IRelativeDistance {
	IEntity getRelativeToEntity();

	boolean isRegion();

	IEntityRelativeDistance setRegion(boolean value);

	IEntityRelativeDistance setRelativeToEntity(IEntity entity);
}
