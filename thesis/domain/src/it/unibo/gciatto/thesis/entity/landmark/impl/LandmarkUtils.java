package it.unibo.gciatto.thesis.entity.landmark.impl;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.landmark.ILandmark;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class LandmarkUtils {

	public static int countCommonLandmarks(final Collection<? extends IEntityRelativeDistance> distances1, final Collection<? extends IEntityRelativeDistance> distances2) {
		int count = 0;
		for (final IEntityRelativeDistance d : distances1) {
			if (isLandmark(d.getRelativeToEntity()) && distances2.contains(d)) {
				count++;
			}
		}
		return count;
	}

	public static int countLandmarks(final Collection<? extends IEntity> entities) {
		int count = 0;
		for (final IEntity e : entities) {
			if (isLandmark(e)) {
				count++;
			}
		}
		return count;
	}

	public static int countLandmarksDistances(final Collection<? extends IEntityRelativeDistance> distances) {
		int count = 0;
		for (final IEntityRelativeDistance d : distances) {
			if (isLandmark(d.getRelativeToEntity())) {
				count++;
			}
		}
		return count;
	}

	public static List<ILandmark> getCommonLandmarks(final Collection<? extends IEntityRelativeDistance> distances1, final Collection<? extends IEntityRelativeDistance> distances2) {
		final LinkedList<ILandmark> landmarks = new LinkedList<ILandmark>();
		for (final IEntityRelativeDistance d : distances1) {
			if (isLandmark(d.getRelativeToEntity()) && distances2.contains(d)) {
				landmarks.add(d.getRelativeToEntity() instanceof ILandmark ? (ILandmark) d.getRelativeToEntity() : new LandmarkDecorator(d.getRelativeToEntity()));
			}
		}
		return landmarks;
	}

	public static List<ILandmark> getLandmarks(final Collection<? extends IEntity> entities) {
		final LinkedList<ILandmark> landmarks = new LinkedList<ILandmark>();
		for (final IEntity e : entities) {
			if (isLandmark(e)) {
				if (e instanceof ILandmark) {
					landmarks.add((ILandmark) e);
				} else {
					landmarks.add(new LandmarkDecorator(e));
				}
			}
		}
		return landmarks;
	}

	public static List<ILandmark> getLandmarksFromDistances(final Collection<? extends IEntityRelativeDistance> distances) {
		final LinkedList<ILandmark> landmarks = new LinkedList<ILandmark>();
		for (final IEntityRelativeDistance d : distances) {
			if (isLandmark(d.getRelativeToEntity())) {
				landmarks.add(d.getRelativeToEntity() instanceof ILandmark ? (ILandmark) d.getRelativeToEntity() : new LandmarkDecorator(d.getRelativeToEntity()));
			}
		}
		return landmarks;
	}

	public static boolean isLandmark(final IEntity entity) {
		return entity.hasKey(ILandmark.ATTR_IS_LANDMARK) && entity.getBooleanAttribute(ILandmark.ATTR_IS_LANDMARK);
	}
}
