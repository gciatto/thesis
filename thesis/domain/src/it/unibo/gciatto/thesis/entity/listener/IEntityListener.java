package it.unibo.gciatto.thesis.entity.listener;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

public interface IEntityListener {
	void onAbsolutePositionFix(IEntity source, IAbsolutePosition newPosition, IAbsolutePosition oldPosition);

	void onAttributesChanged(IEntity source);

	void onCanSee(IEntity source, IEntityRelativeDistance distance);

	void onCantSeeAnymore(IEntity source, IEntity other);

	void onDeviceAssociated(IEntity source, IDevice device);

	void onDeviceDissociated(IEntity source, IDevice device);

	void onGettingCloser(IEntity source, IEntityRelativeDistance other);

	void onGettingFarther(IEntity source, IEntityRelativeDistance other);

	void onLODChanged(IEntity source, ILevelOfDetail newLOD, ILevelOfDetail oldLOD);

	void onRelativeDistanceAdded(IEntity source, IEntityRelativeDistance distance);

	void onRelativeDistanceRemoved(IEntity source, IEntityRelativeDistance distance);
}
