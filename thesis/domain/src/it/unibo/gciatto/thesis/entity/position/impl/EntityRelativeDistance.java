package it.unibo.gciatto.thesis.entity.position.impl;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.position.impl.RelativeDistance;

public class EntityRelativeDistance extends RelativeDistance implements IEntityRelativeDistance {

	/**
	 *
	 */
	private static final long serialVersionUID = 8780401717186053859L;
	private IEntity mRelativeToEntity;
	private boolean mRegion = false;

	public EntityRelativeDistance(final IEntity relativeTo) {
		super(relativeTo.getAbsolutePosition());
		setRelativeToEntity(relativeTo);
	}

	public EntityRelativeDistance(final IEntity relativeTo, final double distance) {
		super(relativeTo.getAbsolutePosition(), distance);
		setRelativeToEntity(relativeTo);
	}

	public EntityRelativeDistance(final IEntity source, final IEntity destination) {
		super(source.getAbsolutePosition(), destination.getAbsolutePosition());
		setRelativeToEntity(destination);
	}

	public EntityRelativeDistance(final IEntityRelativeDistance toClone) {
		this(toClone.getRelativeToEntity(), toClone.getDistance());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IEntityRelativeDistance)) {
			return false;
		}
		final IEntityRelativeDistance other = (IEntityRelativeDistance) obj;
		if (getRelativeToEntity() == null) {
			if (other.getRelativeToEntity() != null) {
				return false;
			}
		} else if (!getRelativeToEntity().equals(other.getRelativeToEntity())) {
			return false;
		}
		return true;
	}

	@Override
	public IEntity getRelativeToEntity() {
		return mRelativeToEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mRelativeToEntity == null ? 0 : mRelativeToEntity.hashCode());
		return result;
	}

	@Override
	public boolean isRegion() {
		return mRegion;
	}

	@Override
	public IEntityRelativeDistance setRegion(final boolean value) {
		mRegion = value;
		return this;
	}

	@Override
	public IEntityRelativeDistance setRelativeToEntity(final IEntity entity) {
		mRelativeToEntity = entity;
		setRelativeTo(entity.getAbsolutePosition());
		return this;
	}

	@Override
	public String toString() {
		return String.format("%s [RelativeTo=%s, Region=%s, Distance=%s]", getClass().getSimpleName(), getRelativeToEntity().getId(), isRegion(), getDistance());
	}

}
