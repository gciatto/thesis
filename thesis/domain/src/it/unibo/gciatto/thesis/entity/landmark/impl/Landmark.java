package it.unibo.gciatto.thesis.entity.landmark.impl;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.impl.Entity;
import it.unibo.gciatto.thesis.entity.landmark.ILandmark;

public class Landmark extends Entity implements ILandmark {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public Landmark(final IEntity toClone) {
		super(toClone);
		putBooleanAttribute(ATTR_IS_LANDMARK, true);
	}

	public Landmark(final IEntityId id) {
		super(id);
		putBooleanAttribute(ATTR_IS_LANDMARK, true);
	}

	@Override
	public boolean isLandmark() {
		return true;
	}

}
