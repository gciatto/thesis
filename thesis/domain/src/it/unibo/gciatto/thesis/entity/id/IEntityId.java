package it.unibo.gciatto.thesis.entity.id;

import java.io.Serializable;
import java.net.URI;

public interface IEntityId extends Serializable {
	static final CharSequence URI_SCHEME = "entity";

	@Override
	boolean equals(Object other);

	String getId();

	boolean isSameEntity(IEntityId otherId);

	URI toUri();
}
