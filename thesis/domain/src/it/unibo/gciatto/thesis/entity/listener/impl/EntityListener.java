package it.unibo.gciatto.thesis.entity.listener.impl;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

public class EntityListener implements IEntityListener {

	@Override
	public void onAbsolutePositionFix(final IEntity source, final IAbsolutePosition newPosition, final IAbsolutePosition oldPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAttributesChanged(final IEntity source) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCanSee(final IEntity source, final IEntityRelativeDistance distance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCantSeeAnymore(final IEntity source, final IEntity other) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDeviceAssociated(final IEntity source, final IDevice device) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDeviceDissociated(final IEntity source, final IDevice device) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGettingCloser(final IEntity source, final IEntityRelativeDistance other) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGettingFarther(final IEntity source, final IEntityRelativeDistance other) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLODChanged(final IEntity source, final ILevelOfDetail newLOD, final ILevelOfDetail oldLOD) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRelativeDistanceAdded(final IEntity source, final IEntityRelativeDistance distance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRelativeDistanceRemoved(final IEntity source, final IEntityRelativeDistance distance) {
		// TODO Auto-generated method stub

	}

}
