package it.unibo.gciatto.thesis.entity.impl;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.IMutableEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.EntityId;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.listener.impl.EntityListenerCollector;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.lod.IMutableLevelOfDetail;
import it.unibo.gciatto.thesis.lod.impl.LOD;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativePosition;
import it.unibo.gciatto.thesis.position.impl.AbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.RelativePosition;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.func.IBiConsumer;
import it.unibo.gciatto.util.impl.AttributeSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Entity implements IEntity {
	public static final double DIST_THRESHOLD = 0.01; // meters

	/**
	 *
	 */
	private static final long serialVersionUID = -2134822400468056534L;
	private final IMutableEntityId mId;
	private final List<IDevice> mDevices = new LinkedList<IDevice>();
	private final IAttributeSet mAttributes = new AttributeSet();
	private IAbsolutePosition mAbsPosition = new AbsolutePosition();

	private final Map<IEntity, IEntityRelativeDistance> mRelDistances = new HashMap<IEntity, IEntityRelativeDistance>();

	private IMutableLevelOfDetail mLOD = new LOD();
	private final EntityListenerCollector mListeners = new EntityListenerCollector(this);

	public Entity(final IEntity toClone) {
		this(toClone.getId());
		mAttributes.putAllAttributes(toClone);
		mDevices.addAll(toClone.getAssociatedDevices());
		for (final IEntityRelativeDistance d : toClone.getRelativeDistances()) {
			mRelDistances.put(d.getRelativeToEntity(), d);
		}
		mAbsPosition = new AbsolutePosition(toClone.getAbsolutePosition());
		mLOD.setValue(toClone.getLOD().getValue());
	}

	public Entity(final IEntityId id) {
		mId = new EntityId(id);
	}

	@Override
	public IEntity addEntityListener(final IEntityListener listener) {
		return mListeners.addEntityListener(listener);
	}

	@Override
	public IEntity addRelativeDistance(final IEntityRelativeDistance distance) {
		final boolean newDist;
		final IEntityRelativeDistance oldDist;
		synchronized (mRelDistances) {
			oldDist = mRelDistances.put(distance.getRelativeToEntity(), distance);
			newDist = oldDist == null;
		}
		mListeners.notifyRelativeDistanceAdded(this, distance);
		if (newDist) {
			mListeners.notifyCanSee(this, distance);
		}
		notifyDistanceVariation(oldDist, distance);
		return this;
	}

	@Override
	public IEntity associateDevice(final IDevice device) {
		synchronized (mDevices) {
			mDevices.add(device);
		}
		mListeners.notifyDeviceAssociated(this, device);
		return this;
	}

	@Override
	public IAttributeSet clearAllAttributes() {
		synchronized (mAttributes) {
			mAttributes.clearAllAttributes();
		}

		return this;
	}

	@Override
	public IEntity clearEntityListeners() {
		return mListeners.clearEntityListeners();
	}

	@Override
	protected IEntity clone() throws CloneNotSupportedException {
		return new Entity(this);
	}

	@Override
	public int countAttributes() {
		synchronized (mAttributes) {
			return mAttributes.countAttributes();
		}
	}

	@Override
	public IEntity disassociateDevice(final IDevice device) {
		synchronized (mDevices) {
			mDevices.remove(device);
		}
		mListeners.notifyDeviceDissociated(this, device);
		return this;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof IEntity)) {
			return false;
		}
		final IEntity other = (IEntity) obj;
		return getId().isSameEntity(other.getId());
	}

	@Override
	public IAbsolutePosition getAbsolutePosition() {
		synchronized (mAbsPosition) {
			return mAbsPosition;
		}
	}

	//
	// @Override
	// public IEntity fixRelativeDistance(final IEntityRelativeDistance
	// distance) {
	// mRelDistances.add(distance);
	// return this;
	// }

	@Override
	public Collection<IDevice> getAssociatedDevices() {
		return new ArrayList<IDevice>(mDevices);
	}

	@Override
	public Object getAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getAttribute(key);
		}
	}

	@Override
	public Set<String> getAttributeKeys() {
		synchronized (mAttributes) {
			return mAttributes.getAttributeKeys();
		}
	}

	@Override
	public IAttributeSet getAttributes() {
		synchronized (mAttributes) {
			return mAttributes;
		}
	}

	@Override
	public boolean getBooleanAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getBooleanAttribute(key);
		}
	}

	@Override
	public char getCharAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getCharAttribute(key);
		}
	}

	@Override
	public IEntityRelativeDistance getDistanceRelativeTo(final IEntity entity) {
		synchronized (mRelDistances) {
			return mRelDistances.get(entity);
		}
	}

	@Override
	public double getDoubleAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getDoubleAttribute(key);
		}
	}

	@Override
	public float getFloatAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getFloatAttribute(key);
		}
	}

	@Override
	public IEntityId getId() {
		return mId;
	}

	@Override
	public int getIntAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getIntAttribute(key);
		}
	}

	@Override
	public ILevelOfDetail getLOD() {
		synchronized (mLOD) {
			return mLOD;
		}
	}

	@Override
	public long getLongAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getLongAttribute(key);
		}
	}

	@Override
	public Collection<IEntityRelativeDistance> getRelativeDistances() {
		synchronized (mRelDistances) {
			return mRelDistances.values();
		}
	}

	@Override
	public IRelativePosition getRelativePositionFrom(final IEntity from) {
		return new RelativePosition(from.getAbsolutePosition(), getAbsolutePosition());
	}

	@Override
	public IRelativePosition getRelativePositionTo(final IEntity to) {
		return new RelativePosition(getAbsolutePosition(), to.getAbsolutePosition());
	}

	@Override
	public short getShortAttribute(final String key) {
		synchronized (mAttributes) {
			return mAttributes.getShortAttribute(key);
		}
	}

	@Override
	public String getStringAttribute(final String key) {
		return mAttributes.getStringAttribute(key);
	}

	@Override
	public boolean hasDistanceRelativeTo(final IEntity entity) {
		synchronized (mRelDistances) {
			return mRelDistances.containsKey(entity);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mId == null ? 0 : mId.hashCode());
		return result;
	}

	@Override
	public boolean hasKey(final String key) {
		synchronized (mAttributes) {
			return mAttributes.hasKey(key);
		}
	}

	@Override
	public boolean isSameEntity(final IEntity other) {
		return isSameEntity(other.getId());
	}

	@Override
	public boolean isSameEntity(final IEntityId otherID) {
		return getId().isSameEntity(otherID);
	}

	@Override
	public boolean matchesDevice(final IDevice device) {
		synchronized (mDevices) {
			for (final IDevice d : mDevices) {
				if (d.equals(device)) {
					return true;
				}
			}
			return false;
		}
	}

	@Override
	public IEntity move(final IAbsolutePosition destination) {
		setAbsolutePosition(destination);
		return this;
	}

	@Override
	public IEntity move(final IRelativePosition howMuch) {
		if (!howMuch.getRelativeTo().equals(getAbsolutePosition())) {
			throw new IllegalArgumentException();
		}
		setAbsolutePosition(howMuch.toAbsolute());
		return this;
	}

	protected void notifyDistanceVariation(final IEntityRelativeDistance oldDist, final IEntityRelativeDistance newDist) {
		if (oldDist == null || newDist == null) {
			return;
		}
		if (!newDist.getRelativeToEntity().isSameEntity(oldDist.getRelativeToEntity())) {
			throw new IllegalArgumentException();
		}
		final double variation = newDist.getDistance() - oldDist.getDistance();
		final double absVariation = Math.abs(variation);
		if (absVariation >= DIST_THRESHOLD) {
			if (variation >= 0) {
				mListeners.notifyGettingFarther(this, newDist);
			} else {
				mListeners.notifyGettingCloser(this, newDist);
			}
		}
	}

	@Override
	public IAttributeSet putAllAttributes(final IAttributeSet source) {
		synchronized (mAttributes) {
			mAttributes.putAllAttributes(source);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putAttribute(final String key, final Object value) {
		synchronized (mAttributes) {
			mAttributes.putAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putBooleanAttribute(final String key, final boolean value) {
		synchronized (mAttributes) {
			mAttributes.putBooleanAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putCharAttribute(final String key, final char value) {
		synchronized (mAttributes) {
			mAttributes.putCharAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putDoubleAttribute(final String key, final double value) {
		synchronized (mAttributes) {
			mAttributes.putDoubleAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putFloatAttribute(final String key, final float value) {
		synchronized (mAttributes) {
			mAttributes.putFloatAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putIntAttribute(final String key, final int value) {
		synchronized (mAttributes) {
			mAttributes.putIntAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putLongAttribute(final String key, final long value) {
		synchronized (mAttributes) {
			mAttributes.putLongAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putShortAttribute(final String key, final Short value) {
		synchronized (mAttributes) {
			mAttributes.putShortAttribute(key, value);
		}
		mListeners.notifyAttributesChanged(this);
		return this;
	}

	@Override
	public IAttributeSet putStringAttribute(final String key, final String value) {
		return mAttributes.putStringAttribute(key, value);
	}

	@Override
	public IEntity removeAllRelativeDistances() {
		final List<IEntityRelativeDistance> distances;
		synchronized (mRelDistances) {
			distances = new ArrayList<IEntityRelativeDistance>(mRelDistances.values());
		}
		for (final IEntityRelativeDistance d : distances) {
			removeRelativeDistance(d);
		}
		return this;
	}

	@Override
	public IAttributeSet removeAttribute(final String key) {
		synchronized (mAttributes) {
			mAttributes.removeAttribute(key);
		}
		return this;
	}

	@Override
	public IEntity removeEntityListener(final IEntityListener listener) {
		return mListeners.removeEntityListener(listener);
	}

	@Override
	public IEntity removeRelativeDistance(final IEntityRelativeDistance distance) {
		final boolean removed;
		final IEntityRelativeDistance oldDist;
		synchronized (mRelDistances) {
			oldDist = mRelDistances.remove(distance.getRelativeToEntity());
			removed = oldDist != null;
		}
		if (removed) {
			mListeners.notifyRelativeDistanceRemoved(this, distance);
			mListeners.notifyCantSeeAnymore(this, distance.getRelativeToEntity());
		}
		return this;
	}

	protected void setAbsolutePosition(final IAbsolutePosition absPosition) {
		final IAbsolutePosition oldPosition;
		synchronized (mAbsPosition) {
			oldPosition = mAbsPosition;
			mAbsPosition = absPosition;
		}
		mListeners.notifyAbsolutePositionFix(this, absPosition, oldPosition);
	}

	@Override
	public synchronized IEntity setLOD(final ILevelOfDetail value) {
		final ILevelOfDetail oldLOD;
		synchronized (mLOD) {
			oldLOD = mLOD;
			mLOD = new LOD(value);
		}
		mListeners.notifyLODChanged(this, value, oldLOD);
		return this;
	}

	@Override
	public synchronized IEntity setLOD(final int value) {
		return setLOD(new LOD(value));
	}

	@Override
	public Map<String, Object> toMap() {
		synchronized (mAttributes) {
			return mAttributes.toMap();
		}
	}

	@Override
	public String toString() {
		return String.format("%s [\n\tId=%s,\n\tAbsolutePosition=%s,\n\tAssociatedDevices=%s,\n\tAttributes=%s,\n\tLOD=%s,\n\tRelativeDistances=%s\n]", getClass().getSimpleName(), getId(), getAbsolutePosition(), getAssociatedDevices(), getAttributes(), getLOD(), getRelativeDistances());
	}

	@Override
	public IAttributeSet forEachAttribute(IBiConsumer<String, Object> consumer) {
		mAttributes.forEachAttribute(consumer);
		return this;
	}

}