package it.unibo.gciatto.thesis.entity.id.impl;

import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.IMutableEntityId;

import java.net.URI;
import java.net.URISyntaxException;

public class EntityId1 implements IMutableEntityId {

	public static EntityId fromURICharSeq(final CharSequence uri) {
		try {
			return new EntityId(new URI(uri.toString()));
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 2725986759223745862L;

	private URI mUri;

	public EntityId1(final IEntityId toClone) {
		mUri = toClone.toUri();
	}

	public EntityId1(final String id) {
		setId(id.toString());
	}

	public EntityId1(final URI uri) {
		mUri = uri;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof IEntityId) {
			final IEntityId other = (IEntityId) obj;
			return isSameEntity(other);
		} else {
			return false;
		}
	}

	@Override
	public String getId() {
		return mUri.getSchemeSpecificPart();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mUri == null ? 0 : mUri.hashCode());
		return result;
	}

	@Override
	public boolean isSameEntity(final IEntityId otherId) {
		return toUri().equals(otherId.toUri());
	}

	@Override
	public IMutableEntityId setId(final String value) {
		try {
			mUri = new URI(URI_SCHEME.toString(), value.toString(), null);
		} catch (final URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}
		return this;
	}

	@Override
	public String toString() {
		return mUri.toString();
	}

	@Override
	public URI toUri() {
		try {
			return new URI(mUri.toString());
		} catch (final URISyntaxException e) {
			return null;
		}
	}

}
