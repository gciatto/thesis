package it.unibo.gciatto.thesis.entity.landmark.impl;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.landmark.ILandmark;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativePosition;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.func.IBiConsumer;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class LandmarkDecorator implements ILandmark {

	/**
	 *
	 */
	private static final long serialVersionUID = -8559699146559618525L;
	private final IEntity mDecorated;

	public LandmarkDecorator(final IEntity toDecorate) {
		if (toDecorate.hasKey(ATTR_IS_LANDMARK) && toDecorate.getBooleanAttribute(ATTR_IS_LANDMARK)) {
			mDecorated = toDecorate;
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public IEntity addEntityListener(final IEntityListener listener) {
		return mDecorated.addEntityListener(listener);
	}

	@Override
	public IEntity addRelativeDistance(final IEntityRelativeDistance distance) {
		return mDecorated.addRelativeDistance(distance);
	}

	@Override
	public IEntity associateDevice(final IDevice device) {
		return mDecorated.associateDevice(device);
	}

	@Override
	public IAttributeSet clearAllAttributes() {
		return mDecorated.clearAllAttributes();
	}

	@Override
	public IEntity clearEntityListeners() {
		return mDecorated.clearEntityListeners();
	}

	@Override
	public int countAttributes() {
		return mDecorated.countAttributes();
	}

	@Override
	public IEntity disassociateDevice(final IDevice device) {
		return mDecorated.disassociateDevice(device);
	}

	@Override
	public boolean equals(final Object obj) {
		return mDecorated.equals(obj);
	}

	@Override
	public IAbsolutePosition getAbsolutePosition() {
		return mDecorated.getAbsolutePosition();
	}

	@Override
	public Collection<IDevice> getAssociatedDevices() {
		return mDecorated.getAssociatedDevices();
	}

	@Override
	public Object getAttribute(final String key) {
		return mDecorated.getAttribute(key);
	}

	@Override
	public Set<String> getAttributeKeys() {
		return mDecorated.getAttributeKeys();
	}

	@Override
	public IAttributeSet getAttributes() {
		return mDecorated.getAttributes();
	}

	@Override
	public boolean getBooleanAttribute(final String key) {
		return mDecorated.getBooleanAttribute(key);
	}

	@Override
	public char getCharAttribute(final String key) {
		return mDecorated.getCharAttribute(key);
	}

	@Override
	public IEntityRelativeDistance getDistanceRelativeTo(final IEntity entity) {
		return mDecorated.getDistanceRelativeTo(entity);
	}

	@Override
	public double getDoubleAttribute(final String key) {
		return mDecorated.getDoubleAttribute(key);
	}

	@Override
	public float getFloatAttribute(final String key) {
		return mDecorated.getFloatAttribute(key);
	}

	@Override
	public IEntityId getId() {
		return mDecorated.getId();
	}

	@Override
	public int getIntAttribute(final String key) {
		return mDecorated.getIntAttribute(key);
	}

	@Override
	public ILevelOfDetail getLOD() {
		return mDecorated.getLOD();
	}

	@Override
	public long getLongAttribute(final String key) {
		return mDecorated.getLongAttribute(key);
	}

	@Override
	public Collection<IEntityRelativeDistance> getRelativeDistances() {
		return mDecorated.getRelativeDistances();
	}

	@Override
	public IRelativePosition getRelativePositionFrom(final IEntity from) {
		return mDecorated.getRelativePositionFrom(from);
	}

	@Override
	public IRelativePosition getRelativePositionTo(final IEntity to) {
		return mDecorated.getRelativePositionTo(to);
	}

	@Override
	public short getShortAttribute(final String key) {
		return mDecorated.getShortAttribute(key);
	}

	@Override
	public String getStringAttribute(final String key) {
		return mDecorated.getStringAttribute(key);
	}

	@Override
	public boolean hasDistanceRelativeTo(final IEntity entity) {
		return mDecorated.hasDistanceRelativeTo(entity);
	}

	@Override
	public int hashCode() {
		return mDecorated.hashCode();
	}

	@Override
	public boolean hasKey(final String key) {
		return mDecorated.hasKey(key);
	}

	@Override
	public boolean isLandmark() {
		return true;
	}

	@Override
	public boolean isSameEntity(final IEntity other) {
		return mDecorated.isSameEntity(other);
	}

	@Override
	public boolean isSameEntity(final IEntityId otherID) {
		return mDecorated.isSameEntity(otherID);
	}

	@Override
	public boolean matchesDevice(final IDevice device) {
		return mDecorated.matchesDevice(device);
	}

	@Override
	public IEntity move(final IAbsolutePosition destination) {
		return mDecorated.move(destination);
	}

	@Override
	public IEntity move(final IRelativePosition howMuch) {
		return mDecorated.move(howMuch);
	}

	@Override
	public IAttributeSet putAllAttributes(final IAttributeSet source) {
		return mDecorated.putAllAttributes(source);
	}

	@Override
	public IAttributeSet putAttribute(final String key, final Object value) {
		return mDecorated.putAttribute(key, value);
	}

	@Override
	public IAttributeSet putBooleanAttribute(final String key, final boolean value) {
		return mDecorated.putBooleanAttribute(key, value);
	}

	@Override
	public IAttributeSet putCharAttribute(final String key, final char value) {
		return mDecorated.putCharAttribute(key, value);
	}

	@Override
	public IAttributeSet putDoubleAttribute(final String key, final double value) {
		return mDecorated.putDoubleAttribute(key, value);
	}

	@Override
	public IAttributeSet putFloatAttribute(final String key, final float value) {
		return mDecorated.putFloatAttribute(key, value);
	}

	@Override
	public IAttributeSet putIntAttribute(final String key, final int value) {
		return mDecorated.putIntAttribute(key, value);
	}

	@Override
	public IAttributeSet putLongAttribute(final String key, final long value) {
		return mDecorated.putLongAttribute(key, value);
	}

	@Override
	public IAttributeSet putShortAttribute(final String key, final Short value) {
		return mDecorated.putShortAttribute(key, value);
	}

	@Override
	public IAttributeSet putStringAttribute(final String key, final String value) {
		return mDecorated.putStringAttribute(key, value);
	}

	@Override
	public IEntity removeAllRelativeDistances() {
		return mDecorated.removeAllRelativeDistances();
	}

	@Override
	public IAttributeSet removeAttribute(final String key) {
		return mDecorated.removeAttribute(key);
	}

	@Override
	public IEntity removeEntityListener(final IEntityListener listener) {
		return mDecorated.removeEntityListener(listener);
	}

	@Override
	public IEntity removeRelativeDistance(final IEntityRelativeDistance distance) {
		return mDecorated.removeRelativeDistance(distance);
	}

	@Override
	public IEntity setLOD(final ILevelOfDetail value) {
		return mDecorated.setLOD(value);
	}

	@Override
	public IEntity setLOD(final int value) {
		return mDecorated.setLOD(value);
	}

	@Override
	public Map<String, Object> toMap() {
		return mDecorated.toMap();
	}

	@Override
	public String toString() {
		return String.format("%s [Adapted=%s]", getClass().getSimpleName(), mDecorated);
	}

	@Override
	public IAttributeSet forEachAttribute(IBiConsumer<String, Object> consumer) {
		return mDecorated.forEachAttribute(consumer);
	}

}