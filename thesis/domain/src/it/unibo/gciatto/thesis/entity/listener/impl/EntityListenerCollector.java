package it.unibo.gciatto.thesis.entity.listener.impl;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

import java.util.LinkedList;

public class EntityListenerCollector {
	private final IEntity mParent;
	private final LinkedList<IEntityListener> mListeners = new LinkedList<IEntityListener>();

	public EntityListenerCollector(final IEntity parent) {
		mParent = parent;
	}

	public IEntity addEntityListener(final IEntityListener listener) {
		mListeners.add(listener);
		return mParent;
	}

	public IEntity clearEntityListeners() {
		mListeners.clear();
		return mParent;
	}

	public void notifyAbsolutePositionFix(final IEntity source, final IAbsolutePosition newPosition, final IAbsolutePosition oldPosition) {
		for (final IEntityListener l : mListeners) {
			l.onAbsolutePositionFix(source, newPosition, oldPosition);
		}
	}

	public void notifyAttributesChanged(final IEntity source) {
		for (final IEntityListener l : mListeners) {
			l.onAttributesChanged(source);
		}
	}

	public void notifyCanSee(final IEntity source, final IEntityRelativeDistance distance) {
		for (final IEntityListener l : mListeners) {
			l.onCanSee(source, distance);
		}
	}

	public void notifyCantSeeAnymore(final IEntity source, final IEntity other) {
		for (final IEntityListener l : mListeners) {
			l.onCantSeeAnymore(source, other);
		}
	}

	public void notifyDeviceAssociated(final IEntity source, final IDevice device) {
		for (final IEntityListener l : mListeners) {
			l.onDeviceAssociated(source, device);
		}
	}

	public void notifyDeviceDissociated(final IEntity source, final IDevice device) {
		for (final IEntityListener l : mListeners) {
			l.onDeviceDissociated(source, device);
		}
	}

	public void notifyGettingCloser(final IEntity source, final IEntityRelativeDistance other) {
		for (final IEntityListener l : mListeners) {
			l.onGettingCloser(source, other);
		}
	}

	public void notifyGettingFarther(final IEntity source, final IEntityRelativeDistance other) {
		for (final IEntityListener l : mListeners) {
			l.onGettingFarther(source, other);
		}
	}

	public void notifyLODChanged(final IEntity source, final ILevelOfDetail newLOD, final ILevelOfDetail oldLOD) {
		for (final IEntityListener l : mListeners) {
			l.onLODChanged(source, newLOD, oldLOD);
		}
	}

	public void notifyRelativeDistanceAdded(final IEntity source, final IEntityRelativeDistance distance) {
		for (final IEntityListener l : mListeners) {
			l.onRelativeDistanceAdded(source, distance);
		}
	}

	public void notifyRelativeDistanceRemoved(final IEntity source, final IEntityRelativeDistance distance) {
		for (final IEntityListener l : mListeners) {
			l.onRelativeDistanceRemoved(source, distance);
		}
	}

	public IEntity removeEntityListener(final IEntityListener listener) {
		mListeners.remove(listener);
		return mParent;
	}
}
