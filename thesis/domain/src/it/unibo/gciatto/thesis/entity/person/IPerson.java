package it.unibo.gciatto.thesis.entity.person;

import it.unibo.gciatto.thesis.entity.IEntity;

public interface IPerson extends IEntity, IPersonalData {
	@Override
	IPersonalData getAttributes();
}
