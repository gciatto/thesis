package it.unibo.gciatto.thesis.entity.person;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.util.IAttributeSet;

public interface IPersonalData extends IAttributeSet {
	static final String ATTR_NAME = IDevice.class.getName() + ".name";

	String getName();
}
