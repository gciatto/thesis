package it.unibo.gciatto.thesis.entity.landmark;

import it.unibo.gciatto.thesis.entity.IEntity;

public interface ILandmark extends IEntity {
	public static final String ATTR_IS_LANDMARK = ILandmark.class.getName() + ".isLandmark";

	boolean isLandmark();
}
