package it.unibo.gciatto.thesis.entity.id;

public interface IMutableEntityId extends IEntityId {
	IMutableEntityId setId(String value);
}
