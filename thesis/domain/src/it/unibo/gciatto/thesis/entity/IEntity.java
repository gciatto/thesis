package it.unibo.gciatto.thesis.entity;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativePosition;
import it.unibo.gciatto.util.IAttributeSet;

import java.util.Collection;

public interface IEntity extends IAttributeSet {
	static final String ATTR_MESSAGE = IEntity.class.getName() + ".message";
	
	IEntity addEntityListener(IEntityListener listener);

	IEntity addRelativeDistance(IEntityRelativeDistance distance);

	IEntity associateDevice(IDevice device);

	IEntity clearEntityListeners();

	IEntity disassociateDevice(IDevice device);

	IAbsolutePosition getAbsolutePosition();

	Collection<IDevice> getAssociatedDevices();

	IAttributeSet getAttributes();

	IEntityRelativeDistance getDistanceRelativeTo(IEntity entity);

	IEntityId getId();

	ILevelOfDetail getLOD();

	Collection<IEntityRelativeDistance> getRelativeDistances();

	IRelativePosition getRelativePositionFrom(IEntity from);

	IRelativePosition getRelativePositionTo(IEntity to);

	boolean hasDistanceRelativeTo(IEntity entity);

	boolean isSameEntity(IEntity other);

	boolean isSameEntity(IEntityId otherID);

	boolean matchesDevice(IDevice device);

	IEntity move(IAbsolutePosition destination);

	IEntity move(IRelativePosition howMuch);

	IEntity removeAllRelativeDistances();

	IEntity removeEntityListener(IEntityListener listener);

	IEntity removeRelativeDistance(IEntityRelativeDistance distance);

	IEntity setLOD(ILevelOfDetail value);

	IEntity setLOD(int value);
}
