package it.unibo.gciatto.thesis.entity.id.impl;

import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.IMutableEntityId;

import java.net.URI;
import java.net.URISyntaxException;

public class EntityId implements IMutableEntityId {

	public static EntityId fromURICharSeq(final CharSequence uri) {
		try {
			return new EntityId(new URI(uri.toString()));
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 2725986759223745862L;

	private String mId;

	public EntityId(final IEntityId toClone) {
		mId = toClone.getId();
	}

	public EntityId(final String id) {
		setId(id.toString());
	}

	public EntityId(final URI uri) {
		if (uri.getScheme().equals(URI_SCHEME)) {
			mId = uri.getSchemeSpecificPart();
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof IEntityId) {
			final IEntityId other = (IEntityId) obj;
			return mId.equals(other.getId());
		} else {
			return false;
		}
	}

	@Override
	public String getId() {
		return mId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mId == null ? 0 : mId.hashCode());
		return result;
	}

	@Override
	public boolean isSameEntity(final IEntityId otherId) {
		return equals(otherId);
	}

	@Override
	public IMutableEntityId setId(final String value) {
		mId = value;
		return this;
	}

	@Override
	public String toString() {
		return toUri().toString();
	}

	@Override
	public URI toUri() {
		try {
			final URI uri = new URI(URI_SCHEME.toString(), mId, null);
			return uri;
		} catch (final URISyntaxException e) {
			return null;
		}
	}

}
