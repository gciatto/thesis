package it.unibo.gciatto.thesis.device.impl;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.device.IMutableDevice;
import it.unibo.gciatto.util.impl.AttributeSet;

public class Device extends AttributeSet implements IMutableDevice {

	public static Device getTestDevice() {
		return (Device) new Device("pc", "testDevice").setIp("127.0.0.1").setMac("aa:bb:cc:dd:ee:ff");
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1280458427688837113L;

	private String type, name;

	public Device() {
	}

	public Device(final IDevice toClone) {
		super(toClone);
	}

	public Device(final String type, final String name) {
		setName(name);
		setType(type);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof IDevice)) {
			return false;
		}
		final IDevice other = (IDevice) obj, biggest, smallest;
		if (countAttributes() >= other.countAttributes()) {
			biggest = this;
			smallest = other;
		} else {
			biggest = other;
			smallest = this;
		}
		for (final String key : smallest.getAttributeKeys()) {
			final boolean condition1 = biggest.hasKey(key);
			final Object value1 = biggest.getAttribute(key);
			final Object value2 = smallest.getAttribute(key);
			final boolean condition2 = value1 != null && value1.equals(value2);
			if (condition1 && condition2) {
				continue;
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	public String getIp() {
		return (String) getAttribute(ATTR_IP);
	}

	@Override
	public String getMac() {
		return (String) getAttribute(ATTR_MAC);
	}

	@Override
	public String getName() {
		return (String) getAttribute(ATTR_NAME);
	}

	@Override
	public String getType() {
		return (String) getAttribute(ATTR_TYPE);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (getName() == null ? 0 : getName().hashCode());
		result = prime * result + (getType() == null ? 0 : getType().hashCode());
		return result;
	}

	@Override
	public IMutableDevice setIp(final String value) {
		putAttribute(ATTR_IP, value);
		return this;
	}

	@Override
	public IMutableDevice setMac(final String value) {
		putAttribute(ATTR_MAC, value);
		return this;
	}

	@Override
	public IMutableDevice setName(final String value) {
		putAttribute(ATTR_NAME, value);
		return this;
	}

	@Override
	public IMutableDevice setType(final String value) {
		putAttribute(ATTR_TYPE, value);
		return this;
	}

	// @Override
	// public String toString() {
	// return String.format("%s [Ip=%s, Mac=%s, Name=%s, Type=%s]",
	// getClass().getSimpleName(), getIp(), getMac(), getName(), getType());
	// }

}
