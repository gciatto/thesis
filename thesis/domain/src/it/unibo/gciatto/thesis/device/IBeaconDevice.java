package it.unibo.gciatto.thesis.device;

public interface IBeaconDevice extends IDevice {
	public static final String ATTR_UUID = IBeaconDevice.class.getName() + ".uuid";
	public static final String ATTR_MAJOR = IBeaconDevice.class.getName() + ".major";
	public static final String ATTR_MINOR = IBeaconDevice.class.getName() + ".minor";

	public String getMajor();

	public String getMinor();

	public String getUUID();
}
