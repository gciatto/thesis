package it.unibo.gciatto.thesis.device;

public interface IMutableDevice extends IDevice {
	IMutableDevice setIp(String value);

	IMutableDevice setMac(String value);

	IMutableDevice setName(String value);

	IMutableDevice setType(String value);
}
