package it.unibo.gciatto.thesis.device;

public interface IMutableBeaconDevice extends IBeaconDevice {
	public IMutableBeaconDevice setMajor(String value);

	public IMutableBeaconDevice setMinor(String value);

	public IMutableBeaconDevice setUUID(String value);
}
