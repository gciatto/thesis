package it.unibo.gciatto.thesis.device;

import it.unibo.gciatto.util.IAttributeSet;

public interface IDevice extends IAttributeSet {
	static final String ATTR_TYPE = IDevice.class.getName() + ".type";
	static final String ATTR_NAME = IDevice.class.getName() + ".name";
	static final String ATTR_MAC = IDevice.class.getName() + ".mac";
	static final String ATTR_IP = IDevice.class.getName() + ".ip";

	static final String TYPE_PC = IDevice.class.getName() + ".pc";
	static final String TYPE_TABLET = IDevice.class.getName() + ".tablet";
	static final String TYPE_PHONE = IDevice.class.getName() + ".phone";
	static final String TYPE_ANDROID = IDevice.class.getName() + ".android";
	static final String TYPE_BEACON = IDevice.class.getName() + ".beacon";

	String getIp();

	String getMac();

	String getName();

	String getType();
	
	boolean equals(Object value);
}
