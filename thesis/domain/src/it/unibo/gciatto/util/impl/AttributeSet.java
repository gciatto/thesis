package it.unibo.gciatto.util.impl;

import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.func.IBiConsumer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AttributeSet implements IAttributeSet {

	public static String keyToString(final String key) {
		if (key.contains(".")) {
			return key.substring(key.lastIndexOf('.') + 1);
		} else {
			return key;
		}
	}

	public static String valueToString(final Object value) {
		final String converted = value.toString();
		if (converted.contains(".")) {
			return converted.substring(converted.lastIndexOf('.') + 1);
		} else {
			return converted;
		}
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 2898510461915481597L;

	private final Map<String, Object> mAttributes = new HashMap<String, Object>();

	public AttributeSet() {

	}

	public AttributeSet(final IAttributeSet toClone) {
		putAllAttributes(toClone);
	}

	public AttributeSet(final String firstKey, final Object firstValue) {
		putAttribute(firstKey, firstValue);
	}

	protected String attributesToString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (final String key : mAttributes.keySet()) {
			sb.append(keyToString(key));
			sb.append("=");
			sb.append(valueToString(mAttributes.get(key)));
			sb.append(", ");
		}
		if (mAttributes.size() > 0) {
			final int size = sb.length();
			sb.delete(size - 2, size - 1);
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	public IAttributeSet clearAllAttributes() {
		mAttributes.clear();
		return this;
	}

	@Override
	public int countAttributes() {
		return mAttributes.size();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AttributeSet)) {
			return false;
		}
		final AttributeSet other = (AttributeSet) obj;
		if (mAttributes == null) {
			if (other.mAttributes != null) {
				return false;
			}
		} else if (!mAttributes.equals(other.mAttributes)) {
			return false;
		}
		return true;
	}

	@Override
	public Object getAttribute(final String key) {
		return mAttributes.get(key);
	}

	@Override
	public Set<String> getAttributeKeys() {
		return mAttributes.keySet();
	}

	@Override
	public boolean getBooleanAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return Boolean.parseBoolean(attribute.toString());
		} else {
			final Boolean value = (Boolean) attribute;
			return value.booleanValue();
		}
	}

	@Override
	public char getCharAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return attribute.toString().charAt(0);
		} else {
			final Character value = (Character) attribute;
			return value.charValue();
		}

	}

	@Override
	public double getDoubleAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return Double.parseDouble(attribute.toString());
		} else {
			final Double value = (Double) attribute;
			return value.doubleValue();
		}

	}

	@Override
	public float getFloatAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return Float.parseFloat(attribute.toString());
		} else {
			final Float value = (Float) attribute;
			return value.floatValue();
		}

	}

	@Override
	public int getIntAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return Integer.parseInt(attribute.toString());
		} else {
			final Integer value = (Integer) attribute;
			return value.intValue();
		}

	}

	@Override
	public long getLongAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return Long.parseLong(attribute.toString());
		} else {
			final Long value = (Long) attribute;
			return value.longValue();
		}

	}

	@Override
	public short getShortAttribute(final String key) {
		final Object attribute = getAttribute(key);
		if (attribute instanceof CharSequence) {
			return Short.parseShort(attribute.toString());
		} else {
			final Short value = (Short) attribute;
			return value.shortValue();
		}
	}

	@Override
	public String getStringAttribute(final String key) {
		return (String) getAttribute(key);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mAttributes == null ? 0 : mAttributes.hashCode());
		return result;
	}

	@Override
	public boolean hasKey(final String key) {
		return mAttributes.containsKey(key);
	}

	@Override
	public IAttributeSet putAllAttributes(final IAttributeSet source) {
		final Set<String> keys = source.getAttributeKeys();
		for (final String key : keys) {
			putAttribute(key, source.getAttribute(key));
		}
		return this;
	}

	@Override
	public IAttributeSet putAttribute(final String key, final Object value) {
		if (value == null) {
			mAttributes.remove(key);
		} else {
			mAttributes.put(key, value);
		}
		return this;
	}

	@Override
	public IAttributeSet putBooleanAttribute(final String key, final boolean value) {
		putAttribute(key, new Boolean(value));
		return this;
	}

	@Override
	public IAttributeSet putCharAttribute(final String key, final char value) {
		putAttribute(key, new Character(value));
		return this;
	}

	@Override
	public IAttributeSet putDoubleAttribute(final String key, final double value) {
		putAttribute(key, new Double(value));
		return this;
	}

	@Override
	public IAttributeSet putFloatAttribute(final String key, final float value) {
		putAttribute(key, new Float(value));
		return this;
	}

	@Override
	public IAttributeSet putIntAttribute(final String key, final int value) {
		putAttribute(key, new Integer(value));
		return this;
	}

	@Override
	public IAttributeSet putLongAttribute(final String key, final long value) {
		putAttribute(key, new Long(value));
		return this;
	}

	@Override
	public IAttributeSet putShortAttribute(final String key, final Short value) {
		putAttribute(key, new Short(value));
		return this;
	}

	@Override
	public IAttributeSet putStringAttribute(final String key, final String value) {
		putAttribute(key, value);
		return this;
	}

	@Override
	public IAttributeSet removeAttribute(final String key) {
		mAttributes.remove(key);
		return this;
	}

	@Override
	public Map<String, Object> toMap() {
		return new HashMap<String, Object>(mAttributes);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "=" + attributesToString();
	}

	@Override
	public IAttributeSet forEachAttribute(IBiConsumer<String, Object> consumer) {
		int i = 0;
		for (final String key : mAttributes.keySet()) {
			consumer.consume(key, getAttribute(key), i);
		}
		return this;
	}

}
