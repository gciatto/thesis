package it.unibo.gciatto.util.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.AbsolutePosition;

import java.util.Arrays;
import java.util.Random;

import com.javadocmd.simplelatlng.util.LengthUnit;
import com.javadocmd.simplelatlng.window.LatLngWindow;

public final class MathUtils {

	public static class alg {
		public static double[] div(final double[] a, final double alpha) {
			final double[] res = vector(a);
			for (int i = 0; i < res.length; i++) {
				res[i] /= alpha;
			}
			return res;
		}

		public static double[] div(final double[] a, final double[] b) {
			final double[] res = vector(a);
			for (int i = 0; i < res.length; i++) {
				res[i] /= b[i];
			}
			return res;
		}

		public static double[] mult(final double[] a, final double alpha) {
			final double[] res = vector(a);
			for (int i = 0; i < res.length; i++) {
				res[i] *= alpha;
			}
			return res;
		}

		public static double[] neg(final double[] a) {
			final double[] res = vector(a);
			for (int i = 0; i < res.length; i++) {
				res[i] *= -1;
			}
			return res;
		}

		public static double norm(final double[] a) {
			final double scalar = dot(a, a);
			return Math.sqrt(scalar);
		}

		public static double dot(final double[] a, final double[] b) {
			double res = 0;
			for (int i = 0; i < a.length; i++) {
				res += a[i] * b[i];
			}
			return res;
		}

		public static double[] sub(final double[] a, final double[] b) {
			final double[] res = vector(a);
			for (int i = 0; i < res.length; i++) {
				res[i] -= b[i];
			}
			return res;
		}

		public static double[] sum(final double[]... vectors) {
			if (vectors.length == 0) {
				return null;
			} else if (vectors.length == 1) {
				return vector(vectors[0]);
			}

			final double[] res = vector(vectors[0]);
			for (int i = 0; i < res.length; i++) {
				for (int j = 1; j < vectors.length; j++) {
					res[i] += vectors[j][i];
				}
			}
			return res;
		}

		public static double[] vector(final double... vector) {
			return Arrays.copyOf(vector, vector.length);
		}

		public static double[] vector(final int size) {
			return new double[size];
		}

		public static double[] cross(final double[] a, final double[] b) {
			if (a.length != 3 || b.length != 3) {
				return null;
			}
			return new double[] { a[1] * b[2] - a[2] * b[1], a[0] * b[2] - a[2] * b[0], a[0] * b[1] - a[1] * b[0] };
		}

		public static double[] vers(final double[] a) {
			return div(a, norm(a));
		}
	}

	public static class geo {
		public static final class wgs84 {
			public static final double a = 6378137; // semiasse maggiore
			private static final double a2 = a * a;
			public static final double f = 1 / 298.257223563; // schiacciamento
			public static final double b = a * (1 - f); // semiasse minore
			private static final double b2 = b * b;
			public static final double e2 = (a2 - b2) / a2; // eccentricità?
			public static final double e2_1 = (a2 - b2) / b2;
			public static final double bOverASq = (b / a) * (b / a);
		}

		public static double[] LLAFromAbsPos(IAbsolutePosition pos) {
			return alg.vector(pos.getLatitude(), pos.getLongitude(), pos.getAltitude());
		}

		public static IAbsolutePosition absPosFromLLA(double[] lla) {
			if (lla.length == 2)  
				return new AbsolutePosition(lla[0], lla[1]);
			else if (lla.length >= 3)
				return new AbsolutePosition(lla[0], lla[1], lla[2]);
			else
				return null;
		}

		public static double distance(final double lat1, final double lng1, final double lat2, final double lng2, final double medAlt) {
			final double lat1R = Math.toRadians(lat1);
			final double lat2R = Math.toRadians(lat2);
			final double dLatR = Math.abs(lat2R - lat1R);
			final double dLngR = Math.abs(Math.toRadians(lng2 - lng1));
			final double a = Math.sin(dLatR / 2) * Math.sin(dLatR / 2) + Math.cos(lat1R) * Math.cos(lat2R) * Math.sin(dLngR / 2) * Math.sin(dLngR / 2);
			final double radResult = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			return radResult * (getN((lat1 + lat2) / 2)  + medAlt);
		}

		public static double distanceToLatitudeDelta(final double distance) {
			return LatLngWindow.lengthToLatitudeDelta(distance, LengthUnit.METER);
		}

		public static double distanceToLongitudeDelta(final double distance, final double latitude) {
			return LatLngWindow.lengthToLongitudeDelta(distance, LengthUnit.METER, latitude);
		}

		public static double latitudeDeltaToDistance(final double deltaLat) {
			return LatLngWindow.latitudeDeltaToLength(deltaLat, LengthUnit.METER);
		}

		public static double longitudeDeltaToDistance(final double deltaLng, final double latitude) {
			return LatLngWindow.latitudeDeltaToLength(deltaLng, LengthUnit.METER);
		}

		public static double[] toECEF(final double[] coords) {
			if (coords.length == 2) {
				return toECEF(coords[0], coords[1]);
			} else if (coords.length >= 3) {
				return toECEF(coords[0], coords[1], coords[2]);
			} else {
				return null;
			}
		}

		/**
		 * Radius of Curvature (meters) at a given latitude
		 * @param lat
		 * @return
		 */
		private static double getN(double lat) {
			final double sinLat = Math.sin(Math.toRadians(lat));
			return wgs84.a / Math.sqrt(1 - wgs84.e2 * sinLat * sinLat);
		}

		public static double[] toECEF(final double lat, final double lng, double alt) {
			//Ttemporany
			alt = 0;
			final double N = getN(lat);
			final double x = (N + alt) * Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(lng));
			final double y = (N + alt) * Math.cos(Math.toRadians(lat)) * Math.sin(Math.toRadians(lng));
			final double z = (wgs84.bOverASq * N + alt) * Math.sin(Math.toRadians(lat));

			return alg.vector(x, y, z);
		}

		public static double[] toECEF(final double lat, final double lng) {
			return toECEF(lat, lng, 0);
		}

		public static double[] toLatLng(final double[] coords) {
			return toLatLng(coords[0], coords[1], coords[2]);
		}

		public static double[] toLatLng(final double x, final double y, final double z) {
			final double p = alg.norm(alg.vector(x, y));
			final double theta = Math.atan2(z * wgs84.a, p * wgs84.b);
			final double tanLatNum = z + wgs84.e2_1 * wgs84.b * Math.pow(Math.sin(theta), 3);
			final double tanLatDen = p - wgs84.e2 * wgs84.a * Math.pow(Math.cos(theta), 3);
			final double lat = Math.toDegrees(Math.atan2(tanLatNum, tanLatDen));
			final double lon = Math.toDegrees(Math.atan2(y, x));
			final double alt = 0; //p / Math.cos(lat) - getN(lat);
			
			return alg.vector(lat, lon, alt);
		}
	}

	public static class rand {

		public static double randomPositiveDouble(final double max) {
			return rand.mRandom.nextDouble() * max;
		}

		public static double randomRealDouble(final double absMax) {
			return rand.mRandom.nextDouble() * absMax * 2 - absMax;
		}

		public static int randomInt(final int min, final int max) {
			return rand.randomInt(min, max, false);
		}

		public static int randomInt(final int min, final int max, final boolean maxInclusive) {
			final int actualMax = maxInclusive ? max + 1 : max;
			final int random = mRandom.nextInt(actualMax - min) + min;
			return random;
		}

		private static final Random mRandom = rand.mSeed == null ? new Random() : new Random(rand.mSeed.longValue());

		private static final Long mSeed = null;

	}

	public static class triangles {

		public static class carnot {
			public static double alpha(final double a, final double b, final double c) {
				return angle(a, b, c);
			}

			public static double angle(final double opp, final double ad1, final double ad2) {
				final double cos = cosAngle(opp, ad1, ad2);
				return Math.acos(cos);
			}

			public static double beta(final double a, final double b, final double c) {
				return angle(b, a, c);
			}

			public static double cosAlpha(final double a, final double b, final double c) {
				return cosAngle(a, b, c);
			}

			public static double cosAngle(final double opp, final double ad1, final double ad2) {
				final double ad1Quad = ad1 * ad1;
				final double ad2Quad = ad2 * ad2;
				final double oppQuad = opp * opp;
				final double doubleProd = ad1 * ad2;
				final double res = (ad1Quad + ad2Quad - oppQuad) / (2 * doubleProd);
				return res;
			}

			public static double cosBeta(final double a, final double b, final double c) {
				return cosAngle(b, a, c);
			}

			public static double cosGamma(final double a, final double b, final double c) {
				return cosAngle(c, a, b);
			}

			public static double gamma(final double a, final double b, final double c) {
				return angle(c, a, b);
			}
		}

		/**
		 * @see http
		 *      ://gis.stackexchange.com/questions/66/trilateration-using-3-
		 *      latitude -and-longitude-points-and-3-distances
		 * @param p1
		 * @param p2
		 * @param p3
		 * @param dP1
		 * @param dP2
		 * @param dP3
		 * @return
		 */
		public static double[] trilaterate(final double[] p1, final double[] p2, final double[] p3, final double dP1, final double dP2, final double dP3) {
			final double[] p12 = alg.sub(p2, p1);
			final double[] p13 = alg.sub(p3, p1);
			final double d = alg.norm(p12);
			final double[] ex = alg.div(p12, d);
			final double i = alg.dot(ex, p13);
			final double[] temp = alg.sub(p13, alg.mult(ex, i));
			final double[] ey = alg.vers(temp);
			final double[] ez = alg.cross(ex, ey);
			final double j = alg.dot(ey, p13);

			final double x = (dP1 * dP1 - dP2 * dP2 + d * d) / (d * 2);
			final double y = (dP1 * dP1 - dP3 * dP3 + i * i + j * j) / (j * 2) - ((i / j) * x);
			final double absZ = Math.sqrt(Math.abs(dP1 * dP1 - x * x - y * y));

			final double[] xex = alg.mult(ex, x);
			final double[] yey = alg.mult(ey, y);
			final double[] zez = alg.mult(ez, absZ);

			final double[] res = alg.sum(p1, xex, yey, zez);

			return res;
		}

		public static double[] trilaterateLatLng(final double[] p1, final double[] p2, final double[] p3, final double dP1, final double dP2, final double dP3) {
			final double[] temp = trilaterate(geo.toECEF(p1), geo.toECEF(p2), geo.toECEF(p3), dP1, dP2, dP3);
			final double[] res = geo.toLatLng(temp);
			return res;
		}

		public static void main(String[] args) {
			testTrilateration(0, 0, 0);
			for (int i = 0; i < 50; i++) {
				testTrilateration(rand.randomRealDouble(1), rand.randomRealDouble(1), rand.randomRealDouble(1));
			}
			testTrilateration(0, 0, 0);
		}

		private static void testTrilateration(double errA, double errB, double errC) {
			double[] a = alg.vector(37.996094, 15.412164, 70);
			double[] b = alg.vector(37.996014, 15.412417, 70);
			double[] c = alg.vector(37.996139, 15.412336, 70);
			double[] d = alg.vector(37.99605, 15.412292, 70);

			double distA = geo.distance(a[0], a[1], d[0], d[1], 70) + errA;
			double distB = geo.distance(b[0], b[1], d[0], d[1], 70) + errB;
			double distC = geo.distance(c[0], c[1], d[0], d[1], 70) + errC;

			double[] dt = trilaterateLatLng(a, b, c, distA, distB, distC);
			double error = geo.distance(d[0], d[1], dt[0], dt[1], 70);

			double[] diff = alg.sub(d, dt);
			System.out.printf("errA = %s\nerrB = %s\nerrC = %s\n", errA, errB, errC);
			System.out.printf("original = %s\ntrilaterated = %s\ndifference = %s\nerror = %s\n\n", Arrays.toString(d), Arrays.toString(dt), Arrays.toString(diff), error);
		}

	}

}
