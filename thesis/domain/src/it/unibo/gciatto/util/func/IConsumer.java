package it.unibo.gciatto.util.func;

public interface IConsumer<T> {
	void consume(T obj, int index);
}
