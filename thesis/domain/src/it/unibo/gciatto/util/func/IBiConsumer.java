package it.unibo.gciatto.util.func;

public interface IBiConsumer<T1, T2> {
	void consume(T1 obj1, T2 obj2, int num);
}
