package it.unibo.gciatto.util;

import it.unibo.gciatto.util.func.IBiConsumer;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Attribute set into the key-value-pairs form
 *
 * @author Giovanni Ciatto
 *
 */
public interface IAttributeSet extends Serializable, Cloneable {
	/**
	 * Removes all attributes from the set
	 *
	 * @return the object this method was invoked on
	 */
	IAttributeSet clearAllAttributes();

	int countAttributes();

	/**
	 *
	 * @param key
	 * @return
	 */
	Object getAttribute(String key);

	Set<String> getAttributeKeys();

	boolean getBooleanAttribute(String key);

	char getCharAttribute(String key);

	double getDoubleAttribute(String key);

	float getFloatAttribute(String key);

	int getIntAttribute(String key);

	long getLongAttribute(String key);

	short getShortAttribute(String key);

	String getStringAttribute(String key);

	/**
	 * Check if existis an attribute with the specified key
	 *
	 * @param key
	 *            is the key {@link String}
	 * @return a <code>boolean</code> value
	 */
	boolean hasKey(String key);

	IAttributeSet putAllAttributes(IAttributeSet source);

	IAttributeSet putAttribute(String key, Object value);

	IAttributeSet putBooleanAttribute(String key, boolean value);

	IAttributeSet putCharAttribute(String key, char value);

	IAttributeSet putDoubleAttribute(String key, double value);

	IAttributeSet putFloatAttribute(String key, float value);

	IAttributeSet putIntAttribute(String key, int value);

	IAttributeSet putLongAttribute(String key, long value);

	IAttributeSet putShortAttribute(String key, Short value);

	IAttributeSet putStringAttribute(String key, String value);

	IAttributeSet removeAttribute(String key);

	Map<String, Object> toMap();
	
	IAttributeSet forEachAttribute(IBiConsumer<String, Object> consumer);
}
