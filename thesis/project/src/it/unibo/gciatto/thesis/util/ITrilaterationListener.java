package it.unibo.gciatto.thesis.util;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

public interface ITrilaterationListener {
	void onTrilateration(IEntity trilaterated, IAbsolutePosition position, Object... args);
}
