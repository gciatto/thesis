package it.unibo.gciatto.thesis.util;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;

public interface IAbsolutePositionListener {
	void onAbsolutePosition(IAbsolutePosition position, Object args);
}
