package it.unibo.gciatto.thesis.util;

import it.unibo.gciatto.thesis.entity.IEntity;

public interface IManagerListener {
	void onEntityBound(IEntity entity, Object... args);

	void onEntityUnbound(IEntity entity, Object... args);
}
