package it.unibo.gciatto.thesis.util;

import it.unibo.gciatto.thesis.entity.IEntity;

public interface IIdentityListener {
	void onGoodbyeEntity(IEntity entity, Object args);

	void onWelcomeEntity(IEntity entity, Object args);
}
