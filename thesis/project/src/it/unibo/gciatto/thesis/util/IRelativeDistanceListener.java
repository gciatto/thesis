package it.unibo.gciatto.thesis.util;

import it.unibo.gciatto.thesis.position.IRelativeDistance;

public interface IRelativeDistanceListener {
	public static final String OP_ADD = "add";
	public static final String OP_REMOVE = "rem";

	void onRelativeDistance(IRelativeDistance distance, Object args);
}
