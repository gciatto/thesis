package it.unibo.gciatto.thesis.util;

import it.unibo.gciatto.thesis.entity.IEntity;

public interface IEntityDetectedListener {
	void onEntityDetected(IEntity entity, Object... args);
}
