package it.unibo.gciatto.thesis.entity.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.device.impl.JsonDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.JsonEntityId;
import it.unibo.gciatto.thesis.position.IRelativeDistance;
import it.unibo.gciatto.thesis.position.impl.JsonAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.JsonEntityRelativeDistance;
import it.unibo.gciatto.thesis.position.impl.JsonRelativeDistance;
import it.unibo.gciatto.util.IJsonObjectConvertible;
import it.unibo.gciatto.util.impl.JsonAttributeSet;

import java.util.Map;

import org.json2.JSONArray;
import org.json2.JSONObject;

public class JsonEntity extends Entity implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = 5690775780549241663L;

	public JsonEntity(final IContext context, final JSONObject object) {
		this(new JsonEntityId(object.getJSONObject("id")));

		init(object);

		final JSONArray distances = object.getJSONArray("distances");

		for (int i = 0; i < distances.length(); i++) {
			addRelativeDistance(new JsonEntityRelativeDistance(context, distances.getJSONObject(i)));
		}
	}

	public JsonEntity(final IEntity toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public JsonEntity(final IEntityId id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public JsonEntity(final Map<IEntityId, IEntity> entities, final JSONObject object) {
		this(new JsonEntityId(object.getJSONObject("id")));

		init(object);

		final JSONArray distances = object.getJSONArray("distances");

		for (int i = 0; i < distances.length(); i++) {
			addRelativeDistance(new JsonEntityRelativeDistance(entities, distances.getJSONObject(i)));
		}
	}

	private void init(final JSONObject object) {
		setAbsolutePosition(new JsonAbsolutePosition(object.getJSONObject("position")));
		putAllAttributes(new JsonAttributeSet(object.getJSONObject("attributes")));

		final JSONArray devices = object.getJSONArray("devices");
		// final JSONArray distances = object.getJSONArray("distances");

		for (int i = 0; i < devices.length(); i++) {
			associateDevice(new JsonDevice(devices.getJSONObject(i)));
		}

	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		final JSONArray devices = new JSONArray(), distances = new JSONArray();
		for (final IDevice d : getAssociatedDevices()) {
			devices.put(new JsonDevice(d).toJsonObject());
		}
		for (final IRelativeDistance d : getRelativeDistances()) {
			distances.put(new JsonRelativeDistance(d).toJsonObject());
		}
		obj.put("id", new JsonEntityId(getId()).toJsonObject()).put("devices", devices).put("distances", distances).put("position", new JsonAbsolutePosition(getAbsolutePosition()).toJsonObject()).put("attributes", new JsonAttributeSet(getAttributes()).toJsonObject());
		return obj;
	}

}
