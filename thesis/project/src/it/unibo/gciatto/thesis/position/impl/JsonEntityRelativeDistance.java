package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.JsonEntityId;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.entity.position.impl.EntityRelativeDistance;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import java.util.Map;

import org.json2.JSONObject;

public class JsonEntityRelativeDistance extends EntityRelativeDistance implements IEntityRelativeDistance, IJsonObjectConvertible {

	protected static IEntity getEntityRef(final IContext context, final IEntityId id) {
		return context.getIdentityManager().getEntity(id);
	}

	protected static IEntity getEntityRef(final Map<IEntityId, IEntity> context, final IEntityId id) {
		return context.get(id);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -6323711593561244348L;

	public JsonEntityRelativeDistance(final IContext context, final JSONObject object) {
		this(getEntityRef(context, new JsonEntityId(object.getJSONObject("relativeToEntity"))), object.getDouble("distance"));
	}

	public JsonEntityRelativeDistance(final IEntity relativeTo) {
		super(relativeTo);
	}

	public JsonEntityRelativeDistance(final IEntity relativeTo, final double distance) {
		super(relativeTo, distance);
	}

	public JsonEntityRelativeDistance(final IEntityRelativeDistance toClone) {
		super(toClone);
	}

	public JsonEntityRelativeDistance(final Map<IEntityId, IEntity> context, final JSONObject object) {
		this(getEntityRef(context, new JsonEntityId(object.getJSONObject("relativeToEntity"))), object.getDouble("distance"));
		setRegion(object.getBoolean("isRegion"));
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		obj.put("relativeToEntity", new JsonEntityId(getRelativeToEntity().getId()).toJsonObject()).put("distance", getDistance()).put("isRegion", isRegion());
		return obj;
	}

}
