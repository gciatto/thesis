package it.unibo.gciatto.thesis.handler;

import it.unibo.gciatto.thesis.util.IAbsolutePositionListener;

public interface IAbsolutePositionHandler extends IHandler {
	IAbsolutePositionHandler setAbsolutePositionListener(IAbsolutePositionListener listener);
}
