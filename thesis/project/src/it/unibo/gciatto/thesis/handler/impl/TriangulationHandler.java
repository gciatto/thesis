package it.unibo.gciatto.thesis.handler.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.landmark.ILandmark;
import it.unibo.gciatto.thesis.entity.landmark.impl.LandmarkUtils;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.ITrilaterationHandler;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativePosition;
import it.unibo.gciatto.thesis.position.impl.RelativePosition;
import it.unibo.gciatto.util.impl.MathUtils;

import java.util.ArrayList;
import java.util.List;

public class TriangulationHandler extends BaseHandler implements ITrilaterationHandler {

	public TriangulationHandler(final IContext context) {
		super(context);

		init();
	}

	protected IRelativePosition getLandmarkRelPos(final ILandmark first, final ILandmark second) {
		return new RelativePosition(first.getAbsolutePosition(), second.getAbsolutePosition());
	}

	/* (non-Javadoc)
	 * @see it.unibo.gciatto.thesis.handler.impl.ITrilaterationHandler#isTrilaterationPossible(it.unibo.gciatto.thesis.entity.IEntity)
	 */
	@Override
	public boolean isTrilaterationPossible(final IEntity entity) {
		return LandmarkUtils.countLandmarksDistances(entity.getRelativeDistances()) >= 3;
	}

	/* (non-Javadoc)
	 * @see it.unibo.gciatto.thesis.handler.impl.ITrilaterationHandler#trilaterate(it.unibo.gciatto.thesis.entity.IEntity, java.lang.Object[])
	 */
	@Override
	public IAbsolutePosition trilaterate(final IEntity entity, final Object[] usedLandmarks) throws Exception {
		if (!isTrilaterationPossible(entity)) {
			throw new IllegalStateException();
		}
		final ArrayList<IEntityRelativeDistance> distances = new ArrayList<IEntityRelativeDistance>(entity.getRelativeDistances());
		final List<ILandmark> landmarks = LandmarkUtils.getLandmarksFromDistances(distances);
		final ILandmark landmark1 = landmarks.get(0);
		final ILandmark landmark2 = landmarks.get(1);
		final ILandmark landmark3 = landmarks.get(2);

		if (usedLandmarks != null && usedLandmarks.length >= 3) {
			usedLandmarks[0] = landmark1;
			usedLandmarks[1] = landmark2;
			usedLandmarks[2] = landmark3;
		}

		final double[] res = MathUtils.triangles.trilaterateLatLng(
				MathUtils.geo.LLAFromAbsPos(landmark1.getAbsolutePosition()),
				MathUtils.geo.LLAFromAbsPos(landmark2.getAbsolutePosition()),
				MathUtils.geo.LLAFromAbsPos(landmark3.getAbsolutePosition()),
				entity.getDistanceRelativeTo(landmark1).getDistance(),
				entity.getDistanceRelativeTo(landmark2).getDistance(),
				entity.getDistanceRelativeTo(landmark3).getDistance());
		
		return MathUtils.geo.absPosFromLLA(res);
	}

}
