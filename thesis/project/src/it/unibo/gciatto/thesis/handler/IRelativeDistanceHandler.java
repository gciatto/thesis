package it.unibo.gciatto.thesis.handler;

import it.unibo.gciatto.thesis.util.IRelativeDistanceListener;

public interface IRelativeDistanceHandler extends IHandler {
	IRelativeDistanceHandler setRelativeDistanceListener(IRelativeDistanceListener listener);
}
