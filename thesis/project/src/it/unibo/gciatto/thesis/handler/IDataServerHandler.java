package it.unibo.gciatto.thesis.handler;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.util.IAttributeSet;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface IDataServerHandler extends IHandler {
	public static class ProtocolException extends Exception {

		private static final long serialVersionUID = -6279300756107770243L;

		public ProtocolException() {
			super();
		}

		public ProtocolException(final String arg0) {
			super(arg0);
		}

		public ProtocolException(final String arg0, final Throwable arg1) {
			super(arg0, arg1);
		}

		public ProtocolException(final Throwable arg0) {
			super(arg0);
		}
	}

	IDataServerHandler syncAddData(IEntityId id, IAttributeSet attributes) throws ProtocolException;

	IDataServerHandler syncAddRelativeDistance(IEntityId id, IEntityRelativeDistance distance) throws ProtocolException;

	IDataServerHandler syncFixAbsolutePosition(IEntityId id, IAbsolutePosition position) throws ProtocolException;

	IAttributeSet syncGetAttributes(IEntityId id) throws ProtocolException;

	Collection<IDevice> syncGetDevices(IEntityId id) throws ProtocolException;

	Set<IEntity> syncGetEverything() throws ProtocolException;

	Set<IEntityId> syncGetIds() throws ProtocolException;

	IAbsolutePosition syncGetPosition(IEntityId id) throws ProtocolException;

	Map<IEntityId, IAbsolutePosition> syncGetPositions() throws ProtocolException;

	Collection<IEntityRelativeDistance> syncGetRelativeDistances(IEntityId id) throws ProtocolException;

	IDataServerHandler syncRegisterDevice(IEntityId id, IDevice dev) throws ProtocolException;

	IDataServerHandler syncRegisterEntity(IEntityId id) throws ProtocolException;

	IDataServerHandler syncRemoveData(IEntityId id, IAttributeSet attributes) throws ProtocolException;

	IDataServerHandler syncRemoveRelativeDistance(IEntityId id, IEntityRelativeDistance distance) throws ProtocolException;

	IDataServerHandler syncReset() throws ProtocolException;

	IDataServerHandler syncUnregisterDevice(IEntityId id, IDevice dev) throws ProtocolException;

	IDataServerHandler syncUnregisterEntity(IEntityId id) throws ProtocolException;
}
