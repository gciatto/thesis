package it.unibo.gciatto.thesis.handler;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

public interface ITrilaterationHandler {

	boolean isTrilaterationPossible(IEntity entity);

	IAbsolutePosition trilaterate(IEntity entity, Object[] usedLandmarks) throws Exception;

}