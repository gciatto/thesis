package it.unibo.gciatto.thesis.handler.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.handler.IHandler;
import it.unibo.gciatto.thesis.log.L;

public abstract class BaseHandler implements IHandler {
	private final IContext mContext;

	public BaseHandler(final IContext context) {
		mContext = context;
	}

	@Override
	public IContext getContext() {
		return mContext;
	}

	@Override
	public IHandler init() {
		L.i("%s initialized", this);
		return this;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "#" + hashCode();
	}

}
