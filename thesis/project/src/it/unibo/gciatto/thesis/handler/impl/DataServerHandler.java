package it.unibo.gciatto.thesis.handler.impl;

import it.unibo.gciatto.http.IHttpRequest;
import it.unibo.gciatto.http.IHttpRequest.Method;
import it.unibo.gciatto.http.IHttpResponse;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.device.impl.JsonDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.JsonEntityId;
import it.unibo.gciatto.thesis.entity.impl.JsonEntity;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.http.impl.Protocol;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.JsonAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.JsonEntityRelativeDistance;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.impl.JsonAttributeSet;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json2.JSONArray;
import org.json2.JSONObject;

public abstract class DataServerHandler extends BaseHandler implements IDataServerHandler {

	public DataServerHandler(final IContext context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	protected void checkResponse(final IHttpResponse response) throws ProtocolException {
		if (!response.isOK()) {
			throw new ProtocolException(response.getStatusLine());
		}
	}

	protected abstract IHttpResponse executeRequest(IHttpRequest request) throws ProtocolException;

	protected IHttpResponse executeRequest(final Method method, final String what, final IEntityId id, final Object body) throws ProtocolException {
		final IHttpRequest request = generateRequest(method, what, id, body);
		final IHttpResponse response = executeRequest(request);
		checkResponse(response);
		return response;
	}

	protected abstract IHttpRequest generateRequest(Method method, String what, IEntityId id, Object body);

	protected URI getServerURI() {
		return getContext().getIdentityManager().getServiceURI(IIdentityManager.SERVICE_DATA_SERVER);
	}

	protected abstract JSONArray readJsonArray(IHttpResponse response);

	protected abstract JSONObject readJsonObject(IHttpResponse response);

	@Override
	public IDataServerHandler syncAddData(final IEntityId id, final IAttributeSet attributes) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.POST, Protocol.what.VALUE_DATA, id, new JsonAttributeSet(attributes));
		return this;
	}

	@Override
	public IDataServerHandler syncAddRelativeDistance(final IEntityId id, final IEntityRelativeDistance distance) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.POST, Protocol.what.VALUE_DISTANCE, id, new JsonEntityRelativeDistance(distance));
		return this;
	}

	@Override
	public IDataServerHandler syncFixAbsolutePosition(final IEntityId id, final IAbsolutePosition position) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.POST, Protocol.what.VALUE_POSITION, id, new JsonAbsolutePosition(position));
		return this;
	}

	@Override
	public IAttributeSet syncGetAttributes(final IEntityId id) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_DATA, id, null);
		final JSONObject data = readJsonObject(response);
		return new JsonAttributeSet(data);
	}

	@Override
	public Collection<IDevice> syncGetDevices(final IEntityId id) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_DEVICES, id, null);
		final JSONArray data = readJsonArray(response);
		return unmarshallDevices(data);
	}

	@Override
	public Set<IEntity> syncGetEverything() throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_EVERYTHING, null, null);
		final JSONArray data = readJsonArray(response);
		return unmarshallEntities(data);
	}

	@Override
	public Set<IEntityId> syncGetIds() throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_IDS, null, null);
		final JSONArray data = readJsonArray(response);
		return unmarshallIds(data);
	}

	@Override
	public IAbsolutePosition syncGetPosition(final IEntityId id) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_POSITION, id, null);
		final JSONObject data = readJsonObject(response);
		return new JsonAbsolutePosition(data);
	}

	@Override
	public Map<IEntityId, IAbsolutePosition> syncGetPositions() throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_POSITIONS, null, null);
		final JSONArray data = readJsonArray(response);
		return unmarshallPositions(data);
	}

	@Override
	public Collection<IEntityRelativeDistance> syncGetRelativeDistances(final IEntityId id) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.GET, Protocol.what.VALUE_DISTANCES, id, null);
		final JSONArray data = readJsonArray(response);
		return unmarshallDistances(data);
	}

	@Override
	public IDataServerHandler syncRegisterDevice(final IEntityId id, final IDevice dev) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.POST, Protocol.what.VALUE_DEVICE, id, new JsonDevice(dev));
		return this;
	}

	@Override
	public IDataServerHandler syncRegisterEntity(final IEntityId id) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.POST, Protocol.what.VALUE_ID, id, null);
		return this;
	}

	@Override
	public IDataServerHandler syncRemoveData(final IEntityId id, final IAttributeSet attributes) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.DELETE, Protocol.what.VALUE_DATA, id, new JsonAttributeSet(attributes));
		return this;
	}

	@Override
	public IDataServerHandler syncRemoveRelativeDistance(final IEntityId id, final IEntityRelativeDistance distance) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.DELETE, Protocol.what.VALUE_DISTANCE, id, new JsonEntityRelativeDistance(distance));
		return this;
	}

	@Override
	public IDataServerHandler syncReset() throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.DELETE, Protocol.what.VALUE_EVERYTHING, null, null);
		return this;
	}

	@Override
	public IDataServerHandler syncUnregisterDevice(final IEntityId id, final IDevice dev) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.DELETE, Protocol.what.VALUE_DEVICE, id, new JsonDevice(dev));
		return this;
	}

	@Override
	public IDataServerHandler syncUnregisterEntity(final IEntityId id) throws ProtocolException {
		final IHttpResponse response = executeRequest(Method.DELETE, Protocol.what.VALUE_ID, id, null);
		return this;
	}

	protected Collection<IDevice> unmarshallDevices(final JSONArray array) {
		final List<IDevice> output = new ArrayList<IDevice>(array.length());
		for (int i = 0; i < array.length(); i++) {
			output.add(new JsonDevice(array.getJSONObject(i)));
		}
		return output;
	}

	protected Collection<IEntityRelativeDistance> unmarshallDistances(final JSONArray array) {
		final List<IEntityRelativeDistance> output = new ArrayList<IEntityRelativeDistance>(array.length());
		for (int i = 0; i < array.length(); i++) {
			output.add(new JsonEntityRelativeDistance(getContext(), array.getJSONObject(i)));
		}
		return output;
	}

	protected Set<IEntity> unmarshallEntities(final JSONArray array) {
		final Set<IEntity> output = new HashSet<IEntity>();
		for (int i = 0; i < array.length(); i++) {
			output.add(new JsonEntity(getContext(), array.getJSONObject(i)));
		}
		return output;
	}

	protected Set<IEntityId> unmarshallIds(final JSONArray array) {
		final Set<IEntityId> output = new HashSet<IEntityId>();
		for (int i = 0; i < array.length(); i++) {
			output.add(new JsonEntityId(array.getJSONObject(i)));
		}
		return output;
	}

	protected Map<IEntityId, IAbsolutePosition> unmarshallPositions(final JSONArray array) {
		final Map<IEntityId, IAbsolutePosition> output = new HashMap<IEntityId, IAbsolutePosition>();
		for (int i = 0; i < array.length(); i++) {
			final JSONArray fix = array.getJSONArray(i);
			final IEntityId id = new JsonEntityId(fix.getJSONObject(0));
			final IAbsolutePosition pos = new JsonAbsolutePosition(fix.getJSONObject(1));
			output.put(id, pos);
		}
		return output;
	}

}
