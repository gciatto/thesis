package it.unibo.gciatto.thesis.handler;

import it.unibo.gciatto.thesis.context.IContext;

public interface IHandler {
	IContext getContext();

	IHandler init();
}
