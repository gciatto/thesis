package it.unibo.gciatto.thesis.log;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L {
	public static void addHandler(final Handler handler) throws SecurityException {
		logger.addHandler(handler);
	}

	public static void config(final String msg) {
		logger.config(msg);
	}

	public static void e(final Throwable th) {
		log(Level.SEVERE, th.getMessage(), th);
	}

	public static void f(final String msg) {
		logger.fine(msg);
	}

	public static void fr(final String msg) {
		logger.finer(msg);
	}

	public static void fs(final String msg) {
		logger.finest(msg);
	}

	public static Handler[] getHandlers() {
		return logger.getHandlers();
	}

	public static Level getLevel() {
		return logger.getLevel();
	}

	public static String getName() {
		return logger.getName();
	}

	public static Logger getParent() {
		return logger.getParent();
	}

	public static void i(final String msg) {
		logger.info(msg);
	}

	public static void i(final String format, final Object... objects) {
		logger.info(String.format(format, objects));
	}

	public static boolean isLoggable(final Level level) {
		return logger.isLoggable(level);
	}

	public static void log(final Level level, final String msg) {
		logger.log(level, msg);
	}

	public static void log(final Level level, final String msg, final Object param1) {
		logger.log(level, msg, param1);
	}

	public static void log(final Level level, final String msg, final Object[] params) {
		logger.log(level, msg, params);
	}

	public static void log(final Level level, final String msg, final Throwable thrown) {
		logger.log(level, msg, thrown);
	}

	public static void removeHandler(final Handler handler) throws SecurityException {
		logger.removeHandler(handler);
	}

	public static void s(final String msg) {
		logger.severe(msg);
	}

	public static void setParent(final Logger parent) {
		logger.setParent(parent);
	}

	public static void w(final String msg, final Object... objects) {
		logger.warning(String.format(msg, objects));
	}

	private static final Logger logger = Logger.getGlobal();

	static {
		logger.setLevel(Level.ALL);
	}

	private L() {
		throw new IllegalStateException();
	}

}
