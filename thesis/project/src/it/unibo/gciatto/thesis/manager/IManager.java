package it.unibo.gciatto.thesis.manager;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.util.IManagerListener;
import it.unibo.gciatto.util.func.IConsumer;

import java.util.Collection;

public interface IManager {
	IManager addManagerListener(IManagerListener listener);

	IManager asyncASAPSynchronize();

	IManager bind(IEntity entity);

	int countBoundEntities();

	IManager forEachEntity(IConsumer<IEntity> conusmer);

	IManager forEachEntityId(IConsumer<IEntityId> consumer);

	Collection<IEntity> getBoundEntities();

	IEntity getBoundEntity(IEntityId id);

	IContext getContext();

	boolean isBound(IEntity entity);

	boolean isBound(IEntityId id);

	IManager removeAllManagerListeners();

	IManager removeManagerListener(IManagerListener listener);

	IManager setContext(IContext value);

	IManager unbind(IEntity entity);
}
