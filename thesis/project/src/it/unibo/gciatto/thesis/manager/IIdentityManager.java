package it.unibo.gciatto.thesis.manager;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.util.IIdentityListener;

import java.net.URI;
import java.util.Collection;

public interface IIdentityManager extends IManager {
	public static final String SERVICE_DATA_SERVER = IIdentityManager.class.getName() + ".data_server";

	IIdentityManager addIdentityListener(IIdentityListener listener);

	IIdentityManager asyncQueryDevices(IEntity entity);

	IIdentityManager asyncQueryEntities();

	IIdentityManager asyncRegisterDevice(IEntity entity, IDevice device);

	IIdentityManager asyncRegisterEntity(IEntity entity);

	IIdentityManager asyncUnregisterEntity(IEntityId id);

	// Set<IEntity> getAllRegisteredEntities();

	IIdentityManager clearIdentityListeners();

	IEntity getEntity(IDevice device);

	IEntity getEntity(IEntityId id);

	IEntity getEntityFromMAC(String mac);

	Collection<IEntityId> getEntityIds();

	IEntity getSelf();

	URI getServiceURI(String serviceName);

	IIdentityManager removeIdentityListener(IIdentityListener listener);
}
