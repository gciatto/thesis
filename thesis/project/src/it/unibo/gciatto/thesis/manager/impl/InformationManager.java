package it.unibo.gciatto.thesis.manager.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler.ProtocolException;
import it.unibo.gciatto.thesis.knowledge.K;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.util.IAttributeSet;

import java.util.concurrent.TimeUnit;

public class InformationManager extends ExecutorManager implements IInformationManager {

	private final Runnable mUpdateTask = new Runnable() {

		@Override
		public void run() {
			try {
				syncUpdate();
			} catch (Throwable th) {
				L.e(th);
			}
		}
	};

	public InformationManager(final IContext parent) {
		super(parent);

		scheduleAtFixedRate(mUpdateTask, K.timeouts.TIMEOUT_ATTRIBUTES_UPDATE_INITIAL_DELAY, K.timeouts.TIMEOUT_ATTRIBUTES_UPDATE_PERIOD, TimeUnit.SECONDS);
	}

	@Override
	public IManager asyncASAPSynchronize() {
		execute(new Runnable() {

			@Override
			public void run() {
				try {
					syncUpdate();
				} catch (Throwable th) {
					L.e(th);
				}
			}
		});
		return this;
	}

	@Override
	public IInformationManager asyncQueryData(final IEntity entity) {
		execute(new Runnable() {

			@Override
			public void run() {
				syncQueryData(entity);
			}
		});
		L.i("Data request for entity %s added to queue", entity.getId());
		return this;
	}
	
	protected IInformationManager syncSendData(final IEntity entity) {
		try {
			getHandler().syncAddData(entity.getId(), entity);
		} catch (final ProtocolException e) {
			return syncSendData(entity);
		}
		return this;
	}

	protected IEntity getSelf() {
		return getContext().getIdentityManager().getSelf();
	}

	protected IInformationManager syncQueryData(final IEntity entity) {
		try {
			final IAttributeSet attributes = getHandler().syncGetAttributes(entity.getId());
			updateData(entity, attributes);
		} catch (final ProtocolException e) {
			return syncQueryData(entity);
		}
		return this;
	}

	protected IInformationManager syncUpdate() {
		for (final IEntity e : getBoundEntities()) {
			if (e.isSameEntity(getSelf())) {
				syncSendData(e);
			} else {
				syncQueryData(e);
			}
		}
		L.i("Data synchronized with server");
		return this;
	}

	@Override
	public IInformationManager updateData(final IEntity entity, final IAttributeSet attributes) {
		if (isBound(entity)) {
//			entity.clearAllAttributes();
			entity.putAllAttributes(attributes);
			L.i("updated entity %s's attributes to %s", entity.getId(), attributes);
		} else {
			L.w("entity %s is not bound to %s", entity.getId(), this);
		}
		return this;
	}

}
