package it.unibo.gciatto.thesis.manager.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.EntityId;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.thesis.util.IEntityDetectedListener;

public class EnvironmentManager extends BaseManager implements IEnvironmentManger {
	
	private IEntityDetectedListener mListener;

	public EnvironmentManager(IContext parent) {
		super(parent);
	}

	@Override
	public IManager asyncASAPSynchronize() {
		return this;
	}

	@Override
	public IEnvironmentManger setEntityDetectedListener(IEntityDetectedListener listener) {
		mListener = listener;
		return this;
	}

	@Override
	public IEnvironmentManger notifyPatternDetected(String pattern) {
		try {
			final IEntityId id = EntityId.fromURICharSeq(pattern);
			if (isBound(id)) {
				notifyEntityDetected(getBoundEntity(id));
			}
		} catch (Exception ex) {
			L.e(ex);
		}
		return this;
	}

	protected void notifyEntityDetected(IEntity entity, Object... args) {
		if (mListener != null) {
			 mListener.onEntityDetected(entity, args);
		}
	}
	
}
