package it.unibo.gciatto.thesis.manager.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.ITrilaterationHandler;
import it.unibo.gciatto.thesis.handler.IDataServerHandler.ProtocolException;
import it.unibo.gciatto.thesis.handler.impl.TriangulationHandler;
import it.unibo.gciatto.thesis.knowledge.K;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativeDistance;
import it.unibo.gciatto.thesis.position.IRelativePosition;
import it.unibo.gciatto.thesis.util.IRelativeDistanceListener;
import it.unibo.gciatto.thesis.util.ITrilaterationListener;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PositionManager extends ExecutorManager implements IPositionManager {

	private boolean mSelfLocalizationEnabled = true;
	private ITrilaterationListener mTrilaterationListener;
	private final Runnable mUpdateTask = new Runnable() {

		@Override
		public void run() {
			try {
				syncUpdate();
			} catch (Throwable th) {
				L.e(th);
			}
		}
	};
	private final ITrilaterationHandler mTriangulationHandler;

	public PositionManager(final IContext parent) {
		super(parent);

		mTriangulationHandler = new TriangulationHandler(parent);

		scheduleAtFixedRate(mUpdateTask, K.timeouts.TIMEOUT_POSITION_UPDATES_INITIAL_DELAY, K.timeouts.TIMEOUT_POSITION_UPDATES_PERIOD, TimeUnit.SECONDS);
	}

	@Override
	public IPositionManager addRelativeDistance(final IEntity entity, final IEntityRelativeDistance distance) {
		if (isBound(entity)) {
			entity.addRelativeDistance(distance);
			L.i("added relative distance %s to entity %s", distance, entity.getId());
		} else {
			fixBoundedEntity(entity);
		}
		return this;
	}

	@Override
	public IManager asyncASAPSynchronize() {
		execute(new Runnable() {

			@Override
			public void run() {
				syncUpdate();
			}
		});
		return this;
	}

	@Override
	public IPositionManager asyncQueryAbsolutePosition(final IEntity entity) {
		execute(new Runnable() {

			@Override
			public void run() {
				syncQueryAbsolutePosition(entity);
			}
		});
		L.i("Position request for entity %s added to queue", entity.getId());
		return this;
	}

	@Override
	public IPositionManager asyncQueryRelativeDistances(final IEntity entity) {
		execute(new Runnable() {

			@Override
			public void run() {
				syncQueryRelativeDistances(entity);
			}
		});
		L.i("Distances request for entity %s added to queue", entity.getId());
		return this;
	}

	@Override
	public IPositionManager fixAbsolutePosition(final IEntity entity, final IAbsolutePosition position) {
		if (isBound(entity)) {
			entity.move(position);
			L.i("updated entity %s's absolute position to %s", entity.getId(), position);
		} else {
			fixBoundedEntity(entity);
		}
		return this;
	}
	
	protected void fixBoundedEntity(IEntity entity) {
		if (!isBound(entity) && getContext().getIdentityManager().isBound(entity)) {
			bind(entity);
		} else {
			L.w("entity %s is not bound to %s", entity.getId(), this);
		}
	}

	protected IEntity getSelf() {
		return getContext().getIdentityManager().getSelf();
	}

	protected ITrilaterationListener getTriangulationListener() {
		return mTrilaterationListener;
	}

	@Override
	public boolean isSelfLocalizationEnabled() {
		return mSelfLocalizationEnabled;
	}

	protected void notifyTrilateration(final IEntity trilaterated, IAbsolutePosition pos, final Object... args) {
		if (mTrilaterationListener != null) {
			mTrilaterationListener.onTrilateration(trilaterated, pos, args);
		}
	}

	@Override
	public void onAbsolutePosition(final IAbsolutePosition position, final Object args) {
		if (isSelfLocalizationEnabled() && getSelf().getLOD().isGPSLocalizationConvenient()) {
			fixAbsolutePosition(getSelf(), position);
			asyncFixSelfPositionOnServer(position);
		}
	}
	
	protected void asyncFixSelfPositionOnServer(final IAbsolutePosition position) {
		execute(new Runnable() {

			@Override
			public void run() {
				syncFixSelfPositionOnServer(position);
			}
		});
		L.i("Self absolute position %s update added to queue", position);
	}
	
	protected void syncFixSelfPositionOnServer(final IAbsolutePosition position) {
		try {
			getHandler().syncFixAbsolutePosition(getSelf().getId(), position);
			L.i("sent fix to server");
		} catch (final ProtocolException e) {
			syncFixSelfPositionOnServer(position);
		}
	}

	@Override
	public void onRelativeDistance(final IRelativeDistance distance, final Object args) {
		if (IRelativeDistanceListener.OP_ADD.equals(args)) {
			addRelativeDistance(getSelf(), (IEntityRelativeDistance) distance);
		} else if (IRelativeDistanceListener.OP_REMOVE.equals(args)) {
			removeRelativeDistance(getSelf(), (IEntityRelativeDistance) distance);
		} else {
			L.w("Illegal argument %s", args);
		}
		if (mTriangulationHandler.isTrilaterationPossible(getSelf())) {
			try {
				final Object[] landmarks = new Object[2];
				final IAbsolutePosition triPos = mTriangulationHandler.trilaterate(getSelf(), landmarks);
				getSelf().setLOD(ILevelOfDetail.LOD_INSIDE_INTERESTING_AREA);
				fixAbsolutePosition(getSelf(), triPos);
				asyncFixSelfPositionOnServer(triPos);
				notifyTrilateration(getSelf(), triPos);
			} catch (final Exception ex) {
				L.e(ex);
			}
		} else {
			getSelf().setLOD(ILevelOfDetail.LOD_POINT_OF_INTEREST);
		}
	}

	@Override
	public IPositionManager removeRelativeDistance(final IEntity entity, final IEntityRelativeDistance distance) {
		if (isBound(entity)) {
			entity.removeRelativeDistance(distance);
			L.i("removed relative distance %s to entity %s", distance, entity.getId());
		} else {
			fixBoundedEntity(entity);
		}
		return this;
	}

	@Override
	public IPositionManager setSelfLocalizationEnabled(final boolean value) {
		mSelfLocalizationEnabled = value;
		return this;
	}

	@Override
	public IPositionManager setTriangulationListener(final ITrilaterationListener listener) {
		mTrilaterationListener = listener;
		return this;
	}

	protected IPositionManager syncQueryAbsolutePosition(final IEntity entity) {
		try {
			final IAbsolutePosition position = getHandler().syncGetPosition(entity.getId());

			if (entity.getLOD().isGPSLocalizationConvenient()) {
				fixAbsolutePosition(entity, position);
			}
		} catch (final ProtocolException e) {
			L.e(e);
		}
		return this;
	}

	protected IPositionManager syncQueryAbsolutePositions() {
		try {
			final Map<IEntityId, IAbsolutePosition> positions = getHandler().syncGetPositions();

			for (final IEntityId id : positions.keySet()) {
				final IEntity entity = getContext().getIdentityManager().getEntity(id);
				if (entity == null || entity.isSameEntity(getSelf())) {
					continue;
				}
				if (entity.getLOD().isGPSLocalizationConvenient()) {
					fixAbsolutePosition(entity, positions.get(id));
				}
			}

		} catch (final ProtocolException e) {
			L.e(e);
		}
		return this;
	}

	protected IPositionManager syncQueryRelativeDistances(final IEntity entity) {
		try {
			final Collection<IEntityRelativeDistance> distances = getHandler().syncGetRelativeDistances(entity.getId());
			for (final IEntityRelativeDistance d : entity.getRelativeDistances()) {
				if (!distances.contains(d)) {
					removeRelativeDistance(entity, d);
				}
			}
			for (final IEntityRelativeDistance d : distances) {
				addRelativeDistance(entity, d);
			}
			if (!entity.getLOD().isInsideView()) {
				if (mTriangulationHandler.isTrilaterationPossible(entity)) {
					entity.setLOD(ILevelOfDetail.LOD_INSIDE_INTERESTING_AREA);
					try {
						final IAbsolutePosition triPos = mTriangulationHandler.trilaterate(entity, null);
						fixAbsolutePosition(entity, triPos);
						notifyTrilateration(entity, triPos);
					} catch (Exception ex) {
						L.e(ex);
					}
					
				} else {
					entity.setLOD(ILevelOfDetail.LOD_POINT_OF_INTEREST);
				}
			}
		} catch (final ProtocolException e) {
			entity.setLOD(ILevelOfDetail.LOD_POINT_OF_INTEREST);
			L.e(e);
		}
		return this;
	}

	protected IPositionManager syncUpdate() {
		int count = 0;

		syncQueryAbsolutePositions();

		for (final IEntity e : getContext().getIdentityManager().getBoundEntities()) {
			if (e.getId().isSameEntity(getSelf().getId())) {
				continue;
			}
			// syncQueryAbsolutePosition(e);
			syncQueryRelativeDistances(e);
			count++;
		}
		if (count > 0) {
			L.i("Positions synchronized with server");
		}
		return this;
	}

}
