package it.unibo.gciatto.thesis.manager.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.device.IMutableDevice;
import it.unibo.gciatto.thesis.device.impl.Device;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.impl.Entity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler.ProtocolException;
import it.unibo.gciatto.thesis.knowledge.K;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.thesis.util.IIdentityListener;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class IdentityManager extends ExecutorManager implements IIdentityManager {
	private final IEntity mSelf;
	private final List<IIdentityListener> mIdentityListeners = new LinkedList<IIdentityListener>();
	private final Runnable mUpdateTask = new Runnable() {

		@Override
		public void run() {
			try {
				syncQueryEntities();
			} catch (Throwable th) {
				L.e(th);
			}
		}
	};
	private final Map<String, IEntity> mChacheMAC = new HashMap<String, IEntity>();

	public IdentityManager(final IContext parent, final IEntity self) {
		super(parent);
		mSelf = self;
		bind(mSelf);

		scheduleAtFixedRate(mUpdateTask, K.timeouts.TIMEOUT_IDS_UPDATE_INITIAL_DELAY, K.timeouts.TIMEOUT_IDS_UPDATE_PERIOD, TimeUnit.SECONDS);
	}

	@Override
	public IIdentityManager addIdentityListener(final IIdentityListener listener) {
		mIdentityListeners.add(listener);
		return this;
	}

	@Override
	public IManager asyncASAPSynchronize() {
		execute(new Runnable() {

			@Override
			public void run() {
				syncQueryEntities();
			}
		});
		return this;
	}

	@Override
	public IIdentityManager asyncQueryDevices(final IEntity entity) {
		execute(new Runnable() {

			@Override
			public void run() {
				syncQueryDevices(entity);
			}
		});
		return this;
	}

	@Override
	public IIdentityManager asyncQueryEntities() {
		execute(new Runnable() {

			@Override
			public void run() {
				syncQueryEntities();
			}
		});
		L.i("Sent query for entities ' ids");
		return this;
	}

	@Override
	public IIdentityManager asyncRegisterDevice(final IEntity entity, final IDevice device) {
		execute(new Runnable() {

			@Override
			public void run() {
				syncRegisterDevice(entity, device);
			}
		});
		return this;
	}

	@Override
	public IIdentityManager asyncRegisterEntity(final IEntity entity) {
		if (entity.getAssociatedDevices().size() > 0) {
			for (final IDevice d : entity.getAssociatedDevices()) {
				execute(new Runnable() {

					@Override
					public void run() {
						try {
							getHandler().syncRegisterDevice(entity.getId(), d);
							L.i("Device %s of entity %s successfully registered", d, entity.getId());
						} catch (final ProtocolException ex) {
							L.e(ex);
						}
					}
				});
			}
		} else {
			execute(new Runnable() {

				@Override
				public void run() {
					try {
						getHandler().syncRegisterEntity(entity.getId());
						L.i("Entity %s successfully registered", entity.getId());
					} catch (final ProtocolException ex) {
						L.e(ex);
					}
				}
			});
		}
		L.i("Registration request for entity %s added to queue", entity.getId());
		return this;
	}

	@Override
	public IIdentityManager asyncUnregisterEntity(final IEntityId id) {
		execute(new Runnable() {

			@Override
			public void run() {
				try {
					getHandler().syncUnregisterEntity(id);
					L.i("Entity %s successfully registered", id);
				} catch (final ProtocolException ex) {
					L.e(ex);
				}
			}
		});
		L.i("Unregistration request for entity %s added to queue", id);
		return this;
	}

	@Override
	public IManager bind(final IEntity entity) {
		super.bind(entity);
		if (isBound(entity)) {
			asyncRegisterEntity(entity);
		}
		return this;
	}

	// @Override
	// public Set<IEntity> getAllRegisteredEntities() {
	// return new HashSet<IEntity>(getEntitiesMapReference().values());
	// }

	protected IManager bindWithoutRegister(final IEntity entity) {
		super.bind(entity);
		return this;
	}

	@Override
	public IIdentityManager clearIdentityListeners() {
		mIdentityListeners.clear();
		return this;
	}

	@Override
	public IEntity getEntity(final IDevice device) {
		return searchForDevice(device);
	}

	@Override
	public IEntity getEntity(final IEntityId id) {
		return getBoundEntity(id);
	}

	@Override
	public IEntity getEntityFromMAC(final String mac) {
		synchronized (mChacheMAC) {
			if (mChacheMAC.containsKey(mac)) {
				return mChacheMAC.get(mac);
			}
		}
		final IMutableDevice devFilter = new Device().setMac(mac);
		final IEntity entity = getEntity(devFilter);
		if (entity != null) {
			synchronized (mChacheMAC) {
				mChacheMAC.put(mac, entity);
			}
		}
		return entity;
	}

	@Override
	public Collection<IEntityId> getEntityIds() {
		final Map<IEntityId, IEntity> entitiesMapReference = getEntitiesMapReference();
		synchronized (entitiesMapReference) {
			return new ArrayList<IEntityId>(entitiesMapReference.keySet());
		}
	}

	@Override
	public IEntity getSelf() {
		synchronized (mSelf) {
			return mSelf;
		}
	}

	@Override
	public URI getServiceURI(final String serviceName) {
		if (SERVICE_DATA_SERVER.equalsIgnoreCase(serviceName)) {
			try {
				return new URI(K.addresses.ADDR_DATA_SERVER);
			} catch (final URISyntaxException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	protected void notifyGoodbyeEntity(final IEntity entity) {
		for (final IIdentityListener listener : mIdentityListeners) {
			listener.onGoodbyeEntity(entity, this);
		}
	}

	protected void notifyWelcomeEntity(final IEntity entity) {
		for (final IIdentityListener listener : mIdentityListeners) {
			listener.onWelcomeEntity(entity, this);
		}
	}

	@Override
	public IIdentityManager removeIdentityListener(final IIdentityListener listener) {
		mIdentityListeners.remove(listener);
		return this;
	}

	protected IEntity searchForDevice(final IDevice device) {
		final Collection<IEntity> entities = getBoundEntities();
		for (final IEntity e : entities) {
			for (final IDevice d : e.getAssociatedDevices()) {
				if (d.equals(device)) {
					return e;
				}
			}
		}
		return null;
	}

	protected IIdentityManager syncQueryDevices(final IEntity entity) {
		try {
			final Collection<? extends IDevice> devices = getHandler().syncGetDevices(entity.getId());
			for (final IDevice d : devices) {
				entity.associateDevice(d);
			}
		} catch (final ProtocolException ex) {
			return syncQueryDevices(entity);
		}
		return this;
	}

	protected IIdentityManager syncQueryEntities() {
		try {
			final Set<IEntityId> ids = getHandler().syncGetIds();
			for (final IEntityId id : ids) {
				if (id.isSameEntity(getSelf().getId())) {
					continue;
				}
				if (!isBound(id)) {
					final IEntity newEntity = new Entity(id);
					bindWithoutRegister(newEntity);
					syncQueryDevices(newEntity);
					notifyWelcomeEntity(newEntity);
				}
			}
			if (!ids.contains(getSelf().getId())) {
				syncRegisterEntity(getSelf());
			}
			L.i("Ids synchronized with server");
		} catch (final ProtocolException ex) {
			return syncQueryEntities();
			
		}
		return this;
	}

	protected IIdentityManager syncRegisterDevice(final IEntity entity, final IDevice device) {
		try {
			getHandler().syncRegisterDevice(entity.getId(), device);
		} catch (final ProtocolException ex) {
			return syncRegisterDevice(entity, device);
		}
		return this;
	}

	protected IIdentityManager syncRegisterEntity(final IEntity entity) {
		try {
			if (entity.getAssociatedDevices().size() > 0) {
				for (final IDevice d : entity.getAssociatedDevices()) {
					getHandler().syncRegisterDevice(entity.getId(), d);
					L.i("Device %s of entity %s successfully registered", d, entity.getId());
				}
			} else {
				getHandler().syncRegisterEntity(entity.getId());
				L.i("Entity %s successfully registered", entity.getId());
			}
		} catch (final ProtocolException ex) {
			return syncRegisterEntity(entity);
		}
		return this;
	}

	@Override
	public IManager unbind(final IEntity entity) {
		super.unbind(entity);
		asyncUnregisterEntity(entity.getId());
		return this;
	}

	protected IManager unbindWithoutUnregister(final IEntity entity) {
		super.unbind(entity);
		return this;
	}
}
