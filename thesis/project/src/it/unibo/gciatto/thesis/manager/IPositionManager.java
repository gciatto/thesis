/**
 *
 */
package it.unibo.gciatto.thesis.manager;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.util.IAbsolutePositionListener;
import it.unibo.gciatto.thesis.util.IRelativeDistanceListener;
import it.unibo.gciatto.thesis.util.ITrilaterationListener;

/**
 * @author Giovanni
 *
 */
public interface IPositionManager extends IManager, IAbsolutePositionListener, IRelativeDistanceListener {
	IPositionManager addRelativeDistance(IEntity entity, IEntityRelativeDistance distance);

	IPositionManager asyncQueryAbsolutePosition(IEntity entity);

	IPositionManager asyncQueryRelativeDistances(IEntity entity);

	IPositionManager fixAbsolutePosition(IEntity entity, IAbsolutePosition position);

	boolean isSelfLocalizationEnabled();

	IPositionManager removeRelativeDistance(IEntity entity, IEntityRelativeDistance distance);

	IPositionManager setSelfLocalizationEnabled(boolean value);

	IPositionManager setTriangulationListener(ITrilaterationListener listener);

}
