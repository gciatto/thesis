package it.unibo.gciatto.thesis.manager;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.util.IAttributeSet;

public interface IInformationManager extends IManager {
	IInformationManager asyncQueryData(IEntity entity);

	IInformationManager updateData(IEntity entity, IAttributeSet attributes);
}
