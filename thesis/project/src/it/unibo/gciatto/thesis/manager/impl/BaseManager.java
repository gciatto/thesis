package it.unibo.gciatto.thesis.manager.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.thesis.util.IManagerListener;
import it.unibo.gciatto.util.func.IConsumer;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class BaseManager implements IManager {

	private IContext mParent;
	private final Map<IEntityId, IEntity> mEntities = new HashMap<IEntityId, IEntity>();
	private final List<IManagerListener> mListeners = new LinkedList<IManagerListener>();

	public BaseManager(final IContext parent) {
		mParent = parent;
	}

	@Override
	public IManager addManagerListener(final IManagerListener listener) {
		mListeners.add(listener);
		return this;
	}

	// protected abstract void onBind(IEntity entity);
	// protected abstract void onUnbind(IEntity entity);

	@Override
	public IManager bind(final IEntity entity) {
		if (!isBound(entity)) {
			synchronized (mEntities) {
				mEntities.put(entity.getId(), entity);
			}
			notifyEntityBound(entity, getArgs(entity));
			L.i("Entity %s bound to %s", entity.getId(), this);
		}
		return this;
	}

	@Override
	public int countBoundEntities() {
		synchronized (mEntities) {
			return mEntities.size();
		}
	}

	@Override
	public synchronized IManager forEachEntity(final IConsumer<IEntity> consumer) {
		int i = 0;
		final Set<IEntityId> keys;
		synchronized (mEntities) {
			keys = new HashSet<IEntityId>(mEntities.keySet());
		}
		for (final IEntityId id : keys) {
			consumer.consume(mEntities.get(id), i);
			i++;
		}
		return this;
	}

	@Override
	public IManager forEachEntityId(final IConsumer<IEntityId> consumer) {
		int i = 0;
		final Set<IEntityId> keys;
		synchronized (mEntities) {
			keys = new HashSet<IEntityId>(mEntities.keySet());
		}
		for (final IEntityId id : keys) {
			consumer.consume(id, i);
			i++;
		}
		return this;
	}

	@Override
	public Collection<IEntity> getBoundEntities() {
		synchronized (mEntities) {
			return mEntities.values();
		}
	}

	@Override
	public IEntity getBoundEntity(final IEntityId id) {
		synchronized (mEntities) {
			return mEntities.get(id);
		}
	}

	@Override
	public IContext getContext() {
		return mParent;
	}

	protected Map<IEntityId, IEntity> getEntitiesMapReference() {
		synchronized (mEntities) {
			return mEntities;
		}
	}

	protected IDataServerHandler getHandler() {
		return getContext().getDataServerHandler();
	}

	@Override
	public boolean isBound(final IEntity entity) {
		return isBound(entity.getId());
	}

	@Override
	public boolean isBound(final IEntityId id) {
		synchronized (mEntities) {
			return mEntities.containsKey(id);
		}
	}

	protected void notifyEntityBound(final IEntity entity, final Object... args) {
		for (final IManagerListener l : mListeners) {
			l.onEntityBound(entity, args);
		}
	}

	protected void notifyEntityUnbound(final IEntity entity, final Object... args) {
		for (final IManagerListener l : mListeners) {
			l.onEntityUnbound(entity, args);
		}
	}

	@Override
	public IManager removeAllManagerListeners() {
		mListeners.clear();
		return this;
	}

	@Override
	public IManager removeManagerListener(final IManagerListener listener) {
		mListeners.remove(listener);
		return this;
	}

	@Override
	public IManager setContext(final IContext value) {
		mParent = value;
		return this;
	}

	@Override
	public String toString() {
		return String.format("%s#%s", getClass().getSimpleName(), hashCode());
	}

	@Override
	public IManager unbind(final IEntity entity) {
		if (isBound(entity)) {
			final IEntity removed;
			final Object[] args = getArgs(entity);
			synchronized (mEntities) {
				removed = mEntities.remove(entity.getId());
			}
			if (removed != null && removed.isSameEntity(entity)) {
				notifyEntityUnbound(entity, args);
			}
			L.i("Entity %s unbound from %s", entity.getId(), this);
		}
		return this;
	}
	
	protected Object[] getArgs(IEntity entity) {
		return new Object[0];
	}

}
