package it.unibo.gciatto.thesis.manager;

import it.unibo.gciatto.thesis.util.IEntityDetectedListener;

public interface IEnvironmentManger extends IManager {
	IEnvironmentManger setEntityDetectedListener(IEntityDetectedListener listener);
	IEnvironmentManger notifyPatternDetected(String pattern);
}
