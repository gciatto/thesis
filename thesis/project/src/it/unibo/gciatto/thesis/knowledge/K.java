package it.unibo.gciatto.thesis.knowledge;

public class K {
	public static class addresses {
//		public static final String ADDR_DATA_SERVER = "http://thesis-gciatto.rhcloud.com/server/data";
		// "http://192.168.1.211:8080/server/data";
		public static final String ADDR_DATA_SERVER = "http://192.168.1.7:8080/server/data";

	}

	public static class env {
		public static final String ENV_SW_NAME = "thesis";
		public static final String ENV_BEACONS_UUID = "01234567-89AB-CDEF-0123-456789ABCDEF";
		public static final String ENV_LANDMARK_BEACON_MAJOR = "1";
		public static final String ENV_ENTITY_BEACON_MAJOR = "2";
	}

	public static class ids {
		public static final String ID_BEACONS_UUID = null;
	}

	public static class numbers {
		public static final float NUM_GPS_MIN_DISTANCE = 0f; // m
		public static final float NUM_NW_MIN_DISTANCE = 0f; // m
		public static final double NUM_MIN_SCAN_DISTANCE = 1.5; // m
	}

	public static class timeouts {
		public static final long TIMEOUT_IDS_UPDATE_PERIOD = 30; // seconds
		public static final long TIMEOUT_IDS_UPDATE_INITIAL_DELAY = 5; // seconds

		public static final long TIMEOUT_ATTRIBUTES_UPDATE_PERIOD = 5 * 60; // seconds,
		// 5
		// min
		public static final long TIMEOUT_ATTRIBUTES_UPDATE_INITIAL_DELAY = 11; // seconds

		public static final long TIMEOUT_POSITION_UPDATES_PERIOD = 1; // seconds
		public static final long TIMEOUT_POSITION_UPDATES_INITIAL_DELAY = 10; // seconds

		public static final long TIMEOUT_GPS_MIN_PERIOD = 500; // ms
		public static final long TIMEOUT_NW_MIN_PERIOD = 500; // ms

		public static final int TIMEOUT_CONNECTION = 5000; // ms
		
		public static final int TIMEOUT_USER_SCAN_DURATION = 7; // s

		public static final int TIMEOUT_AUTO_SCAN_DURATION = 2; // s
	}

	private K() {
	}
}
