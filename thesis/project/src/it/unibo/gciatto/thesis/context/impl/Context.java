package it.unibo.gciatto.thesis.context.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;
import it.unibo.gciatto.thesis.manager.impl.EnvironmentManager;
import it.unibo.gciatto.thesis.manager.impl.IdentityManager;
import it.unibo.gciatto.thesis.manager.impl.InformationManager;
import it.unibo.gciatto.thesis.manager.impl.PositionManager;
import it.unibo.gciatto.thesis.util.IIdentityListener;

public abstract class Context implements IContext {

	private final IEntity mSelf;
	private final IIdentityManager mIdentityManager;
	private final IPositionManager mPositionManager;
	private final IInformationManager mInformationManager;
	// private final IDataServerHandler mDataServerHandler;
	private final IEnvironmentManger mEnvironmentManger;

	private final IManager[] mManagers;

	public Context(final IEntity self) {
		mSelf = self;

		mIdentityManager = new IdentityManager(this, mSelf);
		mPositionManager = new PositionManager(this);
		mInformationManager = new InformationManager(this);
		mEnvironmentManger = new EnvironmentManager(this);

		mPositionManager.bind(mSelf);
		mInformationManager.bind(mSelf);

		mIdentityManager.addIdentityListener(new IIdentityListener() {

			@Override
			public void onGoodbyeEntity(final IEntity entity, final Object args) {
				mPositionManager.unbind(entity);
				mInformationManager.unbind(entity);
				mEnvironmentManger.unbind(entity);
				L.i("%s left the system", entity.getId());
			}

			@Override
			public void onWelcomeEntity(final IEntity entity, final Object args) {
				mPositionManager.bind(entity);
				mInformationManager.bind(entity);
				mEnvironmentManger.bind(entity);
				
				mInformationManager.asyncQueryData(entity);

				L.i("%s joined the system", entity.getId());
			}
		});

		mManagers = new IManager[] { 
				mIdentityManager, 
				mPositionManager, 
				mInformationManager,
				mEnvironmentManger
			};
	}

	@Override
	public IContext bindToAllManagers(final IEntity entity) {
		for (final IManager m : mManagers) {
			m.bind(entity);
		}
		return this;
	}

	@Override
	public abstract IDataServerHandler getDataServerHandler();

	@Override
	public IIdentityManager getIdentityManager() {
		return mIdentityManager;
	}

	@Override
	public IInformationManager getInformationManager() {
		return mInformationManager;
	}

	@Override
	public IPositionManager getPositionManager() {
		return mPositionManager;
	}

	@Override
	public IEntity getSelf() {
		return getIdentityManager().getSelf();
	}

	@Override
	public IContext unbindFromAllManagers(final IEntity entity) {
		for (final IManager m : mManagers) {
			m.unbind(entity);
		}
		return this;
	}

	@Override
	public IEnvironmentManger getEnvironmentManager() {
		return mEnvironmentManger;
	}

}
