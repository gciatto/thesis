package it.unibo.gciatto.thesis.context;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;

public interface IContext {
	IContext bindToAllManagers(IEntity entity);

	IDataServerHandler getDataServerHandler();

	IIdentityManager getIdentityManager();

	IInformationManager getInformationManager();

	IPositionManager getPositionManager();

	IEntity getSelf();

	IContext unbindFromAllManagers(IEntity entity);
	
	IEnvironmentManger getEnvironmentManager();
}
