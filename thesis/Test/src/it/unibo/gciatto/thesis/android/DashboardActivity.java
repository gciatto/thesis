package it.unibo.gciatto.thesis.android;

import it.unibo.gciatto.thesis.android.context.impl.SystemContext;
import it.unibo.gciatto.thesis.android.observed.ObservedActivity;
import it.unibo.gciatto.thesis.android.observer.ObserverActivity;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IBeaconDevice;
import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.listener.impl.EntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.handler.IDataServerHandler.ProtocolException;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativePosition;
import it.unibo.gciatto.thesis.util.IIdentityListener;
import it.unibo.gciatto.thesis.util.ITrilaterationListener;
import it.unibo.gciatto.util.android.AttributeSetArrayAdapter;
import it.unibo.gciatto.util.android.CollectionArrayAdapter;
import it.unibo.gciatto.util.android.DeviceCollectionArrayAdapter;
import it.unibo.gciatto.util.android.DistanceCollectionArrayAdpter;
import it.unibo.gciatto.util.android.EntityIdCollectionArrayAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DashboardActivity extends ActionBarActivity implements IContext, NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(final int sectionNumber, final IEntityId id) {
			final PlaceholderFragment fragment = new PlaceholderFragment();
			final Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			if (id != null) {
				args.putSerializable(ARG_ENTITY_ID, id);
			}
			fragment.setArguments(args);
			return fragment;
		}

		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		private static final String ARG_ENTITY_ID = "entity_id";
		private IContext mSysContext = SystemContext.getInstance(null);
		private IEntity mEntity;
		private TextView mLatTextView, mLongTextView, mAltTextView, mAccTextView, mBeaTextView, mSpeeTextView, mTimTextView, mEntityIdTextView, mLodTextView;
		private ListView mDevListView, mAttrListView, mDistListView;
		private CollectionArrayAdapter mDevArrayAdapter, mDistArrayAdapter;
		private AttributeSetArrayAdapter mAttrArrayAdapter;
		private TextView mSectionLabel;

		private final EntityListener mEntityListener = new EntityListener() {

			@Override
			public void onAbsolutePositionFix(final IEntity source, final IAbsolutePosition newPosition, final IAbsolutePosition oldPosition) {
				showAbsolutePosition();
			}

			@Override
			public void onAttributesChanged(final IEntity source) {
				showAttributes();
			}

			@Override
			public void onDeviceAssociated(final IEntity source, final IDevice device) {
				showDevices();
			}

			@Override
			public void onDeviceDissociated(final IEntity source, final IDevice device) {
				showDevices();
			}

			@Override
			public void onRelativeDistanceAdded(final IEntity source, final IEntityRelativeDistance distance) {
				showDistances();
			}

			@Override
			public void onRelativeDistanceRemoved(final IEntity source, final IEntityRelativeDistance distance) {
				showDistances();
			}

			@Override
			public void onLODChanged(IEntity source, ILevelOfDetail newLOD, ILevelOfDetail oldLOD) {
				showEntityId();
			}

		};

		public PlaceholderFragment() {
		}

		@Override
		public void onAttach(final Activity activity) {
			super.onAttach(activity);
			mSysContext = SystemContext.getInstance(null);
			final int number = getArguments().getInt(ARG_SECTION_NUMBER);
			final IEntityId id = (IEntityId) getArguments().getSerializable(ARG_ENTITY_ID);
			((DashboardActivity) activity).onSectionAttached(number);

			if (id != null) {
				mEntity = mSysContext.getIdentityManager().getEntity(id);
				mDevArrayAdapter = new DeviceCollectionArrayAdapter(activity, mEntity.getAssociatedDevices());
				mAttrArrayAdapter = new AttributeSetArrayAdapter(activity, mEntity);
				mDistArrayAdapter = new DistanceCollectionArrayAdpter(activity, mEntity.getRelativeDistances());
				mEntity.addEntityListener(mEntityListener);
			}
		}

		@Override
		public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

			mEntityIdTextView = (TextView) rootView.findViewById(R.id.entityIdTextView);
			mLodTextView = (TextView) rootView.findViewById(R.id.lodTextView);
			mLatTextView = (TextView) rootView.findViewById(R.id.latTextView);
			mLongTextView = (TextView) rootView.findViewById(R.id.longTextView);
			mAltTextView = (TextView) rootView.findViewById(R.id.altTextView);
			mAccTextView = (TextView) rootView.findViewById(R.id.accTextView);
			mBeaTextView = (TextView) rootView.findViewById(R.id.beaTextView);
			mSpeeTextView = (TextView) rootView.findViewById(R.id.speeTextView);
			mTimTextView = (TextView) rootView.findViewById(R.id.timTextView);

			mDevListView = (ListView) rootView.findViewById(R.id.devListView);
			mDistListView = (ListView) rootView.findViewById(R.id.distListView);
			mAttrListView = (ListView) rootView.findViewById(R.id.attrListView);

			mDevListView.setAdapter(mDevArrayAdapter);
			mDistListView.setAdapter(mDistArrayAdapter);
			mAttrListView.setAdapter(mAttrArrayAdapter);

			mSectionLabel = (TextView) rootView.findViewById(R.id.section_label);

			mSysContext.getPositionManager().setTriangulationListener(new ITrilaterationListener() {
				
				@Override
				public void onTrilateration(final IEntity trilaterated, final IAbsolutePosition position, final Object... args) {
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							mSectionLabel.setText(String.format("%s's position is trilaterated to %s", trilaterated.getId().getId(), position));
						}
					});
					
				}
			});

			showEntity();

			return rootView;
		}

		@Override
		public void onDetach() {
			super.onDetach();
			if (mEntity != null) {
				mEntity.removeEntityListener(mEntityListener);
			}
		}

		protected void showAbsolutePosition() {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					final IAbsolutePosition p = mEntity == null ? null : mEntity.getAbsolutePosition();
					mLatTextView.setText(p == null ? "N/D" : String.valueOf(p.getLatitude()));
					mLongTextView.setText(p == null ? "N/D" : String.valueOf(p.getLongitude()));
					mAltTextView.setText(p == null ? "N/D" : String.valueOf(p.getAltitude()));
					mAccTextView.setText(p == null ? "N/D" : String.valueOf(p.getAccurancy()));
					mBeaTextView.setText(p == null ? "N/D" : String.valueOf(p.getBearing()));
					mSpeeTextView.setText(p == null ? "N/D" : String.valueOf(p.getSpeed()));
					mTimTextView.setText(p == null ? "N/D" : String.valueOf(p.getTimestamp()));
				}
			});
		}

		protected void showAttributes() {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (mEntity != null) {
						mAttrArrayAdapter.setItems(mEntity);
					}
				}
			});
		}

		protected void showDevices() {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (mEntity != null) {
						mDevArrayAdapter.setItems(mEntity.getAssociatedDevices());
					}
				}
			});
		}

		protected void showDistances() {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (mEntity != null) {
						mDistArrayAdapter.setItems(mEntity.getRelativeDistances());
					}
				}
			});
		}

		public void showEntity() {
			showEntityId();
			showAbsolutePosition();
			showDevices();
			showAttributes();
			showDistances();
		}

		protected void showEntityId() {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					mEntityIdTextView.setText(mEntity == null ? "N/D" : mEntity.getId().getId());
					mLodTextView.setText(mEntity == null ? "N/D" : mEntity.getLOD().getValueName());
				}
			});
		}

	}

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	private IContext mContext;

	private EntityIdCollectionArrayAdapter mIdsAdapter;

	@Override
	public IContext bindToAllManagers(final IEntity entity) {
		return mContext.bindToAllManagers(entity);
	}

	@Override
	public boolean equals(final Object o) {
		return mContext.equals(o);
	}

	@Override
	public IDataServerHandler getDataServerHandler() {
		return mContext.getDataServerHandler();
	}

	@Override
	public IIdentityManager getIdentityManager() {
		return mContext.getIdentityManager();
	}

	@Override
	public IInformationManager getInformationManager() {
		return mContext.getInformationManager();
	}

	@Override
	public IPositionManager getPositionManager() {
		return mContext.getPositionManager();
	}

	@Override
	public IEntity getSelf() {
		return mContext.getSelf();
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		// Check which request we're responding to
		if (requestCode == BeaconActivity.REQUEST_CREATE_BEACON) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				final IBeaconDevice device = (IBeaconDevice) data.getSerializableExtra(BeaconActivity.EXTRA_BEACON_DEVICE);
				if (device != null) {
					final IEntity self = getIdentityManager().getSelf();
					self.associateDevice(device);
					getIdentityManager().asyncRegisterDevice(self, device);
				}
			}
		}
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContext = SystemContext.getInstance(this);

		setContentView(R.layout.activity_dashboard);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		mIdsAdapter = new EntityIdCollectionArrayAdapter(this, mContext.getIdentityManager().getEntityIds()) {

			@Override
			public void onIdViewGeneration(int position, IEntityId id, TextView textView) {
				if (id.isSameEntity(getSelf().getId())) {
					textView.setText("Self");
				}
			}
			
		};
		getIdentityManager().addIdentityListener(new IIdentityListener() {

			@Override
			public void onGoodbyeEntity(final IEntity entity, final Object args) {
				// mIdsAdapter.add(entity.getId());
				showEntities();
			}

			@Override
			public void onWelcomeEntity(final IEntity entity, final Object args) {
				// mIdsAdapter.remove(entity.getId());
				showEntities();
			}
		});

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
		mNavigationDrawerFragment.setAdapter(mIdsAdapter);
		showEntities();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.dashboard, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onNavigationDrawerItemSelected(final int position) {
		// update the main content by replacing fragments
		final FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.container, PlaceholderFragment.newInstance(position, mIdsAdapter == null ? null : mIdsAdapter.getItem(position))).commit();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int id = item.getItemId();
		switch (id) {
			case R.id.showObserverMenuItem:
				showObserver();
				break;
			case R.id.resetServerMenuItem:
				resetServer();
				break;
			case R.id.associateBeaconMenuItem:
				startActivityForResult(new Intent(this, BeaconActivity.class), BeaconActivity.REQUEST_CREATE_BEACON);
				break;
			case R.id.showObservedMenuItem:
				showObserved();
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onSectionAttached(final int number) {
		if (number >= 0 && mIdsAdapter != null && number < mIdsAdapter.getCount()) {
			mTitle = mIdsAdapter.getItem(number).getId();
		}
	}

	protected void resetServer() {
		final AsyncTask<String, Integer, Boolean> servReset = new AsyncTask<String, Integer, Boolean>() {

			@Override
			protected Boolean doInBackground(final String... params) {
				try {
					getDataServerHandler().syncReset();
					return true;
				} catch (final ProtocolException ex) {
					return false;
				}
			}

			@Override
			protected void onPostExecute(final Boolean result) {
				if (result) {
					Toast.makeText(getApplicationContext(), "Server resetted", Toast.LENGTH_LONG).show();
					getIdentityManager().asyncASAPSynchronize();
				} else {
					Toast.makeText(getApplicationContext(), "Server NOT resetted", Toast.LENGTH_LONG).show();
				}
			}

		};
		servReset.execute();
		// Toast.makeText(this, "Reset server: feature not implemented",
		// Toast.LENGTH_SHORT).show();
	}

	public void restoreActionBar() {
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	protected void showEntities() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				mNavigationDrawerFragment.setIds(getIdentityManager().getEntityIds());
			}
		});
	}

	private void showObserved() {
		final Intent intent = new Intent(this, ObservedActivity.class);
		startActivity(intent);
	}

	protected void showObserver() {
		final Intent intent = new Intent(this, ObserverActivity.class);
		startActivity(intent);
	}

	@Override
	public IContext unbindFromAllManagers(final IEntity entity) {
		return mContext.unbindFromAllManagers(entity);
	}

	@Override
	public IEnvironmentManger getEnvironmentManager() {
		return mContext.getEnvironmentManager();
	}

}
