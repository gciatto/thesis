package it.unibo.gciatto.thesis.android;

import it.unibo.gciatto.thesis.android.context.impl.SystemContext;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.listener.impl.EntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.util.IIdentityListener;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ScrollView;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private static final String TAG = "MainActivity";
	private TextView mTextView, mSelfTextView;
	private ScrollView mScrollView;
	private IContext mContext;
	private final IEntityListener mEntityListener = new EntityListener() {

		@Override
		public void onAbsolutePositionFix(final IEntity source, final IAbsolutePosition newPosition, final IAbsolutePosition oldPosition) {
			writeln("%s position fixed to %s", source.getId(), newPosition);
		}

		@Override
		public void onAttributesChanged(final IEntity source) {
			writeln("%s's attributes changed", source.getId());
		}

		@Override
		public void onDeviceAssociated(final IEntity source, final IDevice device) {
			writeln("%s associated to %s", device, source.getId());
		}

		@Override
		public void onDeviceDissociated(final IEntity source, final IDevice device) {
			writeln("%s dissociated from %s", device, source.getId());
		}

		@Override
		public void onLODChanged(final IEntity source, final ILevelOfDetail newLOD, final ILevelOfDetail oldLOD) {
			writeln("%s's lod changed from %s to %s", source.getId(), oldLOD, newLOD);
		}

		@Override
		public void onRelativeDistanceAdded(final IEntity source, final IEntityRelativeDistance distance) {
			writeln("Added distance %s to %s", distance, source.getId());
		}

		@Override
		public void onRelativeDistanceRemoved(final IEntity source, final IEntityRelativeDistance distance) {
			writeln("Removed distance %s from %s", distance, source.getId());
		}

	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mSelfTextView = (TextView) findViewById(R.id.selfTextView);
		mScrollView = (ScrollView) findViewById(R.id.scrollView);
		mTextView = (TextView) findViewById(R.id.textView);

		mContext = new SystemContext(this);

		mSelfTextView.setText(mContext.getIdentityManager().getSelf().toString());

		mContext.getIdentityManager().getSelf().addEntityListener(mEntityListener);
		mContext.getIdentityManager().getSelf().addEntityListener(new EntityChangeListener() {

			@Override
			public void onChanged(final IEntity source) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mSelfTextView.setText(source.toString());
					}
				});
			}
		});

		mContext.getIdentityManager().addIdentityListener(new IIdentityListener() {

			@Override
			public void onGoodbyeEntity(final IEntity entity, final Object args) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onWelcomeEntity(final IEntity entity, final Object args) {
				entity.addEntityListener(mEntityListener);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void writeln(final String format, final Object... data) {
		if (format == null) {
			return;
		}
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				final String message = String.format(format, data);
				Log.i(TAG, message);
				// mTextView.append("\n");
				// mTextView.append(message);
				// mTextView.append("\n");
				// mScrollView.fullScroll(android.view.View.FOCUS_DOWN);
				mTextView.setText("\n" + message);
			}
		});
	}
}
