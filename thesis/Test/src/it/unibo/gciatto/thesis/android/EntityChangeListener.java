package it.unibo.gciatto.thesis.android;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

public class EntityChangeListener implements IEntityListener {

	@Override
	public void onAbsolutePositionFix(final IEntity source, final IAbsolutePosition newPosition, final IAbsolutePosition oldPosition) {
		onChanged(source);
	}

	@Override
	public void onAttributesChanged(final IEntity source) {
		onChanged(source);
	}

	@Override
	public void onCanSee(final IEntity source, final IEntityRelativeDistance distance) {
		onChanged(source);
	}

	@Override
	public void onCantSeeAnymore(final IEntity source, final IEntity other) {
		onChanged(source);
	}

	public void onChanged(final IEntity source) {

	}

	@Override
	public void onDeviceAssociated(final IEntity source, final IDevice device) {
		onChanged(source);
	}

	@Override
	public void onDeviceDissociated(final IEntity source, final IDevice device) {
		onChanged(source);
	}

	@Override
	public void onGettingCloser(final IEntity source, final IEntityRelativeDistance other) {
		onChanged(source);
	}

	@Override
	public void onGettingFarther(final IEntity source, final IEntityRelativeDistance other) {
		onChanged(source);
	}

	@Override
	public void onLODChanged(final IEntity source, final ILevelOfDetail newLOD, final ILevelOfDetail oldLOD) {
		onChanged(source);
	}

	@Override
	public void onRelativeDistanceAdded(final IEntity source, final IEntityRelativeDistance distance) {
		onChanged(source);
	}

	@Override
	public void onRelativeDistanceRemoved(final IEntity source, final IEntityRelativeDistance distance) {
		onChanged(source);
	}

}
