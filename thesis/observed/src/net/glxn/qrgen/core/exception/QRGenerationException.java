package net.glxn.qrgen.core.exception;

public class QRGenerationException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = -8324910762606456768L;

	public QRGenerationException(final String message, final Throwable underlyingException) {
		super(message, underlyingException);
	}
}
