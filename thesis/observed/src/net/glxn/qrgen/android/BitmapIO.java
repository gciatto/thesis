package net.glxn.qrgen.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import net.glxn.qrgen.core.image.ImageType;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

public class BitmapIO {

	public static boolean write(final Bitmap image, final String type, final File file) throws IOException {
		OutputStream stream = null;
		try {
			stream = new FileOutputStream(file);
			return write(image, type, stream);
		} catch (final IOException ioe) {
			throw ioe;
		} finally {
			if (stream != null) {
				stream.flush();
				stream.close();
			}
		}
	}

	public static boolean write(final Bitmap image, final String type, final OutputStream stream) throws IOException {
		if (type.equals(ImageType.PNG)) {
			return image.compress(CompressFormat.PNG, 80, stream);
		} else if (type.equals(ImageType.JPG)) {
			return image.compress(CompressFormat.JPEG, 80, stream);
		}

		// Default should be Bitmap for android
		return BmpUtil.save(image, stream);

	}
}
