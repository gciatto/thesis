package it.unibo.gciatto.thesis.android.observed;

import it.unibo.gciatto.thesis.android.context.impl.SystemContext;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;
import net.glxn.qrgen.android.QRCode;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.sax.TextElementListener;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class ObservedActivity extends ActionBarActivity implements IContext {
	private IContext mContext;

	private ImageView mQrImageView;
	
	private View mContainer;

	private QRCode mQrCode;
	
	@SuppressLint("NewApi")
	private class ObservedDialog extends DialogFragment {
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage("You're creating an observed entity")
	               .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       final EditText msgEditText = (EditText) ObservedDialog.this.getDialog().findViewById(R.id.editText1);
	                       final String msg = msgEditText.getText().toString();
	                       if (msg != null && mContext != null && mContext.getSelf() != null && !msg.isEmpty()) {
	                    	   mContext.getSelf().putAttribute(IEntity.ATTR_MESSAGE, msg);
	                    	   mContext.getInformationManager().asyncASAPSynchronize();
	                       }
	                   }
	               })
	               .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       ObservedDialog.this.getDialog().cancel();
	                   }
	               })
	               .setView(View.inflate(getApplicationContext(), R.layout.observed_dialog, null));
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
	}
	
	 protected void showDialog() {
	        // Create an instance of the dialog fragment and show it
	        final DialogFragment dialog = new ObservedDialog();
	        dialog.show(getSupportFragmentManager(), "ObservedFragment");
	    }


	public IEnvironmentManger getEnvironmentManager() {
		return mContext.getEnvironmentManager();
	}

	@Override
	public IContext bindToAllManagers(final IEntity entity) {
		return mContext.bindToAllManagers(entity);
	}

	@Override
	public IDataServerHandler getDataServerHandler() {
		return mContext.getDataServerHandler();
	}

	@Override
	public IIdentityManager getIdentityManager() {
		return mContext.getIdentityManager();
	}

	@Override
	public IInformationManager getInformationManager() {
		return mContext.getInformationManager();
	}

	@Override
	public IPositionManager getPositionManager() {
		return mContext.getPositionManager();
	}

	protected float getScreenDimension(final String dimension) {
		final DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		if (dimension.equalsIgnoreCase("width")) {
			return metrics.widthPixels;
		} else if (dimension.equalsIgnoreCase("height")) {
			return metrics.heightPixels;
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public IEntity getSelf() {
		return mContext.getSelf();
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_observed);

		mContext = SystemContext.getInstance(this);

		mQrImageView = (ImageView) findViewById(R.id.qrImageView);
		mContainer = findViewById(R.id.observedContainer);

		final DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		mQrCode = QRCode.from(getSelf().getId().toUri().toString());

		mQrImageView.setImageBitmap(mQrCode.bitmap());
		
		mContainer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final float ratio;

				if (mContainer.getWidth() >= mContainer.getHeight()) {
					ratio = (float) mContainer.getWidth() / mQrCode.getWidth();
				} else {
					ratio = (float) mContainer.getHeight() / mQrCode.getHeight();
				}

				mQrImageView.setScaleX(ratio);
				mQrImageView.setScaleY(ratio);
			}
		});
		
		final float ratio;

		if (metrics.widthPixels >= metrics.heightPixels) {
			ratio = (float) metrics.widthPixels / mQrCode.getWidth();
		} else {
			ratio = (float) metrics.heightPixels / mQrCode.getHeight();
		}

		mQrImageView.setScaleX(ratio);
		mQrImageView.setScaleY(ratio);
		
		showDialog();
	}

	@SuppressLint("NewApi")
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.observed, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public IContext unbindFromAllManagers(final IEntity entity) {
		return mContext.unbindFromAllManagers(entity);
	}
}
