package org.altbeacon.beacon.simulator;

import java.util.List;

import org.altbeacon.beacon.Beacon;

/**
 * Created by dyoung on 4/18/14.
 */
public class StaticBeaconSimulator implements BeaconSimulator {

	public List<Beacon> beacons = null;

	@Override
	public List<Beacon> getBeacons() {
		return beacons;
	}

	public void setBeacons(final List<Beacon> beacons) {
		this.beacons = beacons;
	}
}
