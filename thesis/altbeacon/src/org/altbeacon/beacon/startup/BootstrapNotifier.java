package org.altbeacon.beacon.startup;

import org.altbeacon.beacon.MonitorNotifier;

import android.content.Context;

public interface BootstrapNotifier extends MonitorNotifier {
	public Context getApplicationContext();
}
