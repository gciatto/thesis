package org.altbeacon.beacon.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;

public class RangedBeacon {
	private class Measurement implements Comparable<Measurement> {
		Integer rssi;
		long timestamp;

		@Override
		public int compareTo(final Measurement arg0) {
			return rssi.compareTo(arg0.rssi);
		}
	}

	public static void setSampleExpirationMilliseconds(final long milliseconds) {
		sampleExpirationMilliseconds = milliseconds;
	}

	private static String TAG = "RangedBeacon";
	public static long DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS = 20000; /* 20
																		 * seconds */
	private static long sampleExpirationMilliseconds = DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS;
	private boolean mTracked = true;

	Beacon mBeacon;

	private ArrayList<Measurement> measurements = new ArrayList<Measurement>();

	public RangedBeacon(final Beacon beacon) {
		mBeacon = beacon;
		addMeasurement(mBeacon.getRssi());
	}

	public void addMeasurement(final Integer rssi) {
		mTracked = true;
		final Measurement measurement = new Measurement();
		measurement.rssi = rssi;
		measurement.timestamp = new Date().getTime();
		measurements.add(measurement);
	}

	protected void addRangeMeasurement(final Integer rssi) {
		mBeacon.setRssi(rssi);
		addMeasurement(rssi);
	}

	private double calculateRunningAverage() {
		refreshMeasurements();
		final int size = measurements.size();
		int startIndex = 0;
		int endIndex = size - 1;
		if (size > 2) {
			startIndex = size / 10 + 1;
			endIndex = size - size / 10 - 2;
		}

		double sum = 0;
		for (int i = startIndex; i <= endIndex; i++) {
			sum += measurements.get(i).rssi;
		}
		final double runningAverage = sum / (endIndex - startIndex + 1);

		BeaconManager.logDebug(TAG, "Running average mRssi based on " + size + " measurements: " + runningAverage);
		return runningAverage;

	}

	// Done at the end of each cycle before data are sent to the client
	public void commitMeasurements() {
		if (measurements.size() > 0) {
			final double runningAverage = calculateRunningAverage();
			mBeacon.setRunningAverageRssi(runningAverage);
			BeaconManager.logDebug(TAG, "calculated new runningAverageRssi:" + runningAverage);
		} else {
			BeaconManager.logDebug(TAG, "No measurements available to calculate running average");
		}
	}

	public Beacon getBeacon() {
		return mBeacon;
	}

	public boolean isTracked() {
		return mTracked;
	}

	public boolean noMeasurementsAvailable() {
		return measurements.size() == 0;
	}

	private synchronized void refreshMeasurements() {
		final Date now = new Date();
		final ArrayList<Measurement> newMeasurements = new ArrayList<Measurement>();
		final Iterator<Measurement> iterator = measurements.iterator();
		while (iterator.hasNext()) {
			final Measurement measurement = iterator.next();
			if (now.getTime() - measurement.timestamp < sampleExpirationMilliseconds) {
				newMeasurements.add(measurement);
			}
		}
		measurements = newMeasurements;
		Collections.sort(measurements);
	}

	public void setTracked(final boolean tracked) {
		mTracked = tracked;
	}

}
