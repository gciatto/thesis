package org.altbeacon.beacon;

/**
 *
 * @author Giovanni Ciatto
 * @see http
 *      ://stackoverflow.com/questions/25223385/how-to-detect-region-enter-exit
 *      -for-multiple-beacons-using-altbeacon-android-bea
 */
public class NoFilterBeaconParser extends BeaconParser {
	protected static final String NO_FILTER_PATTERN = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";

	public NoFilterBeaconParser() {
		super();
		setBeaconLayout(NO_FILTER_PATTERN);
	}

}
