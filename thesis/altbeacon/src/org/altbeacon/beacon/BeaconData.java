package org.altbeacon.beacon;

/**
 * Server-side data associated with a beacon. Requires registration of a web
 * service to fetch the data.
 */
public interface BeaconData {
	public String get(String key);

	public Double getLatitude();

	public Double getLongitude();

	public boolean isDirty();

	public void set(String key, String value);

	public void setLatitude(Double latitude);

	public void setLongitude(Double longitude);

	public void sync(BeaconDataNotifier notifier);
}
