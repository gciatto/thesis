package it.unibo.gciatto.thesis.context.impl;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.handler.impl.DataServerHandlerSimulator;

public class SimulationContext extends Context {

	private final IDataServerHandler mDataServerHandler;

	public SimulationContext(final IEntity self) {
		super(self);

		mDataServerHandler = new DataServerHandlerSimulator(this);
	}

	@Override
	public IDataServerHandler getDataServerHandler() {
		return mDataServerHandler;
	}

}
