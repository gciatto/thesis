package it.unibo.gciatto.thesis.entity.id.impl;

import it.unibo.gciatto.thesis.device.impl.SelfDevice;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.impl.Entity;

public final class Self extends Entity {
	public static Entity getSelf() {
		if (INSTANCE == null) {
			INSTANCE = new Self(new EntityId(USER_NAME));
			INSTANCE.associateDevice(SelfDevice.getSelfDevice());
			INSTANCE.putAttribute("firstName", "Giovanni");
			INSTANCE.putAttribute("secondName", "Ciatto");
		}
		return INSTANCE;
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -1941084100999181554L;

	private static final String USER_NAME = "gciatto";

	private static Self INSTANCE;

	private Self(final IEntityId id) {
		super(id);
	}

}
