package it.unibo.gciatto.thesis.device.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class SelfDevice extends Device {

	public static final Device getSelfDevice() {
		if (INSTANCE == null) {
			try {
				INSTANCE = (SelfDevice) new SelfDevice(TYPE_PC, InetAddress.getLocalHost().getHostName()).setIp(InetAddress.getLocalHost().getHostAddress());
			} catch (final UnknownHostException e) {
				new SelfDevice(TYPE_PC, "gciattoPC");
			}
		}
		return INSTANCE;
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1946192766272493720L;

	private static SelfDevice INSTANCE; // = (SelfDevice) new

	// SelfDevice(TYPE_PC,
	// InetAddress.getLocalHost().getHostName()).setIp(InetAddress.getLocalHost().getHostAddress());

	private SelfDevice(final String type, final String name) {
		super(type, name);
		// TODO Auto-generated constructor stub
	}

}
