package it.unibo.gciatto.thesis.simulation;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.context.impl.SimulationContext;
import it.unibo.gciatto.thesis.entity.id.impl.Self;
import it.unibo.gciatto.thesis.handler.impl.AbsolutePositionHandlerSimulator;
import it.unibo.gciatto.thesis.handler.impl.DataServerHandlerSimulator;
import it.unibo.gciatto.thesis.log.L;

import java.io.IOException;

public class Simulation {

	public static void main(final String[] args) {
		final IContext context = new SimulationContext(Self.getSelf());
		final AbsolutePositionHandlerSimulator posSim = new AbsolutePositionHandlerSimulator(context);
		final DataServerHandlerSimulator server = (DataServerHandlerSimulator) context.getDataServerHandler();
		posSim.init();

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					synchronized (System.out) {
						System.out.println("ENTITIES:");
						System.out.println(context.getIdentityManager().getBoundEntities().toString());
						System.out.println();
						System.out.println("ONSERVER:");
						System.out.println(DataServerHandlerSimulator.mData.toString());
						System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
					}
				}
			}
		}).start();

		try {
			System.in.read();
		} catch (final IOException e) {
			L.e(e);
		}
	}

}
