package it.unibo.gciatto.thesis.handler.impl;

import it.unibo.gciatto.thesis.context.IContext;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SensorSimulator extends BaseHandler implements ScheduledExecutorService {

	private final ScheduledExecutorService mExecutor = Executors.newSingleThreadScheduledExecutor();

	public SensorSimulator(final IContext context) {
		super(context);
	}

	@Override
	public boolean awaitTermination(final long arg0, final TimeUnit arg1) throws InterruptedException {
		return mExecutor.awaitTermination(arg0, arg1);
	}

	@Override
	public void execute(final Runnable command) {
		mExecutor.execute(command);
	}

	@Override
	public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> arg0) throws InterruptedException {
		return mExecutor.invokeAll(arg0);
	}

	@Override
	public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> arg0, final long arg1, final TimeUnit arg2) throws InterruptedException {
		return mExecutor.invokeAll(arg0, arg1, arg2);
	}

	@Override
	public <T> T invokeAny(final Collection<? extends Callable<T>> arg0) throws InterruptedException, ExecutionException {
		return mExecutor.invokeAny(arg0);
	}

	@Override
	public <T> T invokeAny(final Collection<? extends Callable<T>> arg0, final long arg1, final TimeUnit arg2) throws InterruptedException, ExecutionException, TimeoutException {
		return mExecutor.invokeAny(arg0, arg1, arg2);
	}

	@Override
	public boolean isShutdown() {
		return mExecutor.isShutdown();
	}

	@Override
	public boolean isTerminated() {
		return mExecutor.isTerminated();
	}

	@Override
	public <V> ScheduledFuture<V> schedule(final Callable<V> callable, final long delay, final TimeUnit unit) {
		return mExecutor.schedule(callable, delay, unit);
	}

	@Override
	public ScheduledFuture<?> schedule(final Runnable command, final long delay, final TimeUnit unit) {
		return mExecutor.schedule(command, delay, unit);
	}

	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(final Runnable command, final long initialDelay, final long period, final TimeUnit unit) {
		return mExecutor.scheduleAtFixedRate(command, initialDelay, period, unit);
	}

	@Override
	public ScheduledFuture<?> scheduleWithFixedDelay(final Runnable command, final long initialDelay, final long delay, final TimeUnit unit) {
		return mExecutor.scheduleWithFixedDelay(command, initialDelay, delay, unit);
	}

	@Override
	public void shutdown() {
		mExecutor.shutdown();
	}

	@Override
	public List<Runnable> shutdownNow() {
		return mExecutor.shutdownNow();
	}

	@Override
	public <T> Future<T> submit(final Callable<T> arg0) {
		return mExecutor.submit(arg0);
	}

	@Override
	public Future<?> submit(final Runnable arg0) {
		return mExecutor.submit(arg0);
	}

	@Override
	public <T> Future<T> submit(final Runnable arg0, final T arg1) {
		return mExecutor.submit(arg0, arg1);
	}

}
