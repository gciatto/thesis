package it.unibo.gciatto.thesis.handler.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.handler.IAbsolutePositionHandler;
import it.unibo.gciatto.thesis.handler.IHandler;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.AbsolutePosition;
import it.unibo.gciatto.thesis.util.IAbsolutePositionListener;
import it.unibo.gciatto.util.impl.MathUtils.rand;

import java.util.concurrent.TimeUnit;

public class AbsolutePositionHandlerSimulator extends SensorSimulator implements IAbsolutePositionHandler {

	private static final long DELAY = 3 * 1000;
	private static final long PERIOD = 5000;

	private static final double LAT = 37.99589;
	private static final double LONG = 15.41268;
	private static final double RADIOUS = 0.0002;

	private IAbsolutePositionListener mAbsPosListener;
	private final Runnable mUpdateTask = new Runnable() {

		@Override
		public void run() {
			final double dlat = rand.randomPositiveDouble(RADIOUS);
			final double dlng = rand.randomPositiveDouble(RADIOUS);
			final IAbsolutePosition pos = new AbsolutePosition(LAT + dlat, LONG + dlng);
			notifySelfPosition(pos);
			L.i("%s generated new position: %s", this, pos);
		}
	};

	public AbsolutePositionHandlerSimulator(final IContext context) {
		super(context);

		setAbsolutePositionListener(context.getPositionManager());

	}

	@Override
	public IHandler init() {
		scheduleAtFixedRate(mUpdateTask, DELAY, PERIOD, TimeUnit.MILLISECONDS);
		return super.init();
	}

	protected void notifySelfPosition(final IAbsolutePosition position) {
		if (mAbsPosListener != null) {
			mAbsPosListener.onAbsolutePosition(position, this);
		}
	}

	@Override
	public IAbsolutePositionHandler setAbsolutePositionListener(final IAbsolutePositionListener listener) {
		mAbsPosListener = listener;
		return this;
	}

	@Override
	public String toString() {
		return String.format("AbsolutePositionHandlerSimulator#" + hashCode());
	}

}
