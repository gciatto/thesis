package it.unibo.gciatto.thesis.handler.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.impl.Entity;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.log.L;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.AbsolutePosition;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.impl.AttributeSet;
import it.unibo.gciatto.util.impl.MathUtils.rand;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataServerHandlerSimulator extends BaseHandler implements IDataServerHandler {

	public static final int DELAY_MIN = 1; // milliseconds
	public static final int DELAY_MAX = 1000; // milliseconds

	public static final Map<IEntityId, IEntity> mData = new HashMap<IEntityId, IEntity>();

	public DataServerHandlerSimulator() {
		super(null);
	}

	public DataServerHandlerSimulator(final IContext context) {
		super(context);
	}

	protected Map<IEntityId, IAbsolutePosition> generatePositionMap() {
		final Map<IEntityId, IAbsolutePosition> map = new HashMap<IEntityId, IAbsolutePosition>();
		synchronized (mData) {
			for (final IEntityId id : mData.keySet()) {
				map.put(id, mData.get(id).getAbsolutePosition());
			}
		}
		return map;
	}

	protected int getRandomDelay() {
		return rand.randomInt(DELAY_MIN, DELAY_MAX, true);
	}

	protected void simulateDelay() {
		try {
			Thread.sleep(getRandomDelay());
		} catch (final InterruptedException e) {
			L.e(e);
		}
	}

	@Override
	public synchronized IDataServerHandler syncAddData(final IEntityId id, final IAttributeSet attributes) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to add entity %s's attributes", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
			} else {
				entity = new Entity(id);
				mData.put(id, entity);
			}
		}
		entity.putAllAttributes(attributes);
		L.i("Server successfully added attributes %s to entity %s", attributes, id);
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncAddRelativeDistance(final IEntityId id, final IEntityRelativeDistance distance) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to add entity %s's relative distance", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
			} else {
				entity = new Entity(id);
				mData.put(id, entity);
			}
		}
		entity.addRelativeDistance(distance);
		L.i("Server successfully added relative distance %s to entity %s", distance, id);
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncFixAbsolutePosition(final IEntityId id, final IAbsolutePosition position) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to fix entity %s's absolute position", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
			} else {
				entity = new Entity(id);
				mData.put(id, entity);
			}
		}
		entity.move(position);
		L.i("Server successfully fixed entity %s's absolute position to %s", id, position);
		return this;
	}

	@Override
	public synchronized IAttributeSet syncGetAttributes(final IEntityId id) throws ProtocolException {
		L.i("Contacting server to request entity %s's attributes", id);
		final IEntity entity;
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
				L.i("Server successfully provided entity %s's attributes", id);
				return new AttributeSet(entity);
			} else {
				L.i("Server couldn't provide entity %s's attributes, becuse it wasn't registered", id);
				return null;
			}
		}
	}

	@Override
	public synchronized Collection<IDevice> syncGetDevices(final IEntityId id) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to request entity %s's devices list", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
				L.i("Server successfully provided device list for entity %s", id);
				return new ArrayList<IDevice>(entity.getAssociatedDevices());
			} else {
				L.i("Server couldn't provide device list for entity %s", id);
				return null;
			}
		}
	}

	@Override
	public synchronized Set<IEntity> syncGetEverything() {
		L.i("Contacting server to request everything");
		simulateDelay();
		L.i("Server successfully provided everything");
		synchronized (mData) {
			return new HashSet<IEntity>(mData.values());
		}
	}

	@Override
	public synchronized Set<IEntityId> syncGetIds() throws ProtocolException {
		L.i("Contacting server to get id set");
		simulateDelay();
		L.i("Server successfully provided an id set");
		synchronized (mData) {
			return new HashSet<IEntityId>(mData.keySet());
		}
	}

	@Override
	public synchronized IAbsolutePosition syncGetPosition(final IEntityId id) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to request entity %s's absolute position", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
				L.i("Server successfully provided entity %s's absolute position", id);
				return new AbsolutePosition(entity.getAbsolutePosition());
			} else {
				L.i("Server couldn't provide entity %s's absolute position, becuse it wasn't registered", id);
				return null;
			}
		}
	}

	@Override
	public synchronized Map<IEntityId, IAbsolutePosition> syncGetPositions() throws ProtocolException {
		L.i("Contacting server to request all positions");
		simulateDelay();
		L.i("Server successfully provided positions map");
		return generatePositionMap();
	}

	@Override
	public synchronized Collection<IEntityRelativeDistance> syncGetRelativeDistances(final IEntityId id) throws ProtocolException {
		L.i("Contacting server to request all relative distances of entity %s", id);
		final IEntity entity;
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
				L.i("Server successfully provided entity %s's relative distances list", id);
				return new ArrayList<IEntityRelativeDistance>(entity.getRelativeDistances());
			} else {
				L.i("Server couldn't provide entity %s's relative distances list, becuse it wasn't registered", id);
				return null;
			}
		}
	}

	@Override
	public synchronized IDataServerHandler syncRegisterDevice(final IEntityId id, final IDevice dev) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to register device %s for entity %s", dev, id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
			} else {
				entity = new Entity(id);
				mData.put(id, entity);
			}
		}
		entity.associateDevice(dev);
		L.i("Server successfully registered device %s for entity %s", dev, id);
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncRegisterEntity(final IEntityId id) throws ProtocolException {
		L.i("Contacting server to register entity %s", id);
		simulateDelay();
		synchronized (mData) {
			if (!mData.containsKey(id)) {
				mData.put(id, new Entity(id));
				L.i("Server successfully registered entity %s", id);
			} else {
				L.i("Server didn't register entity %s beacuse it has been already registered", id);
			}
		}
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncRemoveData(final IEntityId id, final IAttributeSet attributes) throws ProtocolException {
		// TODO implement
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public synchronized IDataServerHandler syncRemoveRelativeDistance(final IEntityId id, final IEntityRelativeDistance distance) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to remove entity %s's relative distance", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
				entity.removeRelativeDistance(distance);
				L.i("Server successfully removed relative distance %s from entity %s", distance, id);
			} else {
				L.i("Server didn't removed entity %s's relative position, because it wasn't registered", id);
			}
		}
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncReset() throws ProtocolException {
		L.i("Contacting server to reset remote data");
		simulateDelay();
		synchronized (mData) {
			mData.clear();
		}
		L.i("Server successfully resetted");
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncUnregisterDevice(final IEntityId id, final IDevice dev) throws ProtocolException {
		final IEntity entity;
		L.i("Contacting server to unregister device %s for entity %s", dev, id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				entity = mData.get(id);
				entity.disassociateDevice(dev);
				L.i("Server successfully unregistered device %s for entity %s", dev, id);
			} else {
				L.i("Server didn't unregister device %s for entity %s, because it wasn't registered", dev, id);
			}
		}
		return this;
	}

	@Override
	public synchronized IDataServerHandler syncUnregisterEntity(final IEntityId id) throws ProtocolException {
		L.i("Contacting server to unregister entity %s", id);
		simulateDelay();
		synchronized (mData) {
			if (mData.containsKey(id)) {
				mData.remove(id);
				L.i("Server successfully unregistered entity %s", id);
			} else {
				L.i("Server didn't unregister entity %s beacuse it wasn't registered", id);
			}
		}
		return this;
	}

}
