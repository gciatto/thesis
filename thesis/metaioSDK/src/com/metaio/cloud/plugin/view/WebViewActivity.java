/**
 * WebViewActivity.java Junaio 2.6 Android
 *
 * Junaio Web View
 *
 * @author Created by Arsalan Malik on 1631 24.06.2011 Copyright 2011 metaio
 *         GmbH. All rights reserved.
 */
package com.metaio.cloud.plugin.view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.metaio.R;
import com.metaio.cloud.plugin.MetaioCloudPlugin;
import com.metaio.cloud.plugin.data.MetaioCloudDataManager;
import com.metaio.cloud.plugin.util.JunaioChannel;
import com.metaio.cloud.plugin.util.MetaioCloudUtils;
import com.metaio.sdk.jni.ASWorldReturnCode;
import com.metaio.sdk.jni.AS_MetaioWorldRequestCommand;
import com.metaio.sdk.jni.MetaioWorldRequest;
import com.metaio.sdk.jni.MetaioWorldRequestChannelsManageGet;

public class WebViewActivity extends Activity {
	private class JunaioWebChromeClient extends WebChromeClient {

		ViewGroup customView;

		WebChromeClient.CustomViewCallback mCustomViewCallback;

		@Override
		public Bitmap getDefaultVideoPoster() {
			return BitmapFactory.decodeResource(getResources(), R.drawable.default_video_poster);
		}

		@Override
		public View getVideoLoadingProgressView() {
			return new ProgressBar(getApplicationContext(), null, android.R.attr.progressBarStyleLarge);
		}

		@Override
		public boolean onConsoleMessage(final ConsoleMessage consoleMessage) {
			final int isBetaResId = getResources().getIdentifier("isBeta", "boolean", getPackageName());
			final boolean isBeta = isBetaResId > 0 ? getResources().getBoolean(isBetaResId) : false;

			// if we are in beta, display errors as toasts
			if (MetaioCloudPlugin.isDebuggable || isBeta) {
				Toast.makeText(getApplicationContext(), consoleMessage.message(), Toast.LENGTH_LONG).show();
			}
			MetaioCloudPlugin.log(consoleMessage.message());

			return super.onConsoleMessage(consoleMessage);
		}

		@Override
		public void onExceededDatabaseQuota(final String url, final String databaseIdentifier, final long currentQuota, final long estimatedSize, final long totalUsedQuota, final WebStorage.QuotaUpdater quotaUpdater) {
			quotaUpdater.updateQuota(estimatedSize * 2);
		}

		@Override
		public void onGeolocationPermissionsShowPrompt(final String origin, final GeolocationPermissions.Callback callback) {
			callback.invoke(origin, true, false);
		}

		@Override
		public void onHideCustomView() {
			try {
				if (customView != null) {
					final ViewParent viewparent = customView.getParent();
					if (viewparent != null) {
						((ViewGroup) viewparent).removeView(customView);
					}
				}
			} catch (final Exception e) {
				Log.e("WebViewActivity", "Can't remove custom view (video player)", e);
			}
			mCustomViewCallback.onCustomViewHidden();
		}

		@Override
		public boolean onJsAlert(final WebView view, final String url, final String message, final JsResult result) {
			MetaioCloudPlugin.log("onJsAlert " + message);

			// display javascript alerts as AlertDialogs
			new AlertDialog.Builder(view.getContext()).setTitle("javaScript dialog").setMessage(message).setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog, final int which) {
					result.confirm();
				}
			}).setCancelable(false).create().show();

			return true;
		}

		@Override
		public boolean onJsTimeout() {
			MetaioCloudPlugin.log("onJsTimeout");
			return false;
		}

		@Override
		public void onProgressChanged(final WebView view, final int progress) {
			super.onProgressChanged(view, progress);
			mProgressView.setIndeterminate(false);
			mProgressView.setProgress(progress);
		}

		@Override
		public void onShowCustomView(final View view, final WebChromeClient.CustomViewCallback callback) {
			customView = (ViewGroup) LayoutInflater.from(getApplicationContext()).inflate(R.layout.html5container, null);

			mCustomViewCallback = callback;
			final FrameLayout mHtml5Container = (FrameLayout) customView.findViewById(R.id.html5viewcontainer);
			mHtml5Container.addView(view);

			final ImageButton closeButton = (ImageButton) customView.findViewById(R.id.buttonClose);
			closeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					onHideCustomView();
				}
			});

			final ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
			root.addView(customView);

		}
	}

	private class JunaioWebViewClient extends WebViewClient implements MetaioCloudDataManager.Callback {

		private void changeButtonState(final WebView webview) {
			if (mButtonBack != null && mButtonForward != null) {
				if (webview.canGoBack()) {
					mButtonBack.setEnabled(true);
				} else {
					mButtonBack.setEnabled(false);
				}
				if (webview.canGoForward()) {
					mButtonForward.setEnabled(true);
				} else {
					mButtonForward.setEnabled(false);
				}
			}
		}

		@Override
		public void onLoadResource(final WebView view, final String url) {
			super.onLoadResource(view, url);
			view.resumeTimers();
			MetaioCloudPlugin.log("onLoadResource " + url);
		}

		@Override
		public void onPageFinished(final WebView view, final String url) {
			MetaioCloudPlugin.log("Finished loading " + url);
			mProgressView.setVisibility(View.GONE);

			if (mButtonStop != null) {
				mButtonStop.setEnabled(false);
			}
			changeButtonState(view);
			view.resumeTimers();
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(final WebView view, final String url, final Bitmap favicon) {
			MetaioCloudPlugin.log("Started loading " + url);
			mProgressView.setVisibility(View.VISIBLE);

			if (mButtonStop != null) {
				mButtonStop.setEnabled(true);
			}
			changeButtonState(view);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onReceivedError(final WebView view, final int errorCode, final String description, final String failingUrl) {
			MetaioCloudPlugin.log("Failed loading " + failingUrl + " " + description);
			mProgressView.setVisibility(View.GONE);

			MetaioCloudUtils.showToast(WebViewActivity.this, description);
			super.onReceivedError(view, errorCode, description, failingUrl);
		}

		@Override
		public void onRequestCancelled(final MetaioWorldRequest request) {

		}

		@Override
		public void onRequestCompleted(final MetaioWorldRequest request, final int error) {

			if (error == ASWorldReturnCode.AS_WORLD_API_SUCCESS.swigValue()) {
				if (request.getCommand() == AS_MetaioWorldRequestCommand.AS_MetaioWorldRequestType_CHANNELS_MANAGE_GET) {
					MetaioCloudPlugin.getDataSource().setChannel(((MetaioWorldRequestChannelsManageGet) request).getResult());
					finish();
				}
			}

		}

		@Override
		public void onRequestStarted() {
		}

		@Override
		public boolean shouldOverrideUrlLoading(final WebView view, String url) {
			JunaioChannel channel = null;

			try {
				// Try to launch default intent first if valid
				final Intent intent = MetaioCloudPlugin.getDefaultIntent(url);
				if (intent != null) {
					try {
						startActivity(intent);
						return true;
					} catch (final Exception e) {
						MetaioCloudPlugin.log(Log.ERROR, "WebViewActivity: Failed to launched the default intent");
						return false;
					}
				}
			} catch (final Exception e) {
			}
			try {

				if (url.compareToIgnoreCase("junaio://?action=closewebview") == 0) {
					MetaioCloudPlugin.log("Closing webview: " + url);
					finish();
					return true;
				} else if ((channel = MetaioCloudUtils.parseUrl(Uri.parse(url))) != null && channel.getChannelID() > -1) {
					MetaioCloudPlugin.log("Channel ID: " + channel.getChannelID());

					final MetaioWorldRequestChannelsManageGet channelsGetRequest = new MetaioWorldRequestChannelsManageGet();
					channelsGetRequest.setChannelID(channel.getChannelID());
					for (final String name : channel.getFilters().keySet()) {
						channelsGetRequest.setParameter(name, channel.getFilters().get(name));
					}

					MetaioCloudPlugin.getDataManager().addRequest(channelsGetRequest);

					MetaioCloudPlugin.getDataManager().registerCallback(this);

					return true;
				}
				// Open in Google Docs viewer if supported file type (based on
				// file extention)
				else if (MetaioCloudPlugin.isSupportedOnGoogleDocs(url) && !url.contains("docs.google.com/gview?embedded")) {
					url = "http://docs.google.com/gview?embedded=true&url=" + url;
					view.loadUrl(url);
					return true;

				} else if (url.contains("youtube.com")) {
					final Uri parsedUrl = Uri.parse(url);
					final Intent youtubeIntent = new Intent(Intent.ACTION_VIEW, parsedUrl);
					startActivity(youtubeIntent);
					finish();
					return true;
				} else if (url.startsWith("http")) {
					view.loadUrl(url);
					return false; // load it in webview
				} else {
					final Intent externalIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					externalIntent.addCategory(Intent.CATEGORY_BROWSABLE);
					startActivity(externalIntent);
					return true;
				}
			} catch (final Exception e) {
				return false;
			}

			// return false;
		}

	}

	/**
	 * Web view
	 */
	private WebView mWebView;

	/**
	 * Progress bar displayed when loading a page
	 */
	private ProgressBar mProgressView;

	/**
	 * Navigation buttons that can be disabled
	 */
	private ImageButton mButtonBack, mButtonForward, mButtonStop;

	private static final int ID_OPENWINDOWLINK = 0x5;

	/**
	 * Set this to true to have a fullscreen webview without navigation controls
	 */
	public static boolean hideNavigationBar;

	/**
	 * Click handlers for buttons
	 *
	 * @param target
	 *            Button that is clicked
	 */
	public void onButtonClickHandler(final View target) {
		try {

			if (target.getId() == R.id.buttonWebBack) {
				if (mWebView.canGoBack()) {
					mWebView.goBack();
				}
			} else if (target.getId() == R.id.buttonWebReload) {
				mWebView.reload();
			}
			// else if (target.getId() == R.id.buttonWebStop)
			// {
			// mWebView.stopLoading();
			// }
			else if (target.getId() == R.id.buttonWebForward) {
				if (mWebView.canGoForward()) {
					mWebView.goForward();
				}
			} else if (target.getId() == R.id.buttonClose) {
				finish();
			} else if (target.getId() == R.id.buttonShare) {
				final Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebView.getUrl()));
				startActivity(Intent.createChooser(i, getString(R.string.BTN_SHARING)));
			}
		} catch (final Exception e) {

		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			// get url and navigatio preference (default true)
			final String url = getIntent().getStringExtra(getPackageName() + ".URL");
			final boolean navigation = getIntent().getBooleanExtra(getPackageName() + ".NAVIGATION", true);

			setContentView(R.layout.webviewnav);

			// if we want navigation bar and not hide it, make it visible. Make
			// it invisible if not.
			if (navigation && !hideNavigationBar) {
				findViewById(R.id.webBottomBar).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.webBottomBar).setVisibility(View.GONE);
			}

			mButtonBack = (ImageButton) findViewById(R.id.buttonWebBack);
			mButtonForward = (ImageButton) findViewById(R.id.buttonWebForward);
			mButtonStop = (ImageButton) findViewById(R.id.buttonWebStop);

			mProgressView = (ProgressBar) findViewById(R.id.progressBar);
			mProgressView.setIndeterminate(true);

			// init webview
			mWebView = (WebView) findViewById(R.id.webView);

			// disable hw accel as it creates flickering pages, html5 video
			// won't work with this
			if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 16) {
				mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			// This hides white bar on the right
			mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

			final WebSettings settings = mWebView.getSettings();
			// enable plugins before java script
			settings.setPluginState(PluginState.ON);

			// TODO test these two settings with a big webpage (cafe lotty?)
			settings.setLoadWithOverviewMode(true);
			settings.setUseWideViewPort(true);
			// enable javascript and zoom controls
			settings.setJavaScriptEnabled(true);
			settings.setBuiltInZoomControls(true);
			settings.setGeolocationEnabled(true);
			settings.setDatabaseEnabled(true);
			final String databasePath = getDir("database_ext", Context.MODE_PRIVATE).getPath();
			settings.setDatabasePath(databasePath);
			settings.setGeolocationDatabasePath(databasePath);
			settings.setDomStorageEnabled(true);
			settings.setAppCacheEnabled(true);

			// allow XMLHttpRequests
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				settings.setAllowUniversalAccessFromFileURLs(true);
			}

			final JunaioWebViewClient client = new JunaioWebViewClient();
			mWebView.setWebViewClient(client);
			mWebView.setWebChromeClient(new JunaioWebChromeClient());

			registerForContextMenu(mWebView);

			if (savedInstanceState != null) {
				mWebView.restoreState(savedInstanceState);
			} else {

				// if we don't have to override the url, load it in the webview
				if (!client.shouldOverrideUrlLoading(mWebView, url)) {
					mWebView.loadUrl(url);
				}
			}
		} catch (final Exception e) {
			MetaioCloudPlugin.log(Log.ERROR, "WebViewActivity.onCreate", e);
		}
	}

	@Override
	public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		final HitTestResult result = ((WebView) v).getHitTestResult();

		final MenuItem.OnMenuItemClickListener handler = new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(final MenuItem item) {
				switch (item.getItemId()) {
					case ID_OPENWINDOWLINK:
						final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(result.getExtra()));
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						break;

					default:
						break;
				}
				return true;
			}
		};

		if (result.getType() == HitTestResult.IMAGE_TYPE || result.getType() == HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
			// Menu options for an image.
			// set the header title to the image url
			menu.setHeaderTitle(result.getExtra());
			menu.add(0, ID_OPENWINDOWLINK, 0, getString(R.string.MENU_OPEN_IMG_EXTERNAL)).setOnMenuItemClickListener(handler);
		} else if (result.getType() == HitTestResult.ANCHOR_TYPE || result.getType() == HitTestResult.SRC_ANCHOR_TYPE) {
			// Menu options for a hyperlink.
			// set the header title to the link url
			menu.setHeaderTitle(result.getExtra());
			menu.add(0, ID_OPENWINDOWLINK, 0, getString(R.string.MENU_OPEN_LINK_EXTERNAL)).setOnMenuItemClickListener(handler);
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		try {
			MetaioCloudUtils.unbindDrawables(findViewById(android.R.id.content));

			mWebView.destroy();

		} catch (final Exception e) {

		}
	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {
		try {
			// if we push back button and the browser can go back then go back
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if (mWebView.canGoBack()) {
					mWebView.goBack();
					return true;
				}
			}
		} catch (final Exception e) {

		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		mWebView.saveState(outState);
	}

}
