package it.unibo.gciatto.thesis.android.http.impl;

import java.net.URI;

import org.apache.http.client.methods.HttpPost;

public class HttpDeleteWithBody extends HttpPost {
	public static final String METHOD_NAME = "DELETE";

	public HttpDeleteWithBody() {

	}

	public HttpDeleteWithBody(final String uri) {
		super(uri);
	}

	public HttpDeleteWithBody(final URI uri) {
		super(uri);
	}

	@Override
	public String getMethod() {
		return METHOD_NAME;
	}

}
