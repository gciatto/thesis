package it.unibo.gciatto.thesis.android.device.impl;

import it.unibo.gciatto.thesis.device.impl.Device;

import java.net.InetAddress;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;

@SuppressLint("NewApi")
public final class SelfDevice extends Device {

	public static final SelfDevice getSelfDevice(final Context appContext) {
		if (INSTANCE == null) {
			INSTANCE = new SelfDevice(appContext);
		}
		return INSTANCE;
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1946192766272493720L;

	private static SelfDevice INSTANCE;

	private SelfDevice(final Context appContext) {
		super(TYPE_ANDROID, Build.SERIAL);
		final WifiManager manager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE);
		final WifiInfo info = manager.getConnectionInfo();
		final String mac = info.getMacAddress();
		setMac(mac);
		try {
			final String ip = InetAddress.getLocalHost().getHostAddress();
			setIp(ip);
		} catch (final Exception e) {

		}
	}

}
