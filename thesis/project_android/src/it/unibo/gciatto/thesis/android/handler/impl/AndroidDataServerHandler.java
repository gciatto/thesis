package it.unibo.gciatto.thesis.android.handler.impl;

import it.unibo.gciatto.http.IHttpRequest;
import it.unibo.gciatto.http.IHttpRequest.Method;
import it.unibo.gciatto.http.IHttpResponse;
import it.unibo.gciatto.thesis.android.http.IAndroidHttpResponseHandler;
import it.unibo.gciatto.thesis.android.http.impl.AndroidProtocolHttpRequest;
import it.unibo.gciatto.thesis.android.http.impl.JsonHttpResponseHandler2;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.handler.impl.DataServerHandler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json2.JSONArray;
import org.json2.JSONObject;

import android.util.Log;

import com.loopj.android.http.SyncHttpClient;

public class AndroidDataServerHandler extends DataServerHandler implements IDataServerHandler {

	private static final String TAG = "serverHandler";

	private final SyncHttpClient mClient;
//	private final ExecutorService mWorker = Executors.newSingleThreadExecutor();

	public AndroidDataServerHandler(final IContext context) {
		super(context);

		mClient = new SyncHttpClient();
		mClient.setTimeout(5000);
	}

	protected IHttpResponse executeRequest(final AndroidProtocolHttpRequest request) throws ProtocolException {
		Log.i(TAG, "request: " + request.toString());
		IAndroidHttpResponseHandler handler;
		switch (request.getMethod()) {
			case DELETE:
				handler = new JsonHttpResponseHandler2();
				mClient.delete(null, request.getURI().toString(), null, request.getRequestParams(), handler);
				break;
			case GET:
				handler = new JsonHttpResponseHandler2();
				mClient.get(request.getURI().toString(), request.getRequestParams(), handler);
				break;
			case POST:
				handler = new JsonHttpResponseHandler2();
				mClient.post(null, request.getQueryURL(), request.getEntity(), "application/json", handler);
				break;
			default:
				throw new IllegalStateException();

		}
		Log.i(TAG, "response: " + handler.getResponse().toString());
		return handler.getResponse();
	}

	@Override
	protected IHttpResponse executeRequest(final IHttpRequest request) throws ProtocolException {
		if (!(request instanceof AndroidProtocolHttpRequest)) {
			throw new IllegalStateException();
		}
		final AndroidProtocolHttpRequest rq = (AndroidProtocolHttpRequest) request;
		// final Future<IHttpResponse> future = mWorker.submit(new
		// Callable<IHttpResponse>() {
		//
		// @Override
		// public IHttpResponse call() throws Exception {
		// return executeRequest(rq);
		// }
		// });
		// try {
		// return future.get();
		// } catch (final Exception e) {
		// throw new ProtocolException(e);
		// }
		return executeRequest(rq);
	}

	@Override
	protected IHttpRequest generateRequest(final Method method, final String what, final IEntityId id, final Object body) {
		return new AndroidProtocolHttpRequest(getServerURI(), method, what, id, body);
	}

	protected SyncHttpClient getClient() {
		return mClient;
	}

	@Override
	protected JSONArray readJsonArray(final IHttpResponse response) {
		// final Reader reader = response.getBodyReader();
		// final JSONArray array = new JSONArray(new JSONTokener(reader));
		// try {
		// reader.close();
		// } catch (IOException e) {
		// L.e(e);
		// }
		// return array;
		return (JSONArray) response.getBody();
	}

	@Override
	protected JSONObject readJsonObject(final IHttpResponse response) {
		// final Reader reader = response.getBodyReader();
		// final JSONObject object = new JSONObject(new JSONTokener(reader));
		// try {
		// reader.close();
		// } catch (IOException e) {
		// L.e(e);
		// }
		// return object;
		return (JSONObject) response.getBody();
	}

}
