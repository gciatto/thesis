package it.unibo.gciatto.thesis.android.entity.impl;

import it.unibo.gciatto.thesis.android.device.impl.BeaconDevice;
import it.unibo.gciatto.thesis.entity.id.impl.EntityId;
import it.unibo.gciatto.thesis.entity.impl.Entity;

public class BeaconEntity extends Entity {

	/**
	 *
	 */
	private static final long serialVersionUID = -75693565736450601L;

	public BeaconEntity(final BeaconDevice dev) {
		super(new EntityId(dev.getMac()));
		associateDevice(dev);
	}
}
