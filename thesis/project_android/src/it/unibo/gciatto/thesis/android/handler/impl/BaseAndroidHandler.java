package it.unibo.gciatto.thesis.android.handler.impl;

import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.handler.impl.BaseHandler;
import android.content.Context;

public class BaseAndroidHandler extends BaseHandler {

	private final Context mApplicationContext;

	public BaseAndroidHandler(final IContext context, final Context appContext) {
		super(context);

		mApplicationContext = appContext;
	}

	public Context getApplicationContext() {
		return mApplicationContext;
	}

}
