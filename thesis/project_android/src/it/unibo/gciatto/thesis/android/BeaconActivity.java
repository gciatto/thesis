package it.unibo.gciatto.thesis.android;

import it.unibo.gciatto.thesis.android.device.impl.BeaconDevice;
import it.unibo.gciatto.thesis.device.IBeaconDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class BeaconActivity extends ActionBarActivity {

	private EditText mMacEditText, mUuidEditText, mMajorEditText, mMinorEditText;

	public static final int REQUEST_CREATE_BEACON = 1;

	public static final String EXTRA_BEACON_DEVICE = BeaconActivity.class.getSimpleName() + ".device";

	private void cancel() {
		setResult(RESULT_CANCELED);
		finish();
	}

	private void done() {
		final IBeaconDevice device = new BeaconDevice(mMacEditText.getText().toString(), mUuidEditText.getText().toString(), mMajorEditText.getText().toString(), mMinorEditText.getText().toString());
		final Intent data = new Intent();
		data.putExtra(EXTRA_BEACON_DEVICE, device);
		setResult(RESULT_OK, data);
		finish();
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_beacon);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.beacon, menu);

		mMacEditText = (EditText) findViewById(R.id.macEditText);
		mMinorEditText = (EditText) findViewById(R.id.minorEditText);
		mMajorEditText = (EditText) findViewById(R.id.majorEditText);
		mUuidEditText = (EditText) findViewById(R.id.uuidEditText);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int id = item.getItemId();
		if (id == R.id.action_done) {
			done();
		} else if (id == R.id.action_cancel) {
			cancel();
		}
		return super.onOptionsItemSelected(item);
	}
}
