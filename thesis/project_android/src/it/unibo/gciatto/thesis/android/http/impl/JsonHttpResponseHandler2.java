/* Android Asynchronous Http Client Copyright (c) 2011 James Smith
 * <james@loopj.com> http://loopj.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License. */

package it.unibo.gciatto.thesis.android.http.impl;

import it.unibo.gciatto.http.IHttpResponse;
import it.unibo.gciatto.thesis.android.http.IAndroidHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.json2.JSONArray;
import org.json2.JSONException;
import org.json2.JSONObject;
import org.json2.JSONTokener;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

/**
 * Used to intercept and handle the responses from requests made using
 * {@link AsyncHttpClient}, with automatic parsing into a {@link JSONObject} or
 * {@link JSONArray}.
 * <p>
 * &nbsp;
 * </p>
 * This class is designed to be passed to get, post, put and delete requests
 * with the
 * {@link #onSuccess(int, org.apache.http.Header[], org.json.JSONArray)} or
 * {@link #onSuccess(int, org.apache.http.Header[], org.json.JSONObject)}
 * methods anonymously overridden.
 * <p>
 * &nbsp;
 * </p>
 * Additionally, you can override the other event methods from the parent class.
 */
public class JsonHttpResponseHandler2 extends TextHttpResponseHandler implements IAndroidHttpResponseHandler {
	private static final String LOG_TAG = "JsonHttpResponseHandler";

	private IHttpResponse mResponse;

	/**
	 * Creates new JsonHttpResponseHandler, with JSON String encoding UTF-8
	 */
	public JsonHttpResponseHandler2() {
		super(DEFAULT_CHARSET);
	}

	/**
	 * Creates new JsonHttpRespnseHandler with given JSON String encoding
	 *
	 * @param encoding
	 *            String encoding to be used when parsing JSON
	 */
	public JsonHttpResponseHandler2(final String encoding) {
		super(encoding);
	}

	@Override
	public IHttpResponse getResponse() {
		return mResponse;
	}

	@Override
	public final void onFailure(final int statusCode, final Header[] headers, final byte[] responseBytes, final Throwable throwable) {
		if (responseBytes != null) {
			final Runnable parser = new Runnable() {
				@Override
				public void run() {
					try {
						final Object jsonResponse = parseResponse(responseBytes);
						postRunnable(new Runnable() {
							@Override
							public void run() {
								if (jsonResponse instanceof JSONObject) {
									onFailure(statusCode, headers, throwable, (JSONObject) jsonResponse);
								} else if (jsonResponse instanceof JSONArray) {
									onFailure(statusCode, headers, throwable, (JSONArray) jsonResponse);
								} else if (jsonResponse instanceof String) {
									onFailure(statusCode, headers, (String) jsonResponse, throwable);
								} else {
									onFailure(statusCode, headers, new JSONException("Unexpected response type " + jsonResponse.getClass().getName()), (JSONObject) null);
								}
							}
						});

					} catch (final JSONException ex) {
						postRunnable(new Runnable() {
							@Override
							public void run() {
								onFailure(statusCode, headers, ex, (JSONObject) null);
							}
						});

					}
				}
			};
			if (!getUseSynchronousMode()) {
				new Thread(parser).start();
			} else {
				parser.run();
			}
		} else {
			Log.v(LOG_TAG, "response body is null, calling onFailure(Throwable, JSONObject)");
			onFailure(statusCode, headers, throwable, (JSONObject) null);
		}
	}

	@Override
	public void onFailure(final int statusCode, final Header[] headers, final String responseString, final Throwable throwable) {
		mResponse = new AndroidHttpResponse(statusCode, "", responseString);
	}

	/**
	 * Returns when request failed
	 *
	 * @param statusCode
	 *            http response status line
	 * @param headers
	 *            response headers if any
	 * @param throwable
	 *            throwable describing the way request failed
	 * @param errorResponse
	 *            parsed response if any
	 */
	public void onFailure(final int statusCode, final Header[] headers, final Throwable throwable, final JSONArray errorResponse) {
		mResponse = new AndroidHttpResponse(statusCode, "", errorResponse);
	}

	/**
	 * Returns when request failed
	 *
	 * @param statusCode
	 *            http response status line
	 * @param headers
	 *            response headers if any
	 * @param throwable
	 *            throwable describing the way request failed
	 * @param errorResponse
	 *            parsed response if any
	 */
	public void onFailure(final int statusCode, final Header[] headers, final Throwable throwable, final JSONObject errorResponse) {
		mResponse = new AndroidHttpResponse(statusCode, "", errorResponse);
	}

	@Override
	public final void onSuccess(final int statusCode, final Header[] headers, final byte[] responseBytes) {
		if (statusCode != HttpStatus.SC_NO_CONTENT) {
			final Runnable parser = new Runnable() {
				@Override
				public void run() {
					try {
						final Object jsonResponse = parseResponse(responseBytes);
						postRunnable(new Runnable() {
							@Override
							public void run() {
								if (jsonResponse instanceof JSONObject) {
									onSuccess(statusCode, headers, (JSONObject) jsonResponse);
								} else if (jsonResponse instanceof JSONArray) {
									onSuccess(statusCode, headers, (JSONArray) jsonResponse);
								} else if (jsonResponse instanceof String) {
									onFailure(statusCode, headers, (String) jsonResponse, new JSONException("Response cannot be parsed as JSON data"));
								} else {
									onFailure(statusCode, headers, new JSONException("Unexpected response type " + jsonResponse.getClass().getName()), (JSONObject) null);
								}

							}
						});
					} catch (final JSONException ex) {
						postRunnable(new Runnable() {
							@Override
							public void run() {
								onFailure(statusCode, headers, ex, (JSONObject) null);
							}
						});
					}
				}
			};
			if (!getUseSynchronousMode()) {
				new Thread(parser).start();
			} else {
				parser.run();
			}
		} else {
			onSuccess(statusCode, headers, new JSONObject());
		}
	}

	/**
	 * Returns when request succeeds
	 *
	 * @param statusCode
	 *            http response status line
	 * @param headers
	 *            response headers if any
	 * @param response
	 *            parsed response if any
	 */
	public void onSuccess(final int statusCode, final Header[] headers, final JSONArray response) {
		mResponse = new AndroidHttpResponse(statusCode, "", response);
	}

	/**
	 * Returns when request succeeds
	 *
	 * @param statusCode
	 *            http response status line
	 * @param headers
	 *            response headers if any
	 * @param response
	 *            parsed response if any
	 */
	public void onSuccess(final int statusCode, final Header[] headers, final JSONObject response) {
		mResponse = new AndroidHttpResponse(statusCode, "", response);
	}

	@Override
	public void onSuccess(final int statusCode, final Header[] headers, final String responseString) {
		mResponse = new AndroidHttpResponse(statusCode, "", responseString);
	}

	/**
	 * Returns Object of type {@link JSONObject}, {@link JSONArray}, String,
	 * Boolean, Integer, Long, Double or {@link JSONObject#NULL}, see
	 * {@link org.json.JSONTokener#nextValue()}
	 *
	 * @param responseBody
	 *            response bytes to be assembled in String and parsed as JSON
	 * @return Object parsedResponse
	 * @throws org.json.JSONException
	 *             exception if thrown while parsing JSON
	 */
	protected Object parseResponse(final byte[] responseBody) throws JSONException {
		if (null == responseBody) {
			return null;
		}
		Object result = null;
		// trim the string to prevent start with blank, and test if the string
		// is valid JSON, because the parser don't do this :(. If JSON is not
		// valid this will return null
		String jsonString = getResponseString(responseBody, getCharset());
		if (jsonString != null) {
			jsonString = jsonString.trim();
			if (jsonString.startsWith("{") || jsonString.startsWith("[")) {
				result = new JSONTokener(jsonString).nextValue();
			}
		}
		if (result == null) {
			result = jsonString;
		}
		return result;
	}
}