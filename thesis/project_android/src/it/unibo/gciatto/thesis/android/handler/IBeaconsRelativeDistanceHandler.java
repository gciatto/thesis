package it.unibo.gciatto.thesis.android.handler;

import it.unibo.gciatto.thesis.device.IBeaconDevice;
import it.unibo.gciatto.thesis.handler.IRelativeDistanceHandler;

import java.util.List;

public interface IBeaconsRelativeDistanceHandler extends IRelativeDistanceHandler {
	List<IBeaconDevice> getVisibileBeacons();
}
