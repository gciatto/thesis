package it.unibo.gciatto.thesis.android.http.impl;

import it.unibo.gciatto.http.IHttpResponse;

import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

public class AndroidHttpResponseAdapter implements IHttpResponse {
	private final HttpResponse mResponse;

	public AndroidHttpResponseAdapter(final HttpResponse response) {
		mResponse = response;
	}

	@Override
	public Object getBody() {
		throw new IllegalStateException("not implemented");
	}

	@Override
	public Reader getBodyReader() {
		try {
			return new InputStreamReader(mResponse.getEntity().getContent());
		} catch (final Exception e) {
			return null;
		}
	}

	@Override
	public int getStatusCode() {
		return mResponse.getStatusLine().getStatusCode();
	}

	@Override
	public String getStatusLine() {
		return mResponse.getStatusLine().toString();
	}

	@Override
	public boolean isOK() {
		return getStatusCode() == HttpStatus.SC_OK;
	}

}
