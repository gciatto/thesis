package it.unibo.gciatto.thesis.android.context.impl;

import it.unibo.gciatto.thesis.android.device.impl.BeaconDevice;
import it.unibo.gciatto.thesis.android.entity.impl.Self;
import it.unibo.gciatto.thesis.android.handler.impl.AbsolutePositionHandler;
import it.unibo.gciatto.thesis.android.handler.impl.AndroidDataServerHandler;
import it.unibo.gciatto.thesis.android.handler.impl.RelativeDistanceHandler;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.context.impl.Context;
import it.unibo.gciatto.thesis.handler.IAbsolutePositionHandler;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.handler.IRelativeDistanceHandler;
import it.unibo.gciatto.thesis.knowledge.K;
import android.app.Activity;

public class SystemContext extends Context {

	public static IContext getInstance(final Activity appContext) {
		if (INSTANCE == null) {
			INSTANCE = new SystemContext(appContext);
		}
		return INSTANCE;
	}

	public static void setInstance(final IContext context) {
		if (context != null) {
			INSTANCE = context;
		}
	}

	private final android.content.Context mAppContext;
	private final IDataServerHandler mDataServerHandler;
	private final IAbsolutePositionHandler mAbsPositionHandler;

	private final IRelativeDistanceHandler mRelDistanceHandler;

	private static IContext INSTANCE;

	public SystemContext(final Activity appContext) {
		super(Self.getSelf(appContext));

		// bindToAllManagers(new BeaconEntity(BeaconDevice.getBeacon1()));
		// bindToAllManagers(new BeaconEntity(BeaconDevice.getBeacon2()));
		
//		getSelf().associateDevice(new BeaconDevice("78:A5:04:4A:4F:78", K.ids.ID_BEACONS_UUID, "" + 2, "" + 1));

		mAppContext = appContext;
		mDataServerHandler = new AndroidDataServerHandler(this);
		mAbsPositionHandler = new AbsolutePositionHandler(this, mAppContext);
		mRelDistanceHandler = new RelativeDistanceHandler(this, appContext);

		mAbsPositionHandler.init();
		mRelDistanceHandler.init();

		// TODO intercept triangulation
		// getPositionManager().setTriangulationListener(new
		// ITriangulationListener() {
		//
		// @Override
		// public void onTriangulation(IRelativePosition relPos, Object... args)
		// {
		// // L.i(null);
		// }
		// });
	}

	protected IAbsolutePositionHandler getAbsolutePositionHandler() {
		return mAbsPositionHandler;
	}

	protected android.content.Context getApplicationContext() {
		return mAppContext;
	}

	@Override
	public IDataServerHandler getDataServerHandler() {
		return mDataServerHandler;
	}

	protected IRelativeDistanceHandler getRelativeDistanceHandler() {
		return mRelDistanceHandler;
	}

}
