package it.unibo.gciatto.thesis.android.http.impl;

import it.unibo.gciatto.http.IHttpResponse;
import it.unibo.gciatto.thesis.android.http.IAndroidHttpResponseHandler;

import org.apache.http.Header;

import com.loopj.android.http.TextHttpResponseHandler;

public class SimpleHttpResponseHandler extends TextHttpResponseHandler implements IAndroidHttpResponseHandler {

	private IHttpResponse mResponse;

	@Override
	public IHttpResponse getResponse() {
		return mResponse;
	}

	@Override
	public void onFailure(final int statusCode, final Header[] headers, final String responseString, final Throwable throwable) {
		mResponse = new AndroidHttpResponse(statusCode, "", responseString);
	}

	@Override
	public void onSuccess(final int statusCode, final Header[] headers, final String responseString) {
		mResponse = new AndroidHttpResponse(statusCode, "", responseString);
	}

}
