package it.unibo.gciatto.thesis.android.http;

import it.unibo.gciatto.http.IHttpResponse;

import com.loopj.android.http.ResponseHandlerInterface;

public interface IAndroidHttpResponseHandler extends ResponseHandlerInterface {
	IHttpResponse getResponse();
}
