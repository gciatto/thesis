package it.unibo.gciatto.thesis.android.handler.impl;

import it.unibo.gciatto.thesis.android.device.impl.BeaconDevice;
import it.unibo.gciatto.thesis.android.handler.IBeaconsRelativeDistanceHandler;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.device.IBeaconDevice;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.entity.position.impl.EntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.IHandler;
import it.unibo.gciatto.thesis.handler.IRelativeDistanceHandler;
import it.unibo.gciatto.thesis.util.IRelativeDistanceListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.NoFilterBeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import android.util.Log;

public class RelativeDistanceHandler extends BaseAndroidHandler implements IBeaconsRelativeDistanceHandler, BeaconConsumer {

	private static final String TAG = RelativeDistanceHandler.class.getSimpleName();
	private static final Region REGION = new Region("region", null, null, null);

	private IRelativeDistanceListener mRelDistListener;
	private final BeaconManager mBeaconManager;
	private final Map<String, Beacon> mVisibleBeacons = new HashMap<String, Beacon>();

	private final RangeNotifier mRangeNotifier = new RangeNotifier() {

		@Override
		public void didRangeBeaconsInRegion(final Collection<Beacon> beacons, final Region region) {
			final Set<String> keys = getVisibleBeacons();
			for (final String key : keys) {
				if (!beacons.contains(getBeacon(key))) {
					final IEntity entity = getContext().getIdentityManager().getEntityFromMAC(key);
					if (entity != null) {
						notifyRemoveRelativeDistance(new EntityRelativeDistance(entity, Double.NaN));
					}
					makeBeaconInvisible(key);
				}
			}
			for (final Beacon b : beacons) {
				makeBeaconVisible(b);
				final IEntity entity = getContext().getIdentityManager().getEntityFromMAC(b.getBluetoothAddress());
				if (entity != null) {
					notifyAddRelativeDistance(new EntityRelativeDistance(entity, b.getDistance()));
				}
			}
		}
	};

	public RelativeDistanceHandler(final IContext context, final Activity appContext) {
		super(context, appContext);

		setRelativeDistanceListener(getContext().getPositionManager());

		mBeaconManager = BeaconManager.getInstanceForApplication(appContext, new NoFilterBeaconParser());
	}

	@Override
	public boolean bindService(final Intent intent, final ServiceConnection connection, final int mode) {
		return getActivity().bindService(intent, connection, mode);
	}

	protected Activity getActivity() {
		return (Activity) getApplicationContext();
	}

	protected Beacon getBeacon(final String address) {
		synchronized (mVisibleBeacons) {
			return mVisibleBeacons.get(address);
		}
	}

	protected BeaconManager getBeaconManager() {
		return mBeaconManager;
	}

	@Override
	public List<IBeaconDevice> getVisibileBeacons() {
		final ArrayList<IBeaconDevice> visibles;
		synchronized (mVisibleBeacons) {
			visibles = new ArrayList<IBeaconDevice>(mVisibleBeacons.size());
			for (final Beacon b : mVisibleBeacons.values()) {
				visibles.add(new BeaconDevice(b));
			}
		}
		return visibles;
	}

	protected Set<String> getVisibleBeacons() {
		synchronized (mVisibleBeacons) {
			return new HashSet<String>(mVisibleBeacons.keySet());
		}
	}

	@Override
	public IHandler init() {

		mBeaconManager.bind(this);

		return super.init();
	}

	protected boolean isBeaconVisible(final Beacon beacon) {
		synchronized (mVisibleBeacons) {
			return mVisibleBeacons.containsKey(beacon.getBluetoothAddress());
		}
	}

	protected boolean makeBeaconInvisible(final String address) {
		synchronized (mVisibleBeacons) {
			return mVisibleBeacons.remove(address) != null;
		}
	}

	protected boolean makeBeaconVisible(final Beacon beacon) {
		synchronized (mVisibleBeacons) {
			if (!isBeaconVisible(beacon)) {
				mVisibleBeacons.put(beacon.getBluetoothAddress(), beacon);
				return true;
			} else {
				return false;
			}
		}
	}

	protected void notifyAddRelativeDistance(final IEntityRelativeDistance entityRelativeDistance) {
		if (mRelDistListener != null) {
			mRelDistListener.onRelativeDistance(entityRelativeDistance, IRelativeDistanceListener.OP_ADD);
		}
	}

	protected void notifyRemoveRelativeDistance(final IEntityRelativeDistance entityRelativeDistance) {
		if (mRelDistListener != null) {
			mRelDistListener.onRelativeDistance(entityRelativeDistance, IRelativeDistanceListener.OP_REMOVE);
		}
	}

	@Override
	public void onBeaconServiceConnect() {
		try {
			getBeaconManager().setRangeNotifier(mRangeNotifier);
			getBeaconManager().startRangingBeaconsInRegion(REGION);
		} catch (final RemoteException ex) {
			Log.e(TAG, "", ex);
		}
	}

	@Override
	public IRelativeDistanceHandler setRelativeDistanceListener(final IRelativeDistanceListener listener) {
		mRelDistListener = listener;
		return this;
	}

	@Override
	public void unbindService(final ServiceConnection connection) {
		getActivity().unbindService(connection);
	}
}
