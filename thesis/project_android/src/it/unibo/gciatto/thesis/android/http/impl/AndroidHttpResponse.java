package it.unibo.gciatto.thesis.android.http.impl;

import it.unibo.gciatto.http.IHttpResponse;

import java.io.Reader;

import org.apache.http.HttpStatus;

public class AndroidHttpResponse implements IHttpResponse {

	private final int mStatusCode;
	private final String mStatusLine;
	private final Object mBody;

	public AndroidHttpResponse(final int status, final String line, final Object body) {
		mStatusCode = status;
		mStatusLine = line;
		mBody = body;
	}

	@Override
	public Object getBody() {
		return mBody;
	}

	@Override
	public Reader getBodyReader() {
		throw new IllegalStateException("not implemented");
	}

	@Override
	public int getStatusCode() {
		return mStatusCode;
	}

	@Override
	public String getStatusLine() {
		return mStatusLine;
	}

	@Override
	public boolean isOK() {
		return getStatusCode() == HttpStatus.SC_OK;
	}

	@Override
	public String toString() {
		return String.format("%s [StatusCode=%s, Body=%s, isOK=%s]", getClass().getSimpleName(), mStatusCode, mBody, isOK());
	}

}
