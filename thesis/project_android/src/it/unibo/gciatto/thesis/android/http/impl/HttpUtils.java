package it.unibo.gciatto.thesis.android.http.impl;

import it.unibo.gciatto.util.IAttributeSet;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicNameValuePair;

public final class HttpUtils {
	public static List<NameValuePair> attributeSetToList(final IAttributeSet attributes) {
		final Set<String> keys = attributes.getAttributeKeys();
		final List<NameValuePair> list = new ArrayList<NameValuePair>(attributes.countAttributes());
		for (final String key : keys) {
			list.add(new BasicNameValuePair(key, attributes.getAttribute(key).toString()));
		}
		return list;
	}

	public static String createQueryURL(final URI baseUri, final IAttributeSet params) {
		return createQueryURL(baseUri, attributeSetToList(params));
	}

	public static String createQueryURL(final URI baseUri, final List<NameValuePair> params) {
		final StringBuilder sb = new StringBuilder(baseUri.toString());
		sb.append("?");
		sb.append(URLEncodedUtils.format(params, "utf-8"));
		return sb.toString();
	}

	public static Writer getBodyWriter(final HttpEntityEnclosingRequestBase rq) {
		final PipedInputStream sink = new PipedInputStream();
		PipedOutputStream writer;
		try {
			writer = new PipedOutputStream(sink);
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		rq.setEntity(new InputStreamEntity(sink, DATA_SIZE));

		final PrintWriter pwriter = new PrintWriter(writer);

		return pwriter;
	}

	public static Writer getBodyWriter(final HttpEntityEnclosingRequestBase rq, final int contentLength) {
		final PipedInputStream sink = new PipedInputStream();
		PipedOutputStream writer;
		try {
			writer = new PipedOutputStream(sink);
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		rq.setEntity(new InputStreamEntity(sink, contentLength));

		final PrintWriter pwriter = new PrintWriter(writer);

		return pwriter;
	}

	public static void setParams(final HttpRequestBase rq, final String uri, final IAttributeSet params) {
		setParams(rq, uri, attributeSetToList(params));
	}

	public static void setParams(final HttpRequestBase rq, final String uri, final List<NameValuePair> params) {
		try {
			setParams(rq, new URI(uri), params);
		} catch (final URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setParams(final HttpRequestBase rq, final URI uri, final IAttributeSet params) {
		setParams(rq, uri, attributeSetToList(params));
	}

	public static void setParams(final HttpRequestBase rq, final URI uri, final List<NameValuePair> params) {
		final StringBuilder sb = new StringBuilder(uri.toString());
		sb.append("?");
		sb.append(URLEncodedUtils.format(params, "utf-8"));
		try {
			rq.setURI(new URI(sb.toString()));
		} catch (final URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static final long DATA_SIZE = 1024;

	private HttpUtils() {

	}
}
