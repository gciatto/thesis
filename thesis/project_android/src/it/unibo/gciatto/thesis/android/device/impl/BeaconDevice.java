package it.unibo.gciatto.thesis.android.device.impl;

import it.unibo.gciatto.thesis.device.IBeaconDevice;
import it.unibo.gciatto.thesis.device.IMutableBeaconDevice;
import it.unibo.gciatto.thesis.device.impl.Device;
import it.unibo.gciatto.thesis.knowledge.K;

import org.altbeacon.beacon.Beacon;

public class BeaconDevice extends Device implements IMutableBeaconDevice {

	public static IBeaconDevice getBeacon1() {
		return new BeaconDevice("78:A5:04:4A:54:BB", K.env.ENV_BEACONS_UUID, K.env.ENV_LANDMARK_BEACON_MAJOR, "1");
	}

	public static IBeaconDevice getBeacon2() {
		return new BeaconDevice("78:A5:04:4A:54:F8", K.env.ENV_BEACONS_UUID, K.env.ENV_LANDMARK_BEACON_MAJOR, "2");
	}

	public static IBeaconDevice getBeacon3() {
		return new BeaconDevice("78:A5:04:4A:4F:78", K.env.ENV_BEACONS_UUID, K.env.ENV_ENTITY_BEACON_MAJOR, "1");
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 5493036075602306268L;

	public BeaconDevice(final Beacon beacon) {
		super(TYPE_BEACON, String.format("beacon[%s]", beacon.getBluetoothAddress()));
		setMac(beacon.getBluetoothAddress());
		setUUID(beacon.getId1().toString());
		setMajor(beacon.getId2().toString());
		setMinor(beacon.getId3().toString());
	}

	public BeaconDevice(final String mac, final String uuid, final String major, final String minor) {
		super(TYPE_BEACON, String.format("beacon[%s]", mac));
		setMac(mac);
		setUUID(uuid);
		setMajor(major);
		setMinor(minor);
	}

	@Override
	public String getMajor() {
		return getStringAttribute(ATTR_MAJOR);
	}

	@Override
	public String getMinor() {
		return getStringAttribute(ATTR_MINOR);
	}

	@Override
	public String getUUID() {
		return getStringAttribute(ATTR_UUID);
	}

	@Override
	public IMutableBeaconDevice setMajor(final String value) {
		putStringAttribute(ATTR_MAJOR, value);
		return this;
	}

	@Override
	public IMutableBeaconDevice setMinor(final String value) {
		putStringAttribute(ATTR_MINOR, value);
		return this;
	}

	@Override
	public IMutableBeaconDevice setUUID(final String value) {
		putStringAttribute(ATTR_UUID, value);
		return this;
	}

}
