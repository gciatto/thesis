package it.unibo.gciatto.thesis.android.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.impl.AbsolutePosition;

import java.util.Date;

import android.location.Location;

public class AndroidAbsolutePosition extends AbsolutePosition {

	/**
	 *
	 */
	private static final long serialVersionUID = -1985891063146873366L;

	private String mProvider = "unknown";

	public AndroidAbsolutePosition() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AndroidAbsolutePosition(final double latitude, final double longitude) {
		super(latitude, longitude);
		// TODO Auto-generated constructor stub
	}

	public AndroidAbsolutePosition(final double latitude, final double longitude, final double altitude) {
		super(latitude, longitude, altitude);
		// TODO Auto-generated constructor stub
	}

	public AndroidAbsolutePosition(final IAbsolutePosition toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public AndroidAbsolutePosition(final Location location) {
		this(location.getLatitude(), location.getLongitude(), location.getAltitude());
		setAccurancy(location.getAccuracy());
		setBearing(location.getBearing());
		setSpeed(location.getSpeed());
		mProvider = location.getProvider();
		// TODO fix
		setTimestamp(new Date(location.getTime()));
	}

	public Location toLocation() {
		final Location location = new Location(mProvider);
		location.setAccuracy((float) getAccurancy());
		location.setAltitude(getAltitude());
		location.setBearing((float) getBearing());
		location.setLatitude(getLatitude());
		location.setLongitude(getLongitude());
		location.setSpeed((float) getSpeed());
		location.setTime(getTimestamp().getTime());
		return location;

	}

}
