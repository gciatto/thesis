package it.unibo.gciatto.thesis.android.http.impl;

import java.net.URI;

import org.apache.http.client.methods.HttpPost;

public class HttpGetWithBody extends HttpPost {
	public static final String METHOD_NAME = "GET";

	public HttpGetWithBody() {

	}

	public HttpGetWithBody(final String uri) {
		super(uri);
	}

	public HttpGetWithBody(final URI uri) {
		super(uri);
	}

	@Override
	public String getMethod() {
		return METHOD_NAME;
	}

}
