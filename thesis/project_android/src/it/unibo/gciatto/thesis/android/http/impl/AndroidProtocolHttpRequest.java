package it.unibo.gciatto.thesis.android.http.impl;

import it.unibo.gciatto.thesis.android.http.IAndroidHttpRequest;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.http.impl.ProtocolHttpRequest;
import it.unibo.gciatto.thesis.log.L;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import com.loopj.android.http.RequestParams;

public class AndroidProtocolHttpRequest extends ProtocolHttpRequest implements IAndroidHttpRequest {

	public AndroidProtocolHttpRequest(final URI url, final Method method, final String what) {
		super(url, method, what);
	}

	public AndroidProtocolHttpRequest(final URI url, final Method method, final String what, final IEntityId id) {
		super(url, method, what, id);
	}

	public AndroidProtocolHttpRequest(final URI url, final Method method, final String what, final IEntityId id, final Object body) {
		super(url, method, what, id, body);
	}

	public HttpEntity getEntity() {
		try {
			return new StringEntity(getBody().toString());
		} catch (final UnsupportedEncodingException e) {
			return null;
		}
	}

	public String getQueryURL() {
		return HttpUtils.createQueryURL(getURI(), getParams());
	}

	public RequestParams getRequestParams() {
		final RequestParams params = new RequestParams();
		params.setAutoCloseInputStreams(true);
		for (final String key : getParams().getAttributeKeys()) {
			params.add(key, getParams().getAttribute(key).toString());
		}
		return params;
	}

	@Override
	public HttpRequestBase toAndroidHttpRequest() {
		final HttpRequestBase rq;
		String serialized;
		Writer bodyWriter;

		switch (getMethod()) {
			case POST:
				rq = new HttpPost();
				final HttpPost post = (HttpPost) rq;
				serialized = getBody().toString();
				bodyWriter = HttpUtils.getBodyWriter(post, serialized.length());
				try {
					bodyWriter.write(serialized);
					bodyWriter.close();
				} catch (final IOException e) {
					L.e(e);
				}
				break;
			case GET:
				if (getBody() != null) {
					rq = new HttpGetWithBody();
					final HttpGetWithBody get = (HttpGetWithBody) rq;
					serialized = getBody().toString();
					bodyWriter = HttpUtils.getBodyWriter(get, serialized.length());
					try {
						bodyWriter.write(serialized);
						bodyWriter.close();
					} catch (final IOException e) {
						L.e(e);
					}
				} else {
					rq = new HttpGet();
				}
				break;

			case DELETE:
				if (getBody() != null) {
					rq = new HttpGetWithBody();
					final HttpDeleteWithBody delete = (HttpDeleteWithBody) rq;
					serialized = getBody().toString();
					bodyWriter = HttpUtils.getBodyWriter(delete, serialized.length());
					try {
						bodyWriter.write(serialized);
						bodyWriter.close();
					} catch (final IOException e) {
						L.e(e);
					}
				} else {
					rq = new HttpDelete();
				}
				break;
			default:
				rq = new HttpRequestBase() {
					private final String METHOD_NAME = AndroidProtocolHttpRequest.this.getMethod().name();

					@Override
					public String getMethod() {
						return METHOD_NAME;
					}
				};
				break;
		}
		HttpUtils.setParams(rq, getURI(), getParams());
		return rq;
	}

}
