package it.unibo.gciatto.thesis.android.handler.impl;

import it.unibo.gciatto.thesis.android.position.impl.AndroidAbsolutePosition;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.handler.IAbsolutePositionHandler;
import it.unibo.gciatto.thesis.handler.IHandler;
import it.unibo.gciatto.thesis.knowledge.K;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.util.IAbsolutePositionListener;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class AbsolutePositionHandler extends BaseAndroidHandler implements IAbsolutePositionHandler {

	private IAbsolutePositionListener mPositionListener;
	private final LocationManager mLocationManger;
	private final LocationListener mLocationListener = new LocationListener() {

		@Override
		public void onLocationChanged(final Location location) {
			notifyAbsolutePosition(new AndroidAbsolutePosition(location));
		}

		@Override
		public void onProviderDisabled(final String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(final String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(final String provider, final int status, final Bundle extras) {
			// TODO Auto-generated method stub

		}
	};

	public AbsolutePositionHandler(final IContext context, final Context appContext) {
		super(context, appContext);

		setAbsolutePositionListener(context.getPositionManager());

		mLocationManger = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
	}

	@Override
	public IHandler init() {

		mLocationManger.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, K.timeouts.TIMEOUT_NW_MIN_PERIOD, K.numbers.NUM_NW_MIN_DISTANCE, mLocationListener);
		mLocationManger.requestLocationUpdates(LocationManager.GPS_PROVIDER, K.timeouts.TIMEOUT_GPS_MIN_PERIOD, K.numbers.NUM_GPS_MIN_DISTANCE, mLocationListener);

		return super.init();
	}

	protected void notifyAbsolutePosition(final IAbsolutePosition position) {
		if (mPositionListener != null) {
			mPositionListener.onAbsolutePosition(position, this);
		}
	}

	@Override
	public IAbsolutePositionHandler setAbsolutePositionListener(final IAbsolutePositionListener listener) {
		mPositionListener = listener;
		return this;
	}

}
