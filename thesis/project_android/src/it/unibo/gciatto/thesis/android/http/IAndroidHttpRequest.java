package it.unibo.gciatto.thesis.android.http;

import it.unibo.gciatto.http.IHttpRequest;

import org.apache.http.client.methods.HttpRequestBase;

public interface IAndroidHttpRequest extends IHttpRequest {
	HttpRequestBase toAndroidHttpRequest();
}
