package it.unibo.gciatto.thesis.android.entity.impl;

import it.unibo.gciatto.thesis.android.device.impl.SelfDevice;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.entity.id.impl.EntityId;
import it.unibo.gciatto.thesis.entity.impl.Entity;
import android.content.Context;
import android.os.Build;

public final class Self extends Entity {
	public static final String ATTR_USER = Self.class.getName() + "user";
	
	public static Self getSelf(final Context appContext) {
		if (INSTANCE == null) {
			INSTANCE = new Self(new EntityId(Build.SERIAL));
			INSTANCE.associateDevice(SelfDevice.getSelfDevice(appContext));
			INSTANCE.putAttribute(ATTR_USER, Build.USER);
		}
		return INSTANCE;
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -1941084100999181554L;

	private static Self INSTANCE;

	private Self(final IEntityId id) {
		super(id);
	}

}
