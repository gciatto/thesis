package it.unibo.gciatto.util.android;

public class KeyValuePair {

	private final String mKey;
	private final Object mValue;

	public KeyValuePair(final String key, final Object value) {
		super();
		mKey = key;
		mValue = value;
	}

	public String getKey() {
		return mKey;
	}

	public Object getValue() {
		return mValue;
	}

}
