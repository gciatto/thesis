package it.unibo.gciatto.util.android;

import it.unibo.gciatto.thesis.entity.id.IEntityId;

import java.util.Collection;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class EntityIdCollectionArrayAdapter extends CollectionArrayAdapter<IEntityId> {

	public EntityIdCollectionArrayAdapter(final Context context, final Collection<IEntityId> ids) {
		super(context, android.R.layout.simple_list_item_1, ids);
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View itemView;
		if (convertView == null) {
			itemView = View.inflate(getContext(), android.R.layout.simple_list_item_1, null);
		} else {
			itemView = convertView;
		}
		final IEntityId id = getItem(position);
		final TextView text1 = (TextView) itemView.findViewById(android.R.id.text1);
		text1.setText(id.getId());
		onIdViewGeneration(position, id, text1);
		return itemView;
	}
	
	public void onIdViewGeneration(int position, IEntityId id, final TextView textView) {
		/*
		 * To override
		 */
	}

}
