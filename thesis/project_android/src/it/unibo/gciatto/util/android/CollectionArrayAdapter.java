package it.unibo.gciatto.util.android;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public abstract class CollectionArrayAdapter<T> extends ArrayAdapter<T> {
	private Collection<T> mCollection;

	public CollectionArrayAdapter(final Context context, final int resource, final Collection<T> collection) {
		super(context, resource, new ArrayList<T>(collection));

		mCollection = collection;
	}

	@Override
	public void add(final T object) {
		mCollection.add(object);
		super.add(object);
	}

	@SuppressLint("NewApi")
	@Override
	public void addAll(final Collection<? extends T> collection) {
		mCollection.addAll(collection);
		super.addAll(collection);
	}

	@SuppressLint("NewApi")
	@Override
	public void addAll(final T... items) {
		mCollection.addAll(Arrays.asList(items));
		super.addAll(items);
	}

	@Override
	public void clear() {
		mCollection.clear();
		super.clear();
	}

	protected Collection<?> getItems() {
		return mCollection;
	}

	@Override
	public abstract View getView(int position, View convertView, ViewGroup parent);

	@Override
	public void insert(final T object, final int index) {
		mCollection.add(object);
		super.insert(object, index);
	}

	@Override
	public void notifyDataSetChanged() {
		setNotifyOnChange(false);
		super.clear();
		for (final T d : mCollection) {
			super.add(d);
		}
		setNotifyOnChange(true);
		super.notifyDataSetChanged();
	}

	@Override
	public void remove(final T object) {
		mCollection.remove(object);
		super.remove(object);
	}

	public void setItems(final Collection<T> items) {
		synchronized (items) {
			mCollection = items;
		}
		setNotifyOnChange(false);
		super.clear();
		for (final T e : mCollection) {
			super.add(e);
		}
		setNotifyOnChange(true);
		super.notifyDataSetChanged();
	}

}
