package it.unibo.gciatto.util.android;

import it.unibo.gciatto.thesis.android.R;
import it.unibo.gciatto.thesis.device.IDevice;

import java.util.Collection;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class DeviceCollectionArrayAdapter extends CollectionArrayAdapter<IDevice> {

	public DeviceCollectionArrayAdapter(final Context context, final Collection<IDevice> devices) {
		super(context, R.layout.device_layout, devices);
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View deviceLayout;
		final ListView devListView;
		if (convertView == null) {
			deviceLayout = View.inflate(getContext(), R.layout.device_layout, null);
		} else {
			deviceLayout = convertView;
		}
		devListView = (ListView) deviceLayout.findViewById(R.id.devListView);
		devListView.setAdapter(new AttributeSetArrayAdapter(getContext(), getItem(position)));
		return deviceLayout;
	}

}
