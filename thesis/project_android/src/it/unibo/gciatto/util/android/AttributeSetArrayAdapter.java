package it.unibo.gciatto.util.android;

import it.unibo.gciatto.thesis.android.R;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.impl.AttributeSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AttributeSetArrayAdapter extends ArrayAdapter<KeyValuePair> {

	private static List<KeyValuePair> attributesToPairList(final IAttributeSet attributes) {
		final List<KeyValuePair> list = new ArrayList<KeyValuePair>(attributes.countAttributes());
		for (final String key : attributes.getAttributeKeys()) {
			list.add(new KeyValuePair(key, attributes.getAttribute(key)));
		}
		return list;
	}

	private IAttributeSet mAttributes;

	public AttributeSetArrayAdapter(final Context context, final IAttributeSet attributes) {
		super(context, R.layout.key_value_layout, attributesToPairList(attributes));

		mAttributes = attributes;
	}

	@Override
	public void add(final KeyValuePair object) {
		mAttributes.putAttribute(object.getKey(), object.getValue());
		super.add(object);
	}

	@SuppressLint("NewApi")
	@Override
	public void addAll(final Collection<? extends KeyValuePair> collection) {
		for (final KeyValuePair p : collection) {
			mAttributes.putAttribute(p.getKey(), p.getValue());
		}
		super.addAll(collection);
	}

	@SuppressLint("NewApi")
	@Override
	public void addAll(final KeyValuePair... items) {
		for (final KeyValuePair p : items) {
			mAttributes.putAttribute(p.getKey(), p.getValue());
		}
		super.addAll(items);
	}

	@Override
	public void clear() {
		mAttributes.clearAllAttributes();
		super.clear();
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View keyValueLayout;
		if (convertView == null) {
			keyValueLayout = View.inflate(getContext(), R.layout.key_value_layout, null);
		} else {
			keyValueLayout = convertView;
		}

		final TextView keyTextView = (TextView) keyValueLayout.findViewById(R.id.keyTextView);
		final TextView valueTextView = (TextView) keyValueLayout.findViewById(R.id.valueTextView);

		final KeyValuePair pair = getItem(position);

		keyTextView.setText(AttributeSet.keyToString(pair.getKey()));
		valueTextView.setText(pair.getValue().toString());

		return keyValueLayout;
	}

	@Override
	public void insert(final KeyValuePair object, final int index) {
		mAttributes.putAttribute(object.getKey(), object.getValue());
		super.insert(object, index);
	}

	@Override
	public void notifyDataSetChanged() {
		setNotifyOnChange(false);
		super.clear();
		final List<KeyValuePair> elements = attributesToPairList(mAttributes);
		for (final KeyValuePair p : elements) {
			super.add(p);
		}
		setNotifyOnChange(true);
		super.notifyDataSetChanged();
	}

	@Override
	public void remove(final KeyValuePair object) {
		mAttributes.removeAttribute(object.getKey());
		super.remove(object);
	}

	public void setItems(final IAttributeSet attributes) {
		mAttributes = attributes;
		setNotifyOnChange(false);
		super.clear();
		for (final String key : attributes.getAttributeKeys()) {
			super.add(new KeyValuePair(key, attributes.getAttribute(key)));
		}
		setNotifyOnChange(true);
		super.notifyDataSetChanged();
	}

}
