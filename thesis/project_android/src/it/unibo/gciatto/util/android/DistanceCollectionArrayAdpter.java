package it.unibo.gciatto.util.android;

import it.unibo.gciatto.thesis.android.R;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;

import java.util.Collection;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DistanceCollectionArrayAdpter extends CollectionArrayAdapter<IEntityRelativeDistance> {

	public DistanceCollectionArrayAdpter(final Context context, final Collection<IEntityRelativeDistance> collection) {
		super(context, R.layout.relative_distance_layout, collection);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View relativeDistanceLayout;
		if (convertView == null) {
			relativeDistanceLayout = View.inflate(getContext(), R.layout.relative_distance_layout, null);
		} else {
			relativeDistanceLayout = convertView;
		}

		final TextView relTextView = (TextView) relativeDistanceLayout.findViewById(R.id.relTextView);
		final TextView distTextView = (TextView) relativeDistanceLayout.findViewById(R.id.distTextView);

		final IEntityRelativeDistance dist = getItem(position);

		relTextView.setText(dist.getRelativeToEntity().getId().getId());
		distTextView.setText(String.valueOf(dist.getDistance()));

		return relativeDistanceLayout;
	}

}
