package it.unibo.gciatto.thesis.entity.id.impl;

import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import java.net.URI;
import java.net.URISyntaxException;

import org.json2.JSONObject;

public class JsonEntityId extends EntityId implements IJsonObjectConvertible {

	private static URI getURIFromString(final String str) {
		try {
			return new URI(str);
		} catch (final URISyntaxException e) {
			return null;
		}
	}

	public static void main(final String[] args) {
		final EntityId id1 = new EntityId("gciatto");
		final EntityId id2 = new JsonEntityId(new JSONObject().put("uri", "entity:gciatto"));

		final boolean r1 = id1.isSameEntity(id2);
		final boolean r2 = id2.isSameEntity(id1);

		System.out.printf("%s %s \n", r1, r2);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = -4919294423029230390L;

	public JsonEntityId(final IEntityId toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public JsonEntityId(final JSONObject object) {
		super(getURIFromString(object.getString("uri")));
	}

	public JsonEntityId(final String id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public JsonEntityId(final URI uri) {
		super(uri);
		// TODO Auto-generated constructor stub
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		obj.put("uri", toString());
		return obj;
	}

}
