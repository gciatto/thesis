package it.unibo.gciatto.thesis.device.impl;

import it.unibo.gciatto.thesis.device.IDevice;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import org.json2.JSONObject;

public class JsonDevice extends Device implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = 3104036989992292336L;

	public JsonDevice(final IDevice toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public JsonDevice(final JSONObject source) {
		super("", "");
		if (source.has(ATTR_NAME) && source.has(ATTR_TYPE)) {
			final String[] fields = JSONObject.getNames(source);
			for (final String field : fields) {
				// TODO check
				putAttribute(field, source.get(field));
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	public JsonDevice(final String type, final String name) {
		super(type, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		for (final String key : getAttributeKeys()) {
			obj.put(key, getAttribute(key).toString());
		}
		return obj;
	}

}
