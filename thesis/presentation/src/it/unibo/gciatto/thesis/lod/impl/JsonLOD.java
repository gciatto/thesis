package it.unibo.gciatto.thesis.lod.impl;

import it.unibo.gciatto.util.IJsonObjectConvertible;

import org.json2.JSONObject;

public class JsonLOD extends LOD implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = 8949263905798795659L;

	public JsonLOD() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JsonLOD(final int value) {
		super(value);
		// TODO Auto-generated constructor stub
	}

	public JsonLOD(final JSONObject obj) {
		this(obj.getInt("lod"));
	}

	@Override
	public JSONObject toJsonObject() {
		return new JSONObject().put("lod", getValue());
	}
}
