package it.unibo.gciatto.thesis.http.impl;

import it.unibo.gciatto.http.impl.HttpRequest;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.IJsonArrayConvertible;
import it.unibo.gciatto.util.IJsonObjectConvertible;
import it.unibo.gciatto.util.impl.AttributeSet;

import java.net.URI;

import org.json2.JSONArray;
import org.json2.JSONObject;

public class ProtocolHttpRequest extends HttpRequest {

	private static IAttributeSet generateParams(final String what, final IEntityId id) {
		final IAttributeSet params = new AttributeSet();
		if (what != null) {
			params.putAttribute(Protocol.PARAM_WHAT, what);
		}
		if (id != null) {
			params.putAttribute(Protocol.PARAM_ENTITY_ID, id.toString());
		}
		return params;
	}

	public ProtocolHttpRequest(final URI url, final Method method, final String what) {
		this(url, method, what, null, null);
	}

	public ProtocolHttpRequest(final URI url, final Method method, final String what, final IEntityId id) {
		this(url, method, what, id, null);
	}

	public ProtocolHttpRequest(final URI url, final Method method, final String what, final IEntityId id, final Object body) {
		super(url, method, generateParams(what, id));
		if (body != null) {
			if (body instanceof IJsonArrayConvertible) {
				final IJsonArrayConvertible array = (IJsonArrayConvertible) body;
				setBody(array.toJsonArray());
			}
			if (body instanceof IJsonObjectConvertible) {
				final IJsonObjectConvertible array = (IJsonObjectConvertible) body;
				setBody(array.toJsonObject());
			}
		}
	}

	protected JSONArray getBodyAsJSONArray() {
		return (JSONArray) getBody();
	}

	protected JSONObject getBodyAsJSONObject() {
		return (JSONObject) getBody();
	}

}
