package it.unibo.gciatto.thesis.http.impl;

public final class Protocol {
	public static class what {
		public static final String VALUE_DATA = "Data";
		public static final String VALUE_DISTANCE = "Distance";
		public static final String VALUE_POSITION = "Position";
		public static final String VALUE_DISTANCES = "Distances";
		public static final String VALUE_POSITIONS = "Positions";
		public static final String VALUE_DEVICES = "Devices";
		public static final String VALUE_DEVICE = "Device";
		public static final String VALUE_EVERYTHING = "Everything";
		public static final String VALUE_IDS = "Ids";
		public static final String VALUE_ID = "Id";
	}

	public static final String PARAM_WHAT = "What";

	public static final String PARAM_ENTITY_ID = "Entity";
}
