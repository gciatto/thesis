package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import java.util.Date;

import org.json2.JSONObject;

public class JsonAbsolutePosition extends AbsolutePosition implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = -9028769592873878382L;
	private static final String[] FIELDS = new String[] { "accurancy", "altitude", "bearing", "latitude", "longitude", "speed", "timestamp" };

	public JsonAbsolutePosition() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JsonAbsolutePosition(final double latitude, final double longitude) {
		super(latitude, longitude);
		// TODO Auto-generated constructor stub
	}

	public JsonAbsolutePosition(final double latitude, final double longitude, final double altitude) {
		super(latitude, longitude, altitude);
		// TODO Auto-generated constructor stub
	}

	public JsonAbsolutePosition(final IAbsolutePosition toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public JsonAbsolutePosition(final JSONObject object) {
		super();

		setAccurancy(object.optDouble(FIELDS[0]));
		setAltitude(object.optDouble(FIELDS[1]));
		setBearing(object.optDouble(FIELDS[2]));
		setLatitude(object.getDouble(FIELDS[3]));
		setLongitude(object.getDouble(FIELDS[4]));
		setSpeed(object.optDouble(FIELDS[5]));
		setTimestamp(new Date(object.getLong(FIELDS[6])));
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		if (isAccurancySet()) {
			obj.put(FIELDS[0], getAccurancy());
		}
		if (isAltitudeSet()) {
			obj.put(FIELDS[1], getAltitude());
		}
		if (isBearingSet()) {
			obj.put(FIELDS[2], getBearing());
		}
		obj.put(FIELDS[3], getLatitude());
		obj.put(FIELDS[4], getLongitude());
		if (isSpeedSet()) {
			obj.put(FIELDS[5], getSpeed());
		}
		obj.put(FIELDS[6], getTimestamp().getTime());
		return obj;
	}
}
