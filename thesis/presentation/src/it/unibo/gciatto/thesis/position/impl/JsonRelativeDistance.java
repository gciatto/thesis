package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.position.IRelativeDistance;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import org.json2.JSONObject;

public class JsonRelativeDistance extends RelativeDistance implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = -2423394196760065090L;

	public JsonRelativeDistance(final IAbsolutePosition relativeTo) {
		super(relativeTo);
		// TODO Auto-generated constructor stub
	}

	public JsonRelativeDistance(final IAbsolutePosition relativeTo, final double distance) {
		super(relativeTo, distance);
		// TODO Auto-generated constructor stub
	}

	public JsonRelativeDistance(final IRelativeDistance toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public JsonRelativeDistance(final JSONObject obj) {
		this(new JsonAbsolutePosition(obj.getJSONObject("relativeTo")), obj.getDouble("distance"));
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		obj.put("relativeTo", new JsonAbsolutePosition(getRelativeTo()).toJsonObject()).put("distance", getDistance());
		return obj;
	}

}
