package it.unibo.gciatto.thesis.position.impl;

import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import org.json2.JSONObject;

public class JsonRelativePosition extends RelativePosition implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = -8640777030746928230L;

	public JsonRelativePosition(final IAbsolutePosition relativeTo) {
		super(relativeTo);
		// TODO Auto-generated constructor stub
	}

	public JsonRelativePosition(final IAbsolutePosition relativeTo, final double deltaLatitude, final double deltaLongitude) {
		super(relativeTo, deltaLatitude, deltaLongitude);
		// TODO Auto-generated constructor stub
	}

	public JsonRelativePosition(final IAbsolutePosition source, final IAbsolutePosition destination) {
		super(source, destination);
		// TODO Auto-generated constructor stub
	}

	public JsonRelativePosition(final JSONObject obj) {
		this(new JsonAbsolutePosition(obj.getJSONObject("relativeTo")), obj.getDouble("deltaLat"), obj.getDouble("deltaLong"));
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		obj.put("relativeTo", new JsonAbsolutePosition(getRelativeTo()).toJsonObject()).put("deltaLat", getLatitudeDelta()).put("deltaLong", getLongitudeDelta());
		return obj;

	}

}
