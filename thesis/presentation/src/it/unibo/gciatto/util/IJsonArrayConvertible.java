package it.unibo.gciatto.util;

import org.json2.JSONArray;

public interface IJsonArrayConvertible {
	JSONArray toJsonArray();
}
