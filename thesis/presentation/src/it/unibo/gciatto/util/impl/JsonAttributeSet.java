package it.unibo.gciatto.util.impl;

import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.IJsonObjectConvertible;

import org.json2.JSONObject;

public class JsonAttributeSet extends AttributeSet implements IJsonObjectConvertible {

	/**
	 *
	 */
	private static final long serialVersionUID = -8373632173507734696L;

	public JsonAttributeSet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JsonAttributeSet(final IAttributeSet toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public JsonAttributeSet(final JSONObject source) {
		final String[] fields = JSONObject.getNames(source);
		if (fields == null) {
			return;
		}
		for (final String field : fields) {
			// TODO check
			putAttribute(field, source.get(field));
		}
	}

	public JsonAttributeSet(final String firstKey, final Object firstValue) {
		super(firstKey, firstValue);
		// TODO Auto-generated constructor stub
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject obj = new JSONObject();
		for (final String key : getAttributeKeys()) {
			obj.put(key, getAttribute(key).toString());
		}
		return obj;
	}

}
