package it.unibo.gciatto.util;

import org.json2.JSONObject;

public interface IJsonObjectConvertible {
	JSONObject toJsonObject();
}
