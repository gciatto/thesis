package it.unibo.gciatto.http;

import java.io.Reader;

public interface IHttpResponse {
	Object getBody();

	Reader getBodyReader();

	int getStatusCode();

	String getStatusLine();

	boolean isOK();
}
