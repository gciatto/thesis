package it.unibo.gciatto.http.impl;

import it.unibo.gciatto.http.IHttpRequest;
import it.unibo.gciatto.util.IAttributeSet;
import it.unibo.gciatto.util.impl.AttributeSet;

import java.net.URI;

public class HttpRequest implements IHttpRequest {

	private URI mURI;
	private Method mMethod = Method.GET;
	private IAttributeSet mParams = new AttributeSet();
	private Object mBody = null;

	public HttpRequest() {
		this(null);
	}

	public HttpRequest(final URI URI) {
		setURI(URI);
	}

	public HttpRequest(final URI URI, final Method method) {
		this(URI);
		setMethod(method);
	}

	public HttpRequest(final URI URI, final Method method, final IAttributeSet params) {
		this(URI);
		setMethod(method);
		mParams.putAllAttributes(params);
	}

	@Override
	public Object getBody() {
		return mBody;
	}

	@Override
	public Method getMethod() {
		return mMethod;
	}

	@Override
	public IAttributeSet getParams() {
		return mParams;
	}

	@Override
	public URI getURI() {
		return mURI;
	}

	@Override
	public IHttpRequest setBody(final Object obj) {
		mBody = obj;
		return this;
	}

	@Override
	public IHttpRequest setMethod(final Method value) {
		mMethod = value;
		return this;
	}

	@Override
	public IHttpRequest setParams(final IAttributeSet value) {
		mParams = value;
		return this;
	}

	@Override
	public IHttpRequest setURI(final URI value) {
		mURI = value;
		return this;
	}

	@Override
	public String toString() {
		return String.format("%s [Method=%s, URI=%s, Params=%s, Body=%s]", getClass().getSimpleName(), mMethod, mURI, mParams, mBody);
	}

}
