package it.unibo.gciatto.http;

import it.unibo.gciatto.util.IAttributeSet;

import java.net.URI;

public interface IHttpRequest {
	public static enum Method {
		GET, POST, HEAD, PUT, DELETE, TRACE, OPTIONS, CONNETCT;
	}

	Object getBody();

	Method getMethod();

	IAttributeSet getParams();

	URI getURI();

	IHttpRequest setBody(Object value);

	IHttpRequest setMethod(Method value);

	IHttpRequest setParams(IAttributeSet value);

	IHttpRequest setURI(URI value);
}
