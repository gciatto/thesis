package it.unibo.gciatto.thesis.android.observer.position.impl;

import it.unibo.gciatto.thesis.android.position.impl.AndroidAbsolutePosition;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;

import java.util.Date;

import android.location.Location;

import com.metaio.sdk.jni.LLACoordinate;

public class MetaioAbsolutePosition extends AndroidAbsolutePosition {
	/**
	 *
	 */
	private static final long serialVersionUID = -8546798751658797437L;

	public MetaioAbsolutePosition() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MetaioAbsolutePosition(final double latitude, final double longitude) {
		super(latitude, longitude);
		// TODO Auto-generated constructor stub
	}

	public MetaioAbsolutePosition(final double latitude, final double longitude, final double altitude) {
		super(latitude, longitude, altitude);
		// TODO Auto-generated constructor stub
	}

	public MetaioAbsolutePosition(final IAbsolutePosition toClone) {
		super(toClone);
		// TODO Auto-generated constructor stub
	}

	public MetaioAbsolutePosition(final LLACoordinate coordinate) {
		super(coordinate.getLatitude(), coordinate.getLongitude(), coordinate.getAltitude());
		setAccurancy(coordinate.getAccuracy());
		setTimestamp(new Date(1000l * (long) coordinate.getTimestamp()));
	}

	public MetaioAbsolutePosition(final Location location) {
		super(location);
		// TODO Auto-generated constructor stub
	}

	public LLACoordinate toLLACoordinate() {
		final LLACoordinate coord = new LLACoordinate(getLatitude(), getLongitude(), getAltitude(), getAccurancy());
		return coord;
	}

}
