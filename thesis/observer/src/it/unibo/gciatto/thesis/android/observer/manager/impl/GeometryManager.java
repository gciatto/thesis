package it.unibo.gciatto.thesis.android.observer.manager.impl;

import it.unibo.gciatto.thesis.android.observer.manager.IGeometryManager;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.thesis.manager.impl.BaseManager;
import it.unibo.gciatto.util.func.IConsumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.metaio.sdk.jni.IGeometry;

public class GeometryManager extends BaseManager implements IGeometryManager {

	private final Map<IEntityId, IGeometry> mGeometries;

	public GeometryManager(final IContext parent) {
		super(parent);
		mGeometries = new HashMap<IEntityId, IGeometry>();
	}

	@Override
	public synchronized IManager asyncASAPSynchronize() {
		return this;
	}

	@Override
	public synchronized IManager bind(final IEntity entity, final IGeometry geometry) {
		bind(entity);
		setGeometry(entity, geometry);
		return this;
	}

	@Override
	public synchronized IGeometryManager forEachGeometry(final IConsumer<IGeometry> consumer) {
		int i = 0;
		for (final IEntityId id : mGeometries.keySet()) {
			consumer.consume(mGeometries.get(id), i++);
		}
		return this;
	}

	@Override
	public synchronized List<IGeometry> getGeometries() {
		return new ArrayList<IGeometry>(mGeometries.values());
	}

	@Override
	public synchronized IGeometry getGeometry(final IEntity entity) {
		return getGeometry(entity.getId());
	}

	@Override
	public synchronized IGeometry getGeometry(final IEntityId id) {
		return mGeometries.get(id);
	}

	@Override
	public synchronized IGeometryManager setGeometry(final IEntity entity, final IGeometry value) {
		return setGeometry(entity.getId(), value);
	}

	@Override
	public synchronized IGeometryManager setGeometry(final IEntityId id, final IGeometry value) {
		if (isBound(id)) {
			mGeometries.put(id, value);
		}
		return this;
	}

	@Override
	public synchronized IManager unbind(final IEntity entity) {
		super.unbind(entity);
		mGeometries.remove(entity);
		return this;
	}

	@Override
	protected Object[] getArgs(IEntity entity) {
		return new Object[] {
				getGeometry(entity)
		};
	}

	
	

}
