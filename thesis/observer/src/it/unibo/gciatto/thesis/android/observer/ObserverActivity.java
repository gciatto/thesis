// Copyright 2007-2014 metaio GmbH. All rights reserved.
package it.unibo.gciatto.thesis.android.observer;

import it.unibo.gciatto.thesis.android.context.impl.SystemContext;
import it.unibo.gciatto.thesis.android.observer.context.IARContext;
import it.unibo.gciatto.thesis.android.observer.context.impl.ARContextDecorator;
import it.unibo.gciatto.thesis.android.observer.manager.IGeometryManager;
import it.unibo.gciatto.thesis.android.observer.position.impl.MetaioAbsolutePosition;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.landmark.impl.Landmark;
import it.unibo.gciatto.thesis.entity.landmark.impl.LandmarkUtils;
import it.unibo.gciatto.thesis.entity.listener.IEntityListener;
import it.unibo.gciatto.thesis.entity.listener.impl.EntityListener;
import it.unibo.gciatto.thesis.entity.position.IEntityRelativeDistance;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.knowledge.K;
import it.unibo.gciatto.thesis.lod.ILevelOfDetail;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;
import it.unibo.gciatto.thesis.position.IAbsolutePosition;
import it.unibo.gciatto.thesis.util.IEntityDetectedListener;
import it.unibo.gciatto.thesis.util.IManagerListener;
import it.unibo.gciatto.util.func.IBiConsumer;
import it.unibo.gciatto.util.func.IConsumer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.metaio.sdk.ARViewActivity;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.SensorsComponentAndroid;
import com.metaio.sdk.jni.AnnotatedGeometriesGroupCallback;
import com.metaio.sdk.jni.EGEOMETRY_FOCUS_STATE;
import com.metaio.sdk.jni.IAnnotatedGeometriesGroup;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.IRadar;
import com.metaio.sdk.jni.ISensorsComponent;
import com.metaio.sdk.jni.LLACoordinate;
import com.metaio.sdk.jni.Rotation;
import com.metaio.sdk.jni.SensorValues;
import com.metaio.sdk.jni.TrackingValues;
import com.metaio.sdk.jni.TrackingValuesVector;
import com.metaio.sdk.jni.Vector3d;
import com.metaio.tools.SystemInfo;
import com.metaio.tools.io.AssetsManager;

public class ObserverActivity extends ARViewActivity implements IARContext, IEntityDetectedListener {

	protected final class MetaioSDKCallbackHandler extends IMetaioSDKCallback {

		@Override
		public void onTrackingEvent(final TrackingValuesVector trackingValues) {
			for (int i = 0; i < trackingValues.size(); i++) {
				final TrackingValues v = trackingValues.get(i);
				if (v.isTrackingState()) {
					// reading the code
					final String[] tokens = v.getAdditionalValues().split("::");
					if (tokens.length > 1) {
						getEnvironmentManager().notifyPatternDetected(tokens[1]);
					}
				}

			}
		}

	}

	protected final class MyAnnotatedGeometriesGroupCallback extends AnnotatedGeometriesGroupCallback {

		@Override
		public IGeometry loadUpdatedAnnotation(final IGeometry geometry, final Object userData, final IGeometry existingAnnotation) {
			if (userData == null) {
				return null;
			}

			if (existingAnnotation != null) {
				// We don't update the annotation if e.g. distance has changed
				return existingAnnotation;
			}

			final String title = (String) userData; // as passed to addGeometry
			final String texturePath = getAnnotationImageForTitle(title);

			return metaioSDK.createGeometryFromImage(texturePath, true, false);
		}

		@Override
		public void onFocusStateChanged(final IGeometry geometry, final Object userData, final EGEOMETRY_FOCUS_STATE oldState, final EGEOMETRY_FOCUS_STATE newState) {
			MetaioDebug.log("onFocusStateChanged for " + (String) userData + ", " + oldState + "->" + newState);
		}
	}

	protected final class ScanTask extends AsyncTask<Integer, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(final Integer... params) {
			final boolean result1 = metaioSDK.setTrackingConfiguration("QRCODE", false);
			final int max;
			final int time;

			if (params.length == 0) {
				max = K.timeouts.TIMEOUT_USER_SCAN_DURATION;
				time = 1000; //1s
			} else if (params.length == 1) {
				time = 1000; //1s
				max = params[0];
			} else {
				max = params[0];
				time = params[1];
			}
			
			for (int i = 0; i < max; i++) {
				try {
					Thread.sleep(time);
					publishProgress(i + 1, max, time);
				} catch (final InterruptedException ex) {
					Log.e(TAG, ex.getMessage());
				}
			}
			final boolean result2 = metaioSDK.setTrackingConfiguration("GPS", false);
			return result1 && result2;
		}

		@Override
		protected void onPostExecute(final Boolean result) {
			synchronized (mScanning) {
				mScanning = false;
			}
			Toast.makeText(getApplicationContext(), "Ended scanning", Toast.LENGTH_SHORT).show();
			showInfo("");
		}

		@Override
		protected void onPreExecute() {
			Toast.makeText(getApplicationContext(), "Start scanning...", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onProgressUpdate(final Integer... values) {
			// Toast.makeText(getApplicationContext(),
			// String.format("Scanning %s s / %s s", values[0], values[1]),
			// values[2]).show();
			showInfo("Scanning %s s / %s s", values[0], values[1]);
		}

	}

	private static final String TAG = "ObserverActivity";

	private IAnnotatedGeometriesGroup mAnnotatedGeometriesGroup;
	private MyAnnotatedGeometriesGroupCallback mAnnotatedGeometriesGroupCallback;
	private IARContext mContext;
	private IRadar mRadar;
	private MetaioSDKCallbackHandler mCallbackHandler;
	private AlertDialog mAlert;
	private TextView mInfoTextView, mPositionTextView;
	private Boolean mScanning = false;
	private Button mScanButton;
	private ToggleButton mLandmarksToggle;
	private boolean mShowLandmarks;
	private final IEntityListener mSelfListener = new EntityListener() {
		@Override
		public void onCanSee(final IEntity source, final IEntityRelativeDistance distance) {
			showInfo(true, "Can see %s", distance.getRelativeToEntity().getId().getId());
		}

		@Override
		public void onCantSeeAnymore(final IEntity source, final IEntity other) {
			showInfo(true, "Can't see %s anymore", other.getId().getId());

		}

		@Override
		public void onGettingCloser(final IEntity source, final IEntityRelativeDistance other) {
			final DecimalFormat format = new DecimalFormat("##.##");
			showInfo("Getting closer to %s: %s", other.getRelativeToEntity().getId().getId(), format.format(other.getDistance()));
			if (other.isRegion() || other.getDistance() < K.numbers.NUM_MIN_SCAN_DISTANCE) {
				scanIfNotScanning(K.timeouts.TIMEOUT_AUTO_SCAN_DURATION);
			}
		}

		@Override
		public void onGettingFarther(final IEntity source, final IEntityRelativeDistance other) {
			final DecimalFormat format = new DecimalFormat("##.##");
			showInfo("Getting farther to %s: %s", other.getRelativeToEntity().getId().getId(), format.format(other.getDistance()));
		}

		@Override
		public void onAbsolutePositionFix(IEntity source, IAbsolutePosition newPosition, IAbsolutePosition oldPosition) {
			final MetaioAbsolutePosition pos = new MetaioAbsolutePosition(newPosition);
			try {
				mSensors.setManualLocation(pos.toLLACoordinate());
			} catch (NullPointerException ex) {
				Log.i(TAG, "", ex);
				//mSensors.resetManualLocation();
			}
		}

		@Override
		public void onLODChanged(IEntity source, final ILevelOfDetail newLOD, ILevelOfDetail oldLOD) {
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (newLOD.isBTLocalizationConvenient()) {
						mPositionTextView.setText("BLE");
					} else if (newLOD.isGPSLocalizationConvenient()) {
						mPositionTextView.setText("GPS");
					} else {
						mPositionTextView.setText("???");
					}
				}
			});
			if (!newLOD.isBTLocalizationConvenient() && newLOD.isGPSLocalizationConvenient()) {
				mSensors.resetManualLocation();
			}
		}
		
	};
	private final IEntityListener mEntityListener = new EntityListener() {

		@Override
		public void onAbsolutePositionFix(final IEntity source, final IAbsolutePosition newPosition, final IAbsolutePosition oldPosition) {
			final IGeometry geo = getGeometryManager().getGeometry(source);
			if (geo != null) {
				runOnARThread(new Runnable() {

					@Override
					public void run() {
						geo.setTranslationLLA(new MetaioAbsolutePosition(newPosition).toLLACoordinate());
					}
				});

			}
		}

		@Override
		public void onLODChanged(final IEntity source, final ILevelOfDetail newLOD, final ILevelOfDetail oldLOD) {
			if (newLOD.isInvisible()) {
				final IGeometry geo = getGeometryManager().getGeometry(source);
				if (geo != null) {
					runOnARThread(new Runnable() {

						@Override
						public void run() {
							geo.setVisible(false);
						}
					});

				}
			} else {
				final IGeometry geo = getGeometryManager().getGeometry(source);
				if (geo != null) {
					runOnARThread(new Runnable() {

						@Override
						public void run() {
							geo.setVisible(true);
						}
					});

				}
			}
		}

	};

	protected void runOnARThread(Runnable action) {
		if (mSurfaceView != null) {
			mSurfaceView.queueEvent(action);
		}
	}
	
	@Override
	public IContext bindToAllManagers(final IEntity entity) {
		return mContext.bindToAllManagers(entity);
	}

	private IGeometry createPOIGeometry(final LLACoordinate lla) {
		final String path = AssetsManager.getAssetPath(getApplicationContext(), "TutorialLocationBasedAR/Assets/ExamplePOI.obj");
		if (path != null) {
			final IGeometry geo = metaioSDK.createGeometry(path);
			geo.setTranslationLLA(lla);
			geo.setLLALimitsEnabled(true);
			geo.setScale(50);
			return geo;
		} else {
			MetaioDebug.log(Log.ERROR, "Missing files for POI geometry");
			return null;
		}
	}

	private String getAnnotationImageForTitle(final String title) {
		Bitmap billboard = null;

		try {
			final String texturepath = getCacheDir() + "/" + title + ".png";
			final Paint mPaint = new Paint();

			// Load background image and make a mutable copy

			final float dpi = SystemInfo.getDisplayDensity(getApplicationContext());
			final int scale = dpi > 240 ? 2 : 1;
			final String filepath = AssetsManager.getAssetPath(getApplicationContext(), "TutorialLocationBasedAR/Assets/POI_bg" + (scale == 2 ? "@2x" : "") + ".png");
			final Bitmap mBackgroundImage = BitmapFactory.decodeFile(filepath);

			billboard = mBackgroundImage.copy(Bitmap.Config.ARGB_8888, true);

			final Canvas c = new Canvas(billboard);

			mPaint.setColor(Color.WHITE);
			mPaint.setTextSize(24);
			mPaint.setTypeface(Typeface.DEFAULT);
			mPaint.setTextAlign(Paint.Align.CENTER);

			float y = 40 * scale;
			final float x = 30 * scale;

			// Draw POI name
			if (title.length() > 0) {
				String n = title.trim();

				final int maxWidth = 160 * scale;

				int i = mPaint.breakText(n, true, maxWidth, null);

				final int xPos = c.getWidth() / 2;
				final int yPos = (int) (c.getHeight() / 2 - (mPaint.descent() + mPaint.ascent()) / 2);
				c.drawText(n.substring(0, i), xPos, yPos, mPaint);

				// Draw second line if valid
				if (i < n.length()) {
					n = n.substring(i);
					y += 20 * scale;
					i = mPaint.breakText(n, true, maxWidth, null);

					if (i < n.length()) {
						i = mPaint.breakText(n, true, maxWidth - 20 * scale, null);
						c.drawText(n.substring(0, i) + "...", x, y, mPaint);
					} else {
						c.drawText(n.substring(0, i), x, y, mPaint);
					}
				}
			}

			// Write texture file
			try {
				final FileOutputStream out = new FileOutputStream(texturepath);
				billboard.compress(Bitmap.CompressFormat.PNG, 90, out);
				MetaioDebug.log("Texture file is saved to " + texturepath);
				return texturepath;
			} catch (final Exception e) {
				MetaioDebug.log("Failed to save texture file");
				e.printStackTrace();
			}
		} catch (final Exception e) {
			MetaioDebug.log("Error creating annotation texture: " + e.getMessage());
			MetaioDebug.printStackTrace(Log.DEBUG, e);
			return null;
		} finally {
			if (billboard != null) {
				billboard.recycle();
				billboard = null;
			}
		}

		return null;
	}

	@Override
	public IDataServerHandler getDataServerHandler() {
		return mContext.getDataServerHandler();
	}

	@Override
	public IGeometryManager getGeometryManager() {
		return mContext.getGeometryManager();
	}

	@Override
	protected int getGUILayout() {
		return R.layout.tutorial_location_based_ar;
	}

	@Override
	public IIdentityManager getIdentityManager() {
		return mContext.getIdentityManager();
	}

	@Override
	public IInformationManager getInformationManager() {
		return mContext.getInformationManager();
	}

	@Override
	protected IMetaioSDKCallback getMetaioSDKCallbackHandler() {
		return mCallbackHandler;
	}

	@Override
	public IPositionManager getPositionManager() {
		return mContext.getPositionManager();
	}

	@Override
	public IEntity getSelf() {
		return mContext.getSelf();
	}

	@Override
	protected void loadContents() {
		mAnnotatedGeometriesGroup = metaioSDK.createAnnotatedGeometriesGroup();
		mAnnotatedGeometriesGroupCallback = new MyAnnotatedGeometriesGroupCallback();
		mAnnotatedGeometriesGroup.registerCallback(mAnnotatedGeometriesGroupCallback);

		// Clamp geometries' Z position to range [5000;200000] no matter how
		// close or far they are away.
		// This influences minimum and maximum scaling of the geometries (easier
		// for development).
		metaioSDK.setLLAObjectRenderingLimits(5, 200);

		// Set render frustum accordingly
		metaioSDK.setRendererClippingPlaneLimits(10, 220000);

		// create radar
		mRadar = metaioSDK.createRadar();
		final String radarBgPath = AssetsManager.getAssetPath(getApplicationContext(), "TutorialLocationBasedAR/Assets/radar.png");
		final String radarObjPath = AssetsManager.getAssetPath(getApplicationContext(), "TutorialLocationBasedAR/Assets/yellow.png");
		mRadar.setBackgroundTexture(radarBgPath);
		mRadar.setObjectsDefaultTexture(radarObjPath);
		mRadar.setRelativeToScreen(IGeometry.ANCHOR_TL);

		getIdentityManager().forEachEntity(new IConsumer<IEntity>() {

			@Override
			public void consume(final IEntity obj, final int index) {
				if (!obj.getId().isSameEntity(getSelf().getId()) && !obj.getId().getId().equalsIgnoreCase("testentity")) {
					getGeometryManager().bind(obj);
				}
			}
		});

	}

	public void onButtonClick(final View v) {
		finish();
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			AssetsManager.extractAllAssets(this, true);
		} catch (final IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

		// Set GPS tracking configuration
		final boolean result = metaioSDK.setTrackingConfiguration("GPS", false);
		MetaioDebug.log("Tracking data loaded: " + result);

		final IContext sysContext = SystemContext.getInstance(this);
		mContext = new ARContextDecorator(sysContext);
		SystemContext.setInstance(mContext);

		getSelf().addEntityListener(mSelfListener);

		mCallbackHandler = new MetaioSDKCallbackHandler();
		mAlert = null;

		getGeometryManager().addManagerListener(new IManagerListener() {

			@Override
			public void onEntityBound(final IEntity entity, final Object... args) {
				if (entity.getId().getId().equalsIgnoreCase("testentity")) {
					return;
				}
				entity.addEntityListener(mEntityListener);
				final IGeometry geo = createPOIGeometry(new MetaioAbsolutePosition(entity.getAbsolutePosition()).toLLACoordinate());
				if (geo == null) {
					Log.w(TAG, "Null geo");
				}
				getGeometryManager().setGeometry(entity, geo);
				if (entity.isSameEntity(getSelf())) {
					return;
				}
				runOnARThread(new Runnable() {

					@Override
					public void run() {
						mAnnotatedGeometriesGroup.addGeometry(geo, entity.getId().getId());
						mRadar.add(geo);
					}
				});
			}

			@Override
			public void onEntityUnbound(final IEntity entity, final Object... args) {
				entity.removeEntityListener(mEntityListener);
				if (entity.isSameEntity(getSelf())) {
					entity.removeEntityListener(mSelfListener);
				}
				if (args.length > 0 && mSurfaceView != null) {
					final IGeometry geo = (IGeometry) args[0];
					runOnARThread(new Runnable() {

						@Override
						public void run() {
							if (mAnnotatedGeometriesGroup != null) {
								mAnnotatedGeometriesGroup.removeGeometry(geo);
							}
							if (mRadar != null) {
								mRadar.remove(geo);
							}
						}
					});
				}
			}
		});
	
		getEnvironmentManager().setEntityDetectedListener(this);
	}

	@Override
	protected void onDestroy() {
		// Break circular reference of Java objects
		if (mAnnotatedGeometriesGroup != null) {
			mAnnotatedGeometriesGroup.registerCallback(null);
		}

		if (mAnnotatedGeometriesGroupCallback != null) {
			mAnnotatedGeometriesGroupCallback.delete();
			mAnnotatedGeometriesGroupCallback = null;
		}

		if (mCallbackHandler != null) {
			mCallbackHandler.delete();
			mCallbackHandler = null;
		}
		final List<IEntity> entities = new ArrayList<IEntity>(getGeometryManager().getBoundEntities());
		for (final IEntity e : entities) {
			getGeometryManager().unbind(e);
			
		}
		super.onDestroy();
	}

	@Override
	public void onDrawFrame() {
		if (metaioSDK != null && mSensors != null) {
			final SensorValues sensorValues = mSensors.getSensorValues();

			float heading = 0.0f;
			if (sensorValues.hasAttitude()) {
				final float m[] = new float[9];
				sensorValues.getAttitude().getRotationMatrix(m);

				Vector3d v = new Vector3d(m[6], m[7], m[8]);
				v = v.normalize();

				heading = (float) (-Math.atan2(v.getY(), v.getX()) - Math.PI / 2.0);
			}

			final List<IGeometry> geos = getGeometryManager().getGeometries();
			final Rotation rot = new Rotation((float) (Math.PI / 2.0), 0.0f, -heading);
			for (final IGeometry geo : geos) {
				if (geo != null) {
					geo.setRotation(rot);
				}
			}
		}

		super.onDrawFrame();
	}

	@Override
	protected void onGeometryTouched(final IGeometry geometry) {
		MetaioDebug.log("Geometry selected: " + geometry);

		runOnARThread(new Runnable() {

			@Override
			public void run() {
				mRadar.setObjectsDefaultTexture(AssetsManager.getAssetPath(getApplicationContext(), "TutorialLocationBasedAR/Assets/yellow.png"));
				mRadar.setObjectTexture(geometry, AssetsManager.getAssetPath(getApplicationContext(), "TutorialLocationBasedAR/Assets/red.png"));
				mAnnotatedGeometriesGroup.setSelectedGeometry(geometry);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		mInfoTextView = (TextView) findViewById(R.id.infoTextView);
		mScanButton = (Button) findViewById(R.id.scanButton);
		mPositionTextView = (TextView) findViewById(R.id.positionTextView);
		mLandmarksToggle = (ToggleButton) findViewById(R.id.landmarksToggleButton);

		showInfo("");
		
		mLandmarksToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				setShowLandmarks(isChecked);
			}
		});

		mScanButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				scanIfNotScanning();
			}
		});

		final TextPaint paint = mInfoTextView.getPaint();
		paint.setColor(Color.BLACK);
		paint.setFakeBoldText(false);
		paint.setShadowLayer(2, 0, 0, Color.WHITE);
	}
	
	protected void scanIfNotScanning(Integer... args) {
		synchronized (mScanning) {
			if (mScanning) {
				return;
			} else {
				mScanning = true;
			}
		}
		final ScanTask task = new ScanTask();
		task.execute(args);
	}

	protected void showInfo(final boolean toast, final String format, final Object... args) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (toast) {
					Toast.makeText(getApplicationContext(), String.format(format, args), Toast.LENGTH_SHORT).show();
				} else {
					mInfoTextView.setText(String.format(format, args));
				}

			}
		});
	}

	protected void showInfo(final String format, final Object... args) {
		showInfo(false, format, args);
	}

	@Override
	public IContext unbindFromAllManagers(final IEntity entity) {
		return mContext.unbindFromAllManagers(entity);
	}

	@Override
	public IEnvironmentManger getEnvironmentManager() {
		return mContext.getEnvironmentManager();
	}

	@Override
	public void onEntityDetected(final IEntity entity, Object... args) {
		
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				final StringBuilder sb = new StringBuilder();
				final Object msg = entity.getAttribute(IEntity.ATTR_MESSAGE);
				if (msg != null) {
					sb.append(msg.toString());
				} else {
					entity.forEachAttribute(new IBiConsumer<String, Object>() {
						
						@Override
						public void consume(String key, Object value, int i) {
							if (i > 0) {
								sb.append("\n");
							}
							sb.append(key);
							sb.append(":\n");
							sb.append(value);
						}
				});
				}
				if (mAlert == null) {
					mAlert = new AlertDialog.Builder(ObserverActivity.this)
					.setTitle(entity.getId().getId())
					.setMessage(sb.toString())
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(final DialogInterface dialog, final int which) {
							dialog.cancel();
						}
					})
					.create();
				}
				if (!mAlert.isShowing()) {
					mAlert.setTitle(entity.getId().getId());
					mAlert.setMessage(sb.toString());
					mAlert.show();
				}
			}
		});
	}

	public boolean isShowLandmarks() {
		return mShowLandmarks;
	}

	public void setShowLandmarks(final boolean showLandmarks) {
		mShowLandmarks = showLandmarks;
		getGeometryManager().forEachEntity(new IConsumer<IEntity>() {
			
			@Override
			public void consume(IEntity obj, int index) {
				if (LandmarkUtils.isLandmark(obj)) {
					final IGeometry g = getGeometryManager().getGeometry(obj);
					runOnARThread(new Runnable() {
						
						@Override
						public void run() {
							g.setVisible(showLandmarks);
						}
					});
				}
			}
		});
	}

}
