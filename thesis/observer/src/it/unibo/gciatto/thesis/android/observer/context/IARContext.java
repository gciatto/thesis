package it.unibo.gciatto.thesis.android.observer.context;

import it.unibo.gciatto.thesis.android.observer.manager.IGeometryManager;
import it.unibo.gciatto.thesis.context.IContext;

public interface IARContext extends IContext {
	IGeometryManager getGeometryManager();
}
