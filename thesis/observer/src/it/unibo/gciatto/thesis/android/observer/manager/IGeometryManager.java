package it.unibo.gciatto.thesis.android.observer.manager;

import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.entity.id.IEntityId;
import it.unibo.gciatto.thesis.manager.IManager;
import it.unibo.gciatto.util.func.IConsumer;

import java.util.List;

import com.metaio.sdk.jni.IGeometry;

public interface IGeometryManager extends IManager {
	IManager bind(IEntity entity, IGeometry geometry);

	IGeometryManager forEachGeometry(IConsumer<IGeometry> consumer);

	List<IGeometry> getGeometries();

	IGeometry getGeometry(final IEntity entity);

	IGeometry getGeometry(final IEntityId id);

	IGeometryManager setGeometry(final IEntity entity, IGeometry value);

	IGeometryManager setGeometry(final IEntityId id, IGeometry value);
}
