package it.unibo.gciatto.thesis.android.observer.context.impl;

import it.unibo.gciatto.thesis.android.observer.context.IARContext;
import it.unibo.gciatto.thesis.android.observer.manager.IGeometryManager;
import it.unibo.gciatto.thesis.android.observer.manager.impl.GeometryManager;
import it.unibo.gciatto.thesis.context.IContext;
import it.unibo.gciatto.thesis.entity.IEntity;
import it.unibo.gciatto.thesis.handler.IDataServerHandler;
import it.unibo.gciatto.thesis.manager.IEnvironmentManger;
import it.unibo.gciatto.thesis.manager.IIdentityManager;
import it.unibo.gciatto.thesis.manager.IInformationManager;
import it.unibo.gciatto.thesis.manager.IPositionManager;

public class ARContextDecorator implements IARContext {

	private final IContext mDecorated;
	private final IGeometryManager mGeometryManager;

	public ARContextDecorator(final IContext decorated) {
		mDecorated = decorated;

		if (decorated instanceof IARContext) {
			mGeometryManager = ((IARContext) decorated).getGeometryManager();
		} else {
			mGeometryManager = new GeometryManager(decorated);
		}
	}

	@Override
	public IContext bindToAllManagers(final IEntity entity) {
		return mDecorated.bindToAllManagers(entity);
	}

	public IEnvironmentManger getEnvironmentManager() {
		return mDecorated.getEnvironmentManager();
	}

	@Override
	public IDataServerHandler getDataServerHandler() {
		return mDecorated.getDataServerHandler();
	}

	@Override
	public IGeometryManager getGeometryManager() {
		return mGeometryManager;
	}

	@Override
	public IIdentityManager getIdentityManager() {
		return mDecorated.getIdentityManager();
	}

	@Override
	public IInformationManager getInformationManager() {
		return mDecorated.getInformationManager();
	}

	@Override
	public IPositionManager getPositionManager() {
		return mDecorated.getPositionManager();
	}

	@Override
	public IEntity getSelf() {
		return mDecorated.getSelf();
	}

	@Override
	public IContext unbindFromAllManagers(final IEntity entity) {
		return mDecorated.unbindFromAllManagers(entity);
	}

}
