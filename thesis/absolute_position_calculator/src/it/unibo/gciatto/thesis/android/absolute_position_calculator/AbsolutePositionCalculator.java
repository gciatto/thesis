package it.unibo.gciatto.thesis.android.absolute_position_calculator;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class AbsolutePositionCalculator extends ActionBarActivity {
	protected static double[] average(final double[] source, final int count) {
		final double[] out = new double[source.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = source[i] / count;
		}
		return out;
	}

	private static final int MAX = 1000;
	private final double[] mAverage = new double[3];
	private final double[] mAverageAverage = new double[3];
	private int mCount = 0, mAverageCount = 0;

	private boolean mEnded = false;

	private TextView mTextView;
	private LocationManager mLocationManger;

	private final LocationListener mLocationListener = new LocationListener() {

		@Override
		public void onLocationChanged(final Location location) {
			addFix(location.getLatitude(), location.getLongitude(), location.getAltitude());
		}

		@Override
		public void onProviderDisabled(final String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(final String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(final String provider, final int status, final Bundle extras) {
			// TODO Auto-generated method stub

		}
	};

	protected void addFix(final double... values) {
		if (mEnded) {
			return;
		}
		final int size1 = Math.min(values.length, mAverage.length);
		for (int i = 0; i < size1; i++) {
			mAverage[i] += values[i];
		}
		if (mCount++ % MAX == 0) {
			// mCount = mCount % MAX;
			final double[] avg = getAverage();
			final int size2 = Math.min(avg.length, mAverageAverage.length);
			for (int i = 0; i < size2; i++) {
				mAverageAverage[i] += avg[i];
			}
			if (mAverageCount++ == MAX) {
				mEnded = true;
			}
		}
		showAverages();
	}

	protected double[] getAverage() {
		return average(mAverage, mCount);
	}

	protected double[] getAverageAverage() {
		return average(mAverageAverage, mAverageCount);
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTextView = (TextView) findViewById(R.id.textView);

		mLocationManger = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
		mLocationManger.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
		mLocationManger.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);

	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int id = item.getItemId();
		if (id == R.id.textView) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void showAverages() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				final double[] avg = getAverage();
				final double[] avgAvg = getAverageAverage();

				mTextView.setText(String.format("Lat = %s\nLong = %s\nAlt = %s\n\nAvgLat = %s\nAvgLong = %s\nAvgAlt = %s\n\nCount = %s\navgCount = %s", String.valueOf(avg[0]), String.valueOf(avg[1]), String.valueOf(avg[2]), String.valueOf(avgAvg[0]), String.valueOf(avgAvg[1]), String.valueOf(avgAvg[2]), String.valueOf(mCount), String.valueOf(mAverageCount)));
			}
		});
	}
}
