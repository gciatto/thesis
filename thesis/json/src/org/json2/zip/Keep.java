package org.json2.zip;

import java.util.HashMap;

import org.json2.Kim;

/* Copyright (c) 2013 JSON.org
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * The Software shall be used for Good, not Evil.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/**
 * A keep is a data structure that associates strings (or substrings) with
 * numbers. This allows the sending of small integers instead of strings.
 *
 * @author JSON.org
 * @version 2013-05-03
 */
class Keep implements None, PostMortem {
	/**
	 * When an item ages, its use count is reduced by at least half.
	 *
	 * @param ticks
	 *            The current use count of an item.
	 * @return The new use count for that item.
	 */
	public static long age(final long ticks) {
		return ticks >= 32 ? 16 : ticks / 2;
	}

	private final int capacity;
	protected int length;
	private final Object[] list;
	private final HashMap<Object, Integer> map;
	private int power;

	private final long[] ticks;

	public Keep(final int bits) {
		capacity = 1 << bits;
		length = 0;
		power = 0;
		ticks = new long[capacity];
		list = new Object[capacity];
		map = new HashMap<Object, Integer>(capacity);
	}

	/**
	 * Return the number of bits required to contain an integer based on the
	 * current length of the keep. As the keep fills up, the number of bits
	 * required to identify one of its items goes up.
	 */
	public int bitsize() {
		while (1 << power < length) {
			power += 1;
		}
		return power;
	}

	/**
	 * Compact the keep. A keep may contain at most this.capacity elements. The
	 * keep contents can be reduced by deleting all elements with low use
	 * counts, and by reducing the use counts of the survivors.
	 */
	private void compact() {
		int from = 0;
		int to = 0;
		while (from < capacity) {
			final Object key = list[from];
			final long usage = age(ticks[from]);
			if (usage > 0) {
				ticks[to] = usage;
				list[to] = key;
				map.put(key, to);
				to += 1;
			} else {
				map.remove(key);
			}
			from += 1;
		}
		if (to < capacity) {
			length = to;
		} else {
			map.clear();
			length = 0;
		}
		power = 0;
	}

	/**
	 * Find the integer value associated with this key, or nothing if this key
	 * is not in the keep.
	 *
	 * @param key
	 *            An object.
	 * @return An integer
	 */
	public int find(final Object key) {
		final Object o = map.get(key);
		return o instanceof Integer ? ((Integer) o).intValue() : none;
	}

	@Override
	public boolean postMortem(final PostMortem pm) {
		final Keep that = (Keep) pm;
		if (length != that.length) {
			JSONzip.log(length + " <> " + that.length);
			return false;
		}
		for (int i = 0; i < length; i += 1) {
			boolean b;
			if (list[i] instanceof Kim) {
				b = list[i].equals(that.list[i]);
			} else {
				Object o = list[i];
				Object q = that.list[i];
				if (o instanceof Number) {
					o = o.toString();
				}
				if (q instanceof Number) {
					q = q.toString();
				}
				b = o.equals(q);
			}
			if (!b) {
				JSONzip.log("\n[" + i + "]\n " + list[i] + "\n " + that.list[i] + "\n " + ticks[i] + "\n " + that.ticks[i]);
				return false;
			}
		}
		return true;
	}

	/**
	 * Register a value in the keep. Compact the keep if it is full. The next
	 * time this value is encountered, its integer can be sent instead.
	 *
	 * @param value
	 *            A value.
	 */
	public void register(final Object value) {
		if (JSONzip.probe) {
			final int integer = find(value);
			if (integer >= 0) {
				JSONzip.log("\nDuplicate key " + value);
			}
		}
		if (length >= capacity) {
			compact();
		}
		list[length] = value;
		map.put(value, length);
		ticks[length] = 1;
		if (JSONzip.probe) {
			JSONzip.log("<" + length + " " + value + "> ");
		}
		length += 1;
	}

	/**
	 * Increase the usage count on an integer value.
	 */
	public void tick(final int integer) {
		ticks[integer] += 1;
	}

	/**
	 * Return the value associated with the integer.
	 *
	 * @param integer
	 *            The number of an item in the keep.
	 * @return The value.
	 */
	public Object value(final int integer) {
		return list[integer];
	}
}
